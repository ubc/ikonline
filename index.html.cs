<?php 
require_once('sys/config.inc');
page(__FILE__, 'start');
?>

<h2 id="index-heading" class="text-primary">E-Learning / IKOnline</h2>

<p>K vědecké práci nepatří pouze psaní a referáty v seminářích. Rozhodující roli sehrávají také informace, bez kterých se výše uvedené základy neobejdou. 
Pro kvalitu práce je podstatné, na jakých informacích je referát či seminární práce postavena.</p>

<p>Je stále důležitější, ale také složitější, najít kvalitní vědecké informace a ohraničit je od méně seriózních. Tento kurz proto nabízí podporu k informační kompetenci. 
Vědecká práce nekončí nalezením informací, zahrnuje také odvození, např. v seznamu literatury, hodnocení a také konkrétní údaje – tedy citace.</p>

<p>Kdo zvládá tyto základy a umí je použít, bude moci správně vědecky pracovat, diskutovat a bádat na vědecké úrovni.</p>

