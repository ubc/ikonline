<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Citace</h2>

<div class="row">
    <div class="col-xs-12">
        <blockquote>
            <p>"Ale pan Prof. Novák to chtěl přeci jinak ..."</p>
            <p>"Musím teď všechno dávat do uvozovek?"</p>
            <p>"Jednoduše tu větu pozměním, bude se skvěle hodit do mé práce."</p>
        </blockquote>
    </div>
</div>

<p>Na začátku je snad každý konfrontován s následky nekorektního citování nebo úmyslného <?php get_popover('plagiarism','plagiátorství');?>.
Následkem je neuznání zkoušky nebo akademického titulu, ale i zničená profesní kariéra.</p>

<p>Proto platí: Naučte se citovat správně již od začátku!</p>

<h3 class="text-danger">Co musím citovat?</h3>
<p>Všechno, co doslova nebo jen obsahově převezmete z cizího zdroje – s vyjímkou všeobecných informací (například začátek a konec první světové války) 
nebo základních informací ve Vašem oboru (např. Pythagorova věta).</p>

<h3 class="text-danger">Kdy musím citovat?</h3>
<p>Když chcete použít dílo nebo myšlenku někoho jiného (i když ji chcete použít pouze opisem a ne doslova!).</p>

<h3 class="text-danger">Kolikrát musím uvádět zdroj?</h3>
<p>Přímo za citací ve zkrácené formě – pozor! V případě poznámek je zdroj většinou uveden celý na začátku, poté už jen ve zkrácené formě.</p>
<p>Pokaždé, když se objeví nový citát z tohoto díla, citujete ve zkrácené formě.</p>
<p>Na konci práce v seznamu literatury jako úplný doklad o zdroji.</p>
