<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>

<h2 class="text-primary">Následky plagiátorství</h2>

<p>Plagiátorství vysokoškolských prací, popř. prací závěrečných může mít nedozírné následky.</p>

<p>Například:</p>
<ul>
    <li>neuznání zkoušky (např. seminární či závěrečné práce, obzvlášť disertace)</li>
    <li>v extrémním případě exmatrikulace</li>
    <li>právní následky kvůli porušení autorského práva</li>
    <li>vymáhání odškodného</li>
</ul>

<p>Správné citace jsou základem vědecké práce. Porušení povinnosti citovat mohou mít za důsledek domněnku, že se jedná o falšování. 
Nesprávné citování s sebou  může nést důsledky.</p>
