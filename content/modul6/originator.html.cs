<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Autorské právo</h2>

<div class="panel-group" id="accordion-urheber" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingLaw">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#law" aria-expanded="true" aria-controls="law">
                    Autorské právo
                </a>
            </h4>
        </div>
        <div id="law" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingLaw">
            <div class="panel-body">
                <p>Autorské právo reguluje práva a povinnosti autorů a ochranu duševního vlastnictví autora.</p>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOriginator">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#originator" aria-expanded="true" aria-controls="originator">
                    Autor
                </a>
            </h4>
        </div>
        <div id="originator" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOriginator">
            <div class="panel-body">
                <p>Autorem je osoba, která vytvořila vlastní dílo. Může se jednat o obrázky, vynálezy, spisy, umělecká díla, počítačové 
                programy, fotografie atd.</p>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingProperty">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#property" aria-expanded="true" aria-controls="property">
                    Duševní vlastnictví 
                </a>
            </h4>
        </div>
        <div id="property" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingProperty">
            <div class="panel-body">
                <p>Jakmile autor zveřejní svůj nápad (jako fyzickou manifestaci nebo elektronický dokument), spadá toto dílo pod autorské právo. Není nutné, 
                toto právo hlásit, jako je to zapotřebí při právní ochraně v případě živnosti.</p>
            </div>
        </div>
    </div>
</div>

<h3 class="text-danger">Zákon o autorském právu</h3>

<p>Ve zkratce: téměř vše je chráněno autorským právem – buďte opatrní při reprodukci obsahů jakéhokoli druhu!</p>

<p>Zákon o autorském  právu je velmi rozsáhlý. Definuje právo na reprodukci, využití a předvádění. Obzvláště důležité jsou meze autorského práva, které zahrnují 
následující aspekty vědecké svobody:</p>

<ul>
    <li>veřejná reprodukce, veřejné zpřístupnění pro vědu a výuku,</li>
    <li>využití děl na elektronických čtecích zařízeních ve veřejných knihovnách, muzeích a archivech,</li>
    <li>kopie pro privátní a jiné vlastní použití, zasílání kopií na požádání.</li>
</ul>

<p class="text-danger">Novinka: od jara 2018 platí v Německu nový zákon o autorských právech, který se týká speciálně univerzit a vysokých škol.</p>

<p>Přesné znění tohoto zákona najdete pod <a href="http://www.gesetze-im-internet.de/urhg/index.html" class="linkextern" target="blank">"Gesetz über 
    Urheberrecht und verwandte Schutzrechte"</a>.</p>

