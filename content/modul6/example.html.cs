<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Nejčastější formy plagiátů</h2>

<p>Častou formou plagiátu je převzetí cizích textů do vědeckých prací bez udání zdroje:</p>

<ol>
    <li>Textový plagiát: Doslovné převzetí jedné nebo více pasáží z textu bez udání zdroje.</li>
    <li>Ideový plagiát: Použití popř. parafrázování myšlenky, přičemž jsou zaměněna slova a stavba věty tak, že se zastíní původ myšlenky.</li>
    <li>Překlad myšlenek a textových pasáží z cizojazyčného textu bez udání zdroje.</li>
    <li>Převzetí metafor, idiomů nebo jazykových spojení bez udání zdroje.</li>
    <li>Plagiát citátu: Použití citátu, který byl nalezen v sekundární literatuře a použit pro podporu vlastní domněnky, ale nebyl uveden zdroj.</li>
</ol>
