<?php

$files = array (
    'de' => array(
        'index' => 'Willkommen',
        'quote' => array(
            'quote' => 'Zitieren',
            'rules' => 'Zitierregeln',
            'direct' => 'Direktes/ indirektes Zitieren',
            'din' => 'DIN'
        ),
        'plagiarism' => array(
            'plagiarism' => 'Plagiate',
            'definition' => 'Definition',
            'example' => 'Beispiele',
            'consequences'  => 'Folgen von Plagiarimus'
        ),
        'originator' => 'Urheberrecht',
        'test' => 'Abschlusstest'
    ),
    'cs' => array(
        'index' => 'Vítejte',
        'quote' => array(
            'quote' => 'Citace',
            'rules' => 'Pravidla citací',
            'direct' => 'Přímé/ nepřímé citace',
            'din' => 'DIN'
        ),
        'plagiarism' => array(
            'plagiarism' => 'Plagiáty',
            'definition' => 'Definice',
            'example' => 'Příklady',
            'consequences'  => 'Následky plagiátorství'
        ),
        'originator' => 'Autorská práva',
        'test' => 'Závěrečný test (DE)'
    ),
    'en' => array(
        'index' => 'Welcome',
        'quote' => array(
            'quote' => 'Citation',
            'rules' => 'Citation rules',
            'direct' => 'Direct/ indirect quotation',
            'din' => 'DIN'
        ),
        'plagiarism' => array(
            'plagiarism' => 'Plagiarisms',
            'definition' => 'Definition',
            'example' => 'Examples',
            'consequences'  => 'Consequences of plagiarism'
        ),
        'originator' => 'Copyright',
        'test' => 'Test'
    ),

);

?>

