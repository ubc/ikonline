<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Pravidla citací</h2>

<p>Čeho byste se měli držet? Shrnutí různých možností citování naleznete na konci tohoto modulu. Jednotlivé styly citací jsou používány v různých oborech. 
Můžete se tak podle nich orientovat. Přesto byste se měli nejprve zeptat vedoucího Vaší práce, jaké citace upřednostňuje.</p>

<h3 class="text-danger">Styly citací a obory</h3>

<div class="panel-group" id="accordion-stil" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingApa">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#apa" aria-expanded="true" aria-controls="apa">
                    APA - American Psychological Association
                </a>
            </h4>
        </div>
        <div id="apa" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingApa">
            <div class="panel-body">
                <p>Sociální vědy (Humanitní obory)<br>
                Toms, E. G., O’Brien, H. L., Kopak, R., & Freund, L. (2005). Searching for Relevance in the Relevance of Search. 
                In F. Crestani & I. Ruthven (Eds.), Context: Nature, Impact, and Role (pp. 59–78). Berlin: Springer.</p>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingMla">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#mla" aria-expanded="true" aria-controls="mla">
                    MLA - Modern Language Association
                </a>
            </h4>
        </div>
        <div id="mla" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingMla">
            <div class="panel-body">
                <p>Humanitní obory, sociální vědy<br>
                Toms, Elaine G. et al. ‘Searching for Relevance in the Relevance of Search’. Context: Nature, Impact, and Role. Ed. Fabio Crestani and Ian Ruthven. 
                Berlin: Springer, 2005. 59–78. Lecture Notes in Computer Science 3507.</p>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingChi">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#chi" aria-expanded="true" aria-controls="chi">
                    Chicago Style
                </a>
            </h4>
        </div>
        <div id="chi" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingChi">
            <div class="panel-body">
                <p>Humanitní obory<br>
                Toms, Elaine G., Heather L. O’Brien, Rick Kopak, and Luanne Freund. 2005. ‘Searching for Relevance in the Relevance of Search’. In Context: 
                Nature, Impact, and Role, edited by Fabio Crestani and Ian Ruthven, 59–78. Lecture Notes in Computer Science 3507. Berlin: Springer.</p>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingIeee">
            <h4 class="panel-title">                    
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#ieee" aria-expanded="true" aria-controls="ieee">
                    IEEE (Institute of Electrical and Electronic Engineers)
                </a>                                                                                                
            </h4>
        </div>
        <div id="ieee" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingIeee">
            <div class="panel-body">
                <p>Přírodní vědy, informatika<br>
                [1] E. G. Toms, H. L. O’Brien, R. Kopak, and L. Freund, ‘Searching for Relevance in the Relevance of Search’, in Context: Nature, Impact, and 
                Role, F. Crestani and I. Ruthven, Eds. Berlin: Springer, 2005, pp. 59–78.</p>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingHarvard">
            <h4 class="panel-title">                    
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#harvard" aria-expanded="true" aria-controls="harvard">
                    Harvard
                </a>                                                                                                
            </h4>
        </div>
        <div id="harvard" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingHarvard">
            <div class="panel-body">
                <p>Přírodní a sociální vědy<br>
                Toms, E.G., O’Brien, H.L., Kopak, R., Freund, L., 2005. Searching for Relevance in the Relevance of Search, in: Crestani, F., 
                Ruthven, I. (Eds.), Context: Nature, Impact, and Role, Lecture Notes in Computer Science. Springer, Berlin, pp. 59–78.</p>
            </div>
        </div>
    </div>
</div>

