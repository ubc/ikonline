<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">DIN ISO 690:2013-10</h2>

<p>Od října 2013  (v Čechách ČSN ISO 690:2011 platná od dubna 2011) platí všeobecně platná <?php get_popover('norm','norma');?>
pro citaci z informačních zdrojů všech druhů a je tím pádem platná i pro citace z internetu: DIN ISO 690:2013-10 “Informace a 
dokumentace – Pravidla pro udávání zdrojů a citace z internetových zdrojů”</p>

<p>Detailní podklady pro tuto normu naleznete zde: <a href="https://bit.ly/2GfaTrj" class="linkextern" target="blank">
    https://bit.ly/2GfaTrj</a></p>

<p>Zásadně platí: Důležité je, aby byl zdroj rychle a jistě dohledatelný pro nezúčastněné osoby.</p>

<p>Vzor:</p>
<p class="text-danger">AUTOR/INSTITUCE. Název dokumentu [online]. Datum [poslední připojení dne …]. Dostupný z: URL</p>

<p>Požadavky vedoucího se mohou lišit dle oboru. Zeptejte se prosím, než začnete práci psát.</p>
