<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Rozdíl mezi přímou a nepřímou citací</h2>

<p>U <strong>přímé citace</strong> je převzat přesný citát ze zdroje. Každá změna musí být označena.</p>

<p>Např.: <span class="text-danger">„[Toto] je doslovná citace s úprav[ami] a [...] vynechanými pasážemi.“ </span></p>

<p>U <strong>nepřímé citace</strong> je převzata myšlenka z původního zdroje. Mělo by být zvýrazněno, kde přesně se 
jedná o cizí myšlenky.</p>

<p>Např.: <span class="text-danger">Jak již autor XY trefně popsal, jedná se o nepřímý citát a zde jsou mé doplňky k danému tématu.</span></p>

<p><span class="glyphicon glyphicon-thumbs-up"></span> Nezapomeňte: Zeptejte se svého vedoucího práce, jaký styl citace upřednostňuje. 
Pokud nepreferuje jeden styl, rozhodněte se pro ten, který Vám vyhovuje a používejte jen ten daný.</p>
