<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Co je plagiát? - Definice.</h2>

<p>Slovo plagiát je dle německého slovníku Duden definováno jako „krádež duševního vlastnictví“.</p>

<p>Jinak řečeno: Plagiát je protiprávní převzetí a zpracování cizích textů jakéhokoli druhu a formy bez udání zdroje. 
To platí pro média, tedy knihy, vědecké a jiné časopisy, noviny a všechny jiné tištěné zdroje, ale i internet.</p>

<p>U plagiátu může dojít na jednu stranu k porušení autorského práva (chybějící udání zdroje atd.) nebo porušení užívacích práv.</p>
