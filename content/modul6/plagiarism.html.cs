<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Plagiáty</h2>

<blockquote>
    <p>Plagiátorství...<br>
    ...aneb chlubiti se cizím peřím</br>
</blockquote>

<p>Seminární práce, diplomové, magisterské a bakalářské práce, disertační práce: </p>

<p>Psaní některých vědeckých textů je nezbytnou součástí studia. Přitom právě tyto texty obsahují odkazy na již existující literaturu: knihy, články v 
odborných časopisech, pracovní podklady, příspěvky, sbírky,
<?php get_popover('primarysource','(primární) zdroje');?>, internetové zdroje atd.</p>

<p class="text-danger">Důležité: výroky, citáty, odkazy i myšlenky, které jsou – doslova nebo obsahově – převzaty z jiných zdrojů, musí být doloženy!</p>

<p>Nabízíme Vám krátký úvod do problematiky plagiátorství. Ukážeme Vám, jak se plagiátorství vyhnout. Další literaturu na toto téma, ale i obecnou literaturu k 
psaní vědeckých prací, naleznete v katalogu vaší knihovny.</p>

<p>Nápomocné Vám mohou být také podklady, jak psát dobrou vědeckou práci, které již na většině vysokých škol existují. TU Chemnitz takové podklady také nabízí, 
    <a href="../links/">odkaz</a> naleznete v seznamu odkazů.</p>
