<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Vítejte v modulu Citace a plagiáty!</h2>

<h3 class="text-danger">V tomto modulu zodpovíme následující otázky:</h3>

<ul>
    <li>Co všechno obsahuje citace?</li>
    <li>Jak byste měli citovat a co můžete převzít z vědeckých prací?</li>
    <li>Co je plagiát?</li>
</ul>

<p>Úvodní informace k tématu naleznete například v sekci Online tutoriály (DE), které naleznete v seznamu odkazů pod záložkou nástroje.</p>
