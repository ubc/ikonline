<?php

$files = array (
    'de' => array(
        'index' => 'Willkommen',
        'database' => 'Datenbank - Begriff',
        'dbis' => array(
            'dbis' => 'DatenbankInfosystem',
            'tutorial1' => 'Tutorial DatenbankInfosystem'
        ),
        'model' => array(
            'model' => '5-Schritte-Model',
            'search1' => 'Was suche ich?',
            'analyse' => 'Tutorial Themenanalyse',
            'searchterm' => 'Suchbegriffe sammeln',
            'requirement' => 'Was brauche ich?',
            'search2' => 'Wo suche ich?',
            'search3' => 'Wie suche ich?',
            'view' => 'Sichten & Modifizieren'
        ),
        'test' => 'Abschlusstest'
    ),
    'cs' => array(
        'index' => 'Vítejte',
        'database' => 'Databáze – pojem',
        'dbis' => array(
            'dbis' => 'Databankový informační systém',
            'tutorial1' => 'Tutoriál Databankový informační systém (DE)'
        ),
        'model' => array(
            'model' => 'Model pěti kroků',
            'search1' => 'Co hledám?',
            'analyse' => 'Tutoriál Tématická analýza (DE)',
            'searchterm' => 'Shromáždění hledaných pojmů',
            'requirement' => 'Co potřebuju?',
            'search2' => 'Kde hledám?',
            'search3' => 'Jak hledám?',
            'view' => 'Prohlédnutí a modifikace'
        ),
        'test' => 'Závěrečný test (DE)'
    ),
    'en' => array(
        'index' => 'Welcome',
        'database' => 'Definition of the term database',
        'dbis' => array(
            'dbis' => 'Database-Infosystem',
            'tutorial1' => 'Tutorial Database-Infosystem'
        ),
        'model' => array(
            'model' => '5 Steps Model',
            'search1' => 'What am I looking for?',
            'analyse' => 'Tutorial topic definition',
            'searchterm' => 'Collecting search terms',
            'requirement' => 'What do I need?',
            'search2' => 'Where do I look for it?',
            'search3' => 'How do I look for it?',
            'view' => 'Examination & modification'
        ),
        'test' => 'Test'
    )
);

?>

