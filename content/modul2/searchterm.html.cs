<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Strukturování tématu</h2>

<p>Zde je připraven příklad, jak by mohla vypadat tabulka k tématu „podniková komunikace“:</p>

<table class="table table-bordered table-reponsive">
    <thead>
        <tr>
            <th class="info">Aspekty</th>
            <th class="info">Komunikace</th>
            <th class="info">Podnik</th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td class="info">Synonyma</td>
        <td>komunikační proces, informační proces</td>
        <td>firma, podnik, podnikání</td>
    </tr>
    <tr>
        <td class="info">Nadřazený pojem</td>
        <td>jazyková teorie</td>
        <td>podniková ekonomika</td>
    </tr>
    <tr>
        <td class="info">Podřazené pojmy</td>
        <td>vědecká komunikace práce s veřejností, nonverbální komunikace, 
        komunikace uvnitř podniku</td>
        <td>technologické podniky malé podniky středně velké podniky</td>
    </tr>
    <tr>
        <td class="info">Příbuzné pojmy</td>
        <td>sémiotika, komunikátor, recipient, informace</td>
        <td>právní forma, podnikatel, personální plán, nákup a logistika</td>
    </tr>
    <tr>
        <td class="info">Zkratky</td>
        <td></td>
        <td>MSVP</td>
    </tr>
    <tr>
        <td class="info">Angličtina</td>
        <td>communication</td>
        <td>enterprise (firm, company)</td>
    </tr>
    </tbody>
</table>
