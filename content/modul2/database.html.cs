<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Databáze – co je to?</h2>

<p>Co je vlastně <?php get_popover('database', 'databáze');?>?</p>

<p>V knihovně znamená pojem databáze databanková aplikace. V takových aplikacích můžete strukturovaně vyhledávat v rozsáhlých sbírkách. Vyhledávat 
můžete údaje z knihovnických katalogů, bibliografií nebo sbírek.</p>

<p>Jednoduše řečno: Databáze obsahuje pokud možno všechnu literaturu, například články z časopisů k určitému tématu, nezávisle na fondu knihovny. 
Všechny databáze naleznete v <?php get_popover('dbis_term','Databankovém informačním systému');?>.</p>

