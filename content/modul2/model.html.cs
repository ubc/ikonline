<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Strukturovaná rešerše: Model pěti kroků</h2>

<p>Abyste se vyhnuli chaosu v rešerši, můžete se držet našeho systematického 
<?php get_popover('5-steps-model','modelu pěti kroků');?>:</p>
<ul>
    <li>Co hledám?</li>
    <li>Co potřebuju?</li>
    <li>Kde hledám?</li>
    <li>Jak hledám?</li>
    <li>Vyhodnocení a modifikace</li>
</ul>
