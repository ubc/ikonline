<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>

<h2 class="text-primary">Krok pátý: Vyhodnocení a modifikace</h2>

<p>Pokud jste ze seznamu shod vybrali relevantní výsledky, ještě si je jednou detailně projděte.</p>

<p>Máte málo shod? Nebo moc?</p>

<p>Můžete změnit zadání pro vyhledávání a najít tak další relevantní výsledky.</p>

<img src="../../images/sichten_cs.png" class="img-responsive">


