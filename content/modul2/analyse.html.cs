<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Tutoriál tématická analýza</h2>

<p>Ukážeme Vám vyhledávání vhodných pojmů k tématu „komunikace“.</p>
<p>Chcete-li si ukázané příklady v klidu projít, připravili jsme Vám zde použité odkazy:</p>
<ul>
<li><a href="http://swb.bsz-bw.de/DB=2.104/START_WELCOME" class="linkextern" target="blank">Společný soubor norem / Gemeinsame Normdatei (OGND)</a></li>
<li><a href="http://zbw.eu/stw/versions/latest/" class="linkextern" target="blank">WISO Standardní tezaurus ekonomický</a></li>
</ul>
<p>Slovo „tezaurus“ bude vysvětleno v <a href="../moduk4/">modulu 4</a>.</p>

<iframe style="width:100%; min-height:550px;" src="../tutorials/modul2-analyse/index.html"></iframe>

