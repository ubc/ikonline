<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Databankový informační systém – Vaše cesta ke vhodné databázi</h2>

<p>Existuje mnoho databází, které jsou dostupné buď volně na internetu nebo jsou licencovány Vaší knihovnou. Jak ale poznáte, která je pro 
Vás vhodná? Pomůže Vám <a href="http://dbis.uni-regensburg.de/dbinfo/fachliste.php?bib_id=tuche&lett=l&colors=&ocolors=" 
    class="linkextern" target="blank">Databankový informační systém (DBIS)</a>.</p>

<p>Můžete v něm hledat podla názvu databáze. Máte následující možnosti:</p>
<ul>
    <li>Na domovské stránce <?php get_popover('dbis_term','DBIS');?> zvolte v <strong>seznamu oborů</strong> (Fächerübersicht) 
    svůj obor a zobrazí se Vám relevantní databáze.</li>
    <li>Můžete si také nechat zobrazit všechny existující databáze <strong>dle abecedního pořadí</strong>.</li>
    <li>Víte-li, jakou databázi hledáte, můžete využít vyhledávací pole pro <strong>jednoduché vyhledávání</strong>.</li>
    <li>Kromě toho můžete využít také  <strong>rozšířené vyhledávání</strong>. Ve vyhledávací masce si můžete vyfiltrovat databáze, které 
    odpovídají Vašim kritériím.</li>
</ul>
