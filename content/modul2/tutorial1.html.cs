<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Turoriál Databankový informační systém</h2>

<p>V tomto tutoriálu Vám na příkladu UK Chemnitz ukážeme, jak se pomocí DBIS dostanete do databáze.</p>

<p>Mimochodem: Databáze s <?php get_popover('campuslicence','licencí pro kampus');?> jsou dostupné pouze z univerzitní sítě.</p> 

<p>Mimo univerzitní síť potřebujete službu <?php get_popover('vpn','VPN');?>, kterou poskytuje oddělení informatiky TU Chemnitz. 
Zde je odkaz na <a href="" target="blank" class="linkextern">VPN (Virtual Private Network)</a></p>

<iframe style="width:100%; min-height:550px;" src="../tutorials/modul2-dbis/index.html"></iframe>
