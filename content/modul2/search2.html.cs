<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Krok třetí: Kde hledám?</h2>

<p>A jaké databáze potřebujete pro Vaši rešerši? Náledující kritéria Vám pomůžou s rozhodnutím:</p>

<ul>
    <li>Jaký <strong>obsah</strong> zahrnuje databáze?</li>
    <li>Jaké materiály databáze vyhodnocuje? Jak <strong>úplná</strong> je databáze?</li>
    <li>Jak <strong>aktuální</strong> jsou obsahy v databázi?</li>
    <li>Jaká <strong>kritéria</strong> rozhodují o zahrnutí dokumentů do databáze?</li>
    <li>Jaké <strong>možnosti vyhledávání</strong> mám v databázi?</li>
</ul>
