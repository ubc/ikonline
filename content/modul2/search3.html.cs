<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Krok čtvrtý: Jak hledám?</h2>

<p>Zde je několik tipů pro vyhledávání v databázích:</p>

<p>Nejlépe začnete vyhledáváním <strong>prvních</strong> <?php get_popover('keyword','předmětových hesel');?>.</p>

<p>Formulujte zadání co nejpřesněji:</p>

<div class="bg-info col-md-4 col-xs-12 hover-div hovereffect">
    <p class="hover-content">Např. Tiger místo kočkovité šemy</p>
</div>

<div class="clearfix"></div>

<p>Myslete na <strong>synonyma</strong>.</p>
