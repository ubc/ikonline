<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Krok první: Co hledám?</h2>

<p>Zvolíte si téma referátu a co teď?</p>

<h3 class="text-danger">Tématická analýza obsahu</h3>

<p>Než vůbec začnete hledat v databázi, podívejte se na následující otázku: Jaké máte téma, co hledáte? Tzn., že:</p>
<ul>
    <li>si musíte vyjasnit <strong>pojmy</strong> Vašeho tématu</li>
    <li>vytvoříte <strong>seznam pojmů</strong> s tématem spojených</li>
    <li>budete myslet také na <strong>nadřazené a podřazené pojmy, synonyma a příbuzné pojmy</strong></li>
    <li>zohledníte <strong>cizojazyčné názvy</strong> a jejich <strong>rozdílný pravopis (britská a americká angličtina)</strong></li>
    <li>se zamyslíte nad případnými <strong>zkratkami</strong></li>
</ul>

<p>Při hledání pojmů najdete pomoc i v knihovně:</p>
<ul>
    <li>(naučné) slovníky</li>
    <li>slovníky synonym</li>
    <li>základní literatura a učební materiál</li>
</ul>
