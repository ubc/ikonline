<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Vítejte v modulu Příprava rešerše!</h2>

<h3 class="text-danger">V tomto modulu zodpovíme následující otázky:</h3>

<ul>
    <li>Jak strukturovat rešerši, abyste ušetřili čas?</li>
    <li>Co jsou databáze a jak poznáte, které jsou pro Vás vhodné?</li>
</ul>
