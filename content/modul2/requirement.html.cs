<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Krok druhý – Co potřebuju?</h2>

<p>Otázka, co potřebujete závisí na zadání (krátký referát, závěrečná práce, ...). Můžete využít následující seznam. Přejeďte přes pojem kurzorem myši:</p>

<div class="bg-info col-md-4 col-xs-12 col-md-offset-1 hover-div" id="checklist1">
    <p class="hover-content">Jak úplný musí být můj seznam literatury (záleží na potřebné vědecké přesnosti a času, který máte k dispozici)?</p>
</div>

<div class="bg-info col-md-4 col-xs-12 col-md-offset-2 hover-div" id="checklist2">
    <p class="hover-content">Z jaké oblasti mám použít literaturu?</p>
</div>

<div class="clearfix"></div>

<div class="bg-info col-md-3 col-xs-12 hover-div" id="checklist3">
    <p class="hover-content">Z jakého období má být použitá literatura? Potřebuju aktuální literaturu?</p>
</div>

<div class="bg-info col-md-6 col-xs-12 col-md-offset-2 hover-div" id="checklist4">
    <p class="hover-content">Jaké druhy publikací potřebuju?</p>
</div>
