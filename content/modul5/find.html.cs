<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>

<h2 class="text-primary">Vyhledání plných textů</h2>

<p>Jak otevřete v katalogu <a href="https://www.tu-chemnitz.de/ub/suchen-und-finden/emedien/ebooks/ebooks.html" 
    class="linkextern" target="blank">e-knihy</a> a <a href="https://www.tu-chemnitz.de/ub/suchen-und-finden/emedien/ejournals/ejournals.html" 
    class="linkextern" target="blank">e-žurnály</a> již víte. Jak se ale dostanete ke článkům z časopisů, které jste našli v bibliografických 
databázích nebo speciálních vyhledávačích?</p>

<p>Pokud používáte <a href="https://scholar.google.de/schhp?hl=de" class="linkextern" target="blank">Google Scholar</a> 
namísto Googlu, máte jednu výhodu: tento speciální vyhledávač prohledává již od začátku pouze akademické stránky s plnými texty a 
odkazy na odbornou literaturu. Některé dokumenty mají být dle myšlenky <?php get_popover('openaccess','Open Access');?> volně přístupné na internetu.</p>

<p>V databázích naleznete odkaz, který Vám ukáže, zda má vaše knihovna případně daný titul k dispozici. Více se dozvíte v následujícím tutoriálu.</p>
