<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Vítejte v modulu Získání literatury!</h2>

<h3 class="text-danger">V tomto modulu zodpovíme následující otázku:</h3>

<ul>
<li>Jak získáte literaturu, kterou jste nalezli při rešerši?</li>
</ul>
