<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Meziknihovní výpůjční služba</h2>

<p>Najdete-li v <a href="http://kvk.bibliothek.kit.edu/?digitalOnly=0&embedFulltitle=0&newTab=0" class="linkextern" target="blank">KVK</a>, 
na internetu nebo v databázi knihy nebo články, které nejsou k dispozici ve vaší knihovně – ani v elektronické podobě – 
pomůže Vám <?php get_popover('interlibraryloan','meziknihovní výpůjční služba');?>. </p>

<p>Zde najdete podrobné informace (DE), jak funguje meziknihovní výpůjční služba v UK Chemnitz: 
<a href="https://www.tu-chemnitz.de/ub/service/anmelden-ausleihe/fernleihe.html" class="linkextern" target="blank">
    https://www.tu-chemnitz.de/ub/service/anmelden-ausleihe/fernleihe.html</a></p>

<p>Vaše knihovna obstará knihy nebo články z jiných knihoven – bohužel ale ne zadarmo. Tato služba stojí 1,50€ za objednávku.</p>

<p>Jak tato objednávka na TU Chemnitz funguje, Vám ukážeme krok za krokem.</p>

