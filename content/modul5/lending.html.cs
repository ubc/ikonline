<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Vypůjčení a objednávka</h2>

<p>Vzpomínáte si na modul 1? Tam jste si již přečetli, jak se dostanete ke knihám, které stojí v knihovně – 
nezávisle na tom, jestli jsou součástí <?php get_popover('openaccessholding','volného výběru');?> 
nebo je musít objednat, protože stojí v <?php get_popover('stackcollection','depozitáři');?>.</p>

<p>Kromě toho už také víte, že si můžete právě vypůjčené knihy jednoduše rezervovat.</p>

<p>Pokyny naleznete zde (DE): <a href="https://www.tu-chemnitz.de/ub/service/anmelden-ausleihe/ausleihe.html" class="linkextern" target="blank">
    https://www.tu-chemnitz.de/ub/service/anmelden-ausleihe/ausleihe.html</a></p>

