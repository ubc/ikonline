<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Služby pro doručení dokumentů</h2>

<p>Nemáte čas nazbyt?</p>

<p>Kromě meziknihovní výpůjční služby nabízené vaší knihovnou, můžete využít služeb komerčních dodavatelů jako je Subito nebo Ingenta. 
Tito dodavatelé zasílají dokumenty přímo k Vám domů, jsou ale dražší.</p>

<p>Kromě toho nabízí nakladatelství na svých stránkách pro digitální dokumenty čas od času možnost stáhnout si dokument nebo článek pomocí 
služby Pay per View; to znamená, že zaplatíte poplatek za stažení.</p>

<p><span class="glyphicon glyphicon-thumbs-up"></span> V Chemnitz ale můžete využít i služby inhouse dodání, díky níž mohou příslušníci 
TU Chemnitz obdržet články z časopisů emailem.</p>
