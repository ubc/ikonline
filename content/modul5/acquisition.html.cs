<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Návrh na pořízení</h2>

<p>Pokud je kniha, kterou jste našli, velmi důležitá pro Vámi studovaný obor, ale v knihovně není v nabídce, 
můžete podat návrh na její pořízení. Pořízení závisí na posouzení odborných pracovníků a finanční situaci.</p>

<p>UK TU Chemnitz má pro tyto účely speciální <a href="https://www.tu-chemnitz.de/ub/suchen-und-finden/fachrecherche/fachportale/formulare/anschaffung_allg.php" 
    class="linkextern" target="blank">formulář (DE)</a>.</p>
