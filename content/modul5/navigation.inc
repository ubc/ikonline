<?php

$files = array (
    'de' => array(
        'index' => 'Willkommen',
        'lending' => 'Ausleihen und Bestellen',
        'find' => array(
            'find' => 'Auffinden von Volltexten',
            'tutorial1' => 'Tutorial Dokument verfügbar'
        ),
        'interlibrary' => array(
            'delivery' => 'Fernleihe/ Dokumentenlieferdienst',
            'interlibrary' => 'Fernleihe',
            'tutorial2' => 'Tutorial Fernleih-Bestellung',
            'deliveryservice'  => 'Dokumentenlieferdienste'
        ),
        'acquisition' => 'Anschaffungsvorschlag',
        'test' => 'Abschlusstest'
    ),
    'cs' => array(
        'index' => 'Vítejte',
        'lending' => 'Vypůjčení a objednávka',
        'find' => array(
            'find' => 'Vyhledání plných textů',
            'tutorial1' => 'Tutoriál Dostupný dokument (DE)'
        ),
        'interlibrary' => array(
            'delivery' => 'Meziknihovní výpůjční služba / Služby pro doručení dokumentů',
            'interlibrary' => 'Meziknihovní výpůjční služba',
            'tutorial2' => 'Tutoriál Meziknihovní výpůjční služba – objednávka (DE)',
            'deliveryservice'  => 'Služby pro doručení dokumentů'
        ),
        'acquisition' => 'Návrh na pořízení',
        'test' => 'Závěrečný test (DE)'
    ),
    'en' => array(
        'index' => 'Welcome',
        'lending' => 'Lending and ordering',
        'find' => array(
            'find' => 'Finding full texts',
            'tutorial1' => 'Tutorial Document available'
        ),
        'interlibrary' => array(
            'delivery' => 'Interlibrary loan/ Document delivery service',
            'interlibrary' => 'Interlibrary loan',
            'tutorial2' => 'Tutorial interlibrary loan request',
            'deliveryservice'  => 'Document delivery service'
        ),
        'acquisition' => 'Acquisition request',
        'test' => 'Test'
    ),

);


?>

