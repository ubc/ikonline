<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Virtuální katalog Karlsruhe/ Karlsruher Virtueller Katalog (KVK) – knihovnický metavyhledávač</h2>

<p>Přes <?php get_popover('kvk_term','KVK');?> 
(<a href="http://kvk.bibliothek.kit.edu/?kataloge=SWB&kataloge=BVB&kataloge=NRW&kataloge=HEBIS&kataloge=HEBIS_RETRO&kataloge=KOBV_SOLR&kataloge=GBV&kataloge=DDB&kataloge=STABI_BERLIN&digitalOnly=0&embedFulltitle=0&newTab=0">odkaz zde</a>) 
můžete vyhledávat v ještě větším množství dokumentů. Tento metavyhledávač Vám umožní vyhledávat v německých, švýcarských a rakouských sloučených katalozích, 
ale i mnoha mezinárodních katalozích knihoven najednou. V současné době je k dispozici přes sto miliónů knih, časopisů a dalších médií (stav únor 2016).</p>

<p>Před začátkem vyhledávání sami určíte, jaké online katalogy mají být prohledány. Váš požadavek bude zaslán do vybraných katalogů. 
Shody se poté ukážou na Vaší obrazovce.</p>

<p>Knihy z fondu Univerzitní knihovny Chemnitz a dalších saských knihoven naleznete v 
<?php get_popover('swb_term','Knihovní síti jihozápadního Německa/ Südwestdeutscher Bibliotheksverbund (SWB)');?>.</p>
