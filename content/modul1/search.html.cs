<?php 
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Vyhledávání</h2>

<p>V katalogu knihovny je nabízeno jednoduché vyhledávání (pole vyhledávání) a rozšířené vyhledávání. Občas je rozšířené vyhledávání o něco 
náročnější, vyplatí se ale, když ohraničíte vyhledávání, aby katalog nenašel příliš mnoho informací.</p>

<p>Většinou můžete k jednoduchému vyhledávání přidat ještě jedno pole. Např. pole autor nebo heslo – více k tomu později.</p>

<p>Zde vidíte <a href="https://katalog.bibliothek.tu-chemnitz.de/?mylang=cs" class="linkextern" target="blank">úvodní stranu katalogu</a> 
Univerzitní knihovny v Chemnitz. Můžete si vyzkoušet vyhledávání. Českou verzi přepněte vpravo nahoře</p>

<iframe style="width:100%; min-height:550px;" src="https://katalog.bibliothek.tu-chemnitz.de/?mylang=cs"></iframe>

