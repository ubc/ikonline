<?php 
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Univerzitní knihovny</h2>

<p>Hlavním úkolem univerzitní knihovny je pokud možno rozsáhlé zajištění relevantní literatury příslušníkům univerzity, 
a to od rektora po studenty, ale i širokou veřejnost. Univerzitní knihovny jsou často rozdělené na několik oddělení. 
V jednoduchém knihovnickém systému, ve kterém neexistují odborné a institucionální knihovny, je univerzitní knihovna 
centrálním zařízením. Většinou ale existují různá oddělení, které by měl každý znát: ve volném výběru jsou knihy k zapůjčení 
a prezenční knihy, které se půjčit nedají, ale jsou dostupné k užívání v rámci knihovny. V depozitáři jsou často knihy 
prezenční, ale i výpůjční, které musí uživatel předem objednat.</p>

<p>V následujícím tutoriálu (DE) se dozvíte základy o Univerzitní knihovně v Chemnitz, jež má působit jako příklad.</p>

<iframe style="width:100%; min-height:550px;" src="../tutorials/modul1-intro/index.html"></iframe>
