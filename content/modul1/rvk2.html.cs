<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Příklady pro Řezenskou sdruženou klasifikaci (RSK)</h2>

<table class="table table-striped">
    <thead>
        <tr class="info">
            <th>kód</th>
            <th>význam</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>R</td>
            <td>Geografie</td>
        </tr>
        <tr>
            <td>RC</td>
            <td>Regionální  geografie</td>
        </tr>
        <tr>
            <td>RC 10000</td>
            <td>Evropa</td>
        </tr>
    </tbody>
</table>

<p>Chcete-li se o RSK dozvědět více: <a href="http://rvk.uni-regensburg.de/index.php/regensburger-verbundklassifikation-online"
    class="linkextern" target="blank">http://rvk.uni-regensburg.de/index.php/regensburger-verbundklassifikation-online</a></p>
