<?php 
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Rozšířené vyhledávání – pro přímé a variabilní vyhledávání</h2>

<p>Vedle <?php get_popover('simplesearch', 'jednoduchého vyhledávání');?> můžete v téměř každém katalogu využít možnosti <?php get_popover('expertsearch', 'rozšířeného vyhledávání ');?> 
Pomůže Vám to především při přímém hledání a variabilním vyhledávání. Rozšířená maska ve vyhledávači u nás vypadá takto:</p>

<iframe style="width:100%; min-height:550px;" src="https://katalog.bibliothek.tu-chemnitz.de/Search/Advanced?mylang=cs"></iframe>
