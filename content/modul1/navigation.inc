<?php

$files = array (
    'de' => array(
        'index' => 'Willkommen',
        'libraries' => 'Universitätsbibliotheken',
        'catalog' => array(
            'catalog' => 'Katalog',
            'search' => 'Sucheinstieg',
            'information' => 'Hinweise',
            'tutorial1' => 'Tutorial Katalog',
            'list' => 'Ergebnisliste',
            'rank' => 'Ranking',
            'limit' => 'Einschränken',
            'display' => 'Exemplaranzeige',
            'tutorial2' => 'Tutorial Exemplaranzeige',
            'expand' => 'Erweiterte Suche'
        ),
        'rvk' => array(
            'rvk' => 'RVK',
            'rvk2' => 'Beispiele'
        ),
        'semester' => 'Semesterapparate',
        'regional' => array(
            'regional' => 'Überregional Suchen',
            'kvk' => 'KVK'
        ),
        'test' => 'Abschlusstest'
    ),
    'cs' => array(
        'index' => 'Vítejte',
        'libraries' => 'Univerzitní knihovny',
        'catalog' => array(
            'catalog' => 'Katalog',
            'search' => 'Vyhledávání',
            'information' => 'Tipy',
            'tutorial1' => 'Tutoriál katalog (DE)',
            'list' => 'Výsledky',
            'rank' => 'Ranking',
            'limit' => 'Omezení',
            'display' => 'Zobrazení exempláře',
            'tutorial2' => 'Tutoriál zobrazení exempláře (DE)',
            'expand' => 'Rozšířené vyhledávání'
        ),
        'rvk' => array(
            'rvk' => 'RVK',
            'rvk2' => 'Příklady'
        ),
        'semester' => 'Semestrální aparáty',
        'regional' => array(
            'regional' => 'Nadregionální vyhledávání',
            'kvk' => 'KVK'
        ),
        'test' => 'Závěrečný test (DE)'
    ),
    'en' => array(
        'index' => 'Welcome',
        'libraries' => 'University Library',
        'catalog' => array(
            'catalog' => 'Catalog',
            'search' => 'Search entry',
            'information' => 'Notes',
            'tutorial1' => 'Tutorial catalog (DE)',
            'list' => 'Resultlist',
            'rank' => 'Ranking',
            'limit' => 'Filter',
            'display' => 'Display of results',
            'tutorial2' => 'Tutorial display of results (DE)',
            'expand' => 'Advanced search'
        ),
        'rvk' => array(
            'rvk' => 'RVK classsification',
            'rvk2' => 'Example'
        ),
        'semester' => 'Course reserved collection',
        'regional' => array(
            'regional' => 'Search nationwide',
            'kvk' => 'KVK'
        ),
        'test' => 'Test (DE)'
    ),
);

?>

