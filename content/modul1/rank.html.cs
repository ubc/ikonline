<?php 
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Ranking - Tip</h2>

<p>Výsledky jsou ve většině případech řazeny dle komplexního postupu v rankingu.</p>

<p>Například  výsledky s většinovou shodou zadaných hesel  jsou hodnoceny jako velmi důležité.</p>

<p>Kromě toho je důležité, kolik hesel je zadaných ve vyhledávacích polích (např. pole název,
<?php get_popover('subjectheading','klíčové slovo');?>) . Zobrazí-li se hledané heslo v krátkém názvu dokumentu, 
bude tento dokument v pořadí výše, než dokument s delším názvem.</p>

<p>Smyslem systému je vyhlédávat a nalézt výsledky, které třeba na první pohled nevyhledáváme, jsou pro nás ale také zajímavé.</p>
