<?php 
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Zobrazení exempláře – všechno o dokumentu</h2>

<p>A jak teď najdete knihu, která se objevila na seznamu? Po kliknutí na dokument se zobrazí všechny patřičné údaje. 
Nejprve uvidíte <?php get_popover('bibliographicdata', 'bibliografické údaje');?>  (tzn. autor, název díla atd.) 
a poté níže přístup k dokumentu.</p>

<p>Cesty k dokumentu mohou být různé.</p>

<p>V krátkém tutoriálu (DE) na další stránce jsme shromáždili nejčastější možnosti přístupu  na TU Chemnitz.</p>
