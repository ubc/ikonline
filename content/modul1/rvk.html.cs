<?php 
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Řezenská sdružená klasifikace – jak jsou knihy uspořádané v regálech?</h2>

<p>V knihovnách existuje mnoho možností, jak knihy uspořádat. Od formálních kritérií, jako je například datum zpracování (nummerus currens), 
přes systematické uspořádání, při kterém jsou knihy přiřazeny jednotlivým oborům, ale i forma s prvky obou uspořádání.</p>

<p>V anglicky hovořících zemích jsou neznámější Deweyova <?php get_popover('system','klasifikace ');?> a LCC – Library of Congress Classificaion. 
V německy hovořících zemích je snad nejznámější <?php get_popover('rvk_term', 'Řezenská sdružená klasifikace/ Regensburger 
Verbundklassifikation (RVK)');?>.</p>

<p>Jistě už jste si při vyhledávání povšimli slova signatura, např. QT 000 end nebo MR 2000 die. Toto označení zahrnuje jak tématické zařazení
v RVK, tak umístění v knihovně včetně popisu regálu.</p>

<p>Knihy s podobným obsahem stojí v knihovně u sebe, a tak můžete na stejném místě najít i další potřebnou literaturu.</p>

<p>Stejně tak můžete vyhledávat i v katalogu, ve kterém se zobrazí všechny knihy se stejným umístěním v regálu.</p>
