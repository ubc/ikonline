<?php 
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Tutoriál katalog</h2>

<p>Na tomto interaktivním vyhledávání v katalogu Univerzitní knihovny v Chemnitz můžete vidět, jak funguje vyhledávání. 
(Pozor: Ve videu budete vyzváni zadat sami hledané heslo. Přejeďte jednoduše kurzorem myši do prostoru vyhledávání.)</p>

<iframe style="width:100%; min-height:550px;" src="../tutorials/modul1-suche/index.html"></iframe>

