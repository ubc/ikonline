<?php 
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Tipy</h2>

<p>Mnoho katalogů spojuje vyhledávané výrazy s „AND“ (více k „AND“ a jiným operátorům  naleznete v <a href="../modul3/">modulu 3</a>).</p>

<p>Zadáte-li do katalogu Univerzitní knihovny v Chemnitz více než tři hesla, budou tři náhodná spojena s „AND“ a další s „OR“ – podobně jako 
při vyhledávání na Googlu. Tato spojení ale můžete ovlivnit využitím  rozšířeného vyhledávání.</p>

<p>Někdy se může stát, že výsledek na první pohled neodpovídá zadání ve vyhledávání. Některé vyhledávače například vyhledávají podobné výrazy. 
Když zadáte heslo „dítě“, naleznete ve výsledcích děti, adoptované dítě, předškolní dítě, dívka, chlapec atd.</p>

<p>Podrobněji se budeme katalogu věnovat <a href="tutorial1.html">v tutoriálu</a> (DE).</p>

