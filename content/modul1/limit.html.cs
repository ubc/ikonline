<?php 
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Omezení vyhledávání – získání kompaktních seznamů</h2>

<p>Pro získání kompatního seznamu  výsledků můžete ve většině katalogů zpřísnit výběr pomocí filtrů.</p>

<p>Můžete tak například vyhledat výsledky z určitého oboru, tituly v určitém jazyce nebo s konkrétním rokem vydání.</p>

<p>Zaškrtnete-li více polí v jedné kategorii, bude použit operátor „OR“. Získáte pak například jak výsledky z oboru psychologie, 
tak výsledky z oboru sociologie.</p>

<p>Pro zobrazení praktické ukázky na příkladu Univerzitní knihovny v Chemnitz:
<a href="https://katalog.bibliothek.tu-chemnitz.de/Search/Results?mylang=cs&type=AllFields&lookfor=schulangst+grundschule" 
class="linkextern" target="blank"> Online katalog</a></p>
