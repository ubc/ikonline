<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Vítejte v modulu Univerzitní knihovna a katalog!</h2>

<h3 class="text-danger">V tomto modulu zodpovíme následující otázky:</h3>

<ul>
    <li>Jak fungují vědecké knihovny?</li>
    <li>Co všechno najdu v katalogu knihovny a co ne?</li>
    <li>Jak najdu mojí knihu v knihovně? A jak si ji vypůjčím?</li>
    <li>Semestrální aparáty – co je to a má je každá knihovna?</li>
    <li>Kde můžu hledat dál, když moje knihovna nemá hledanou knihu nebo časopis?</li>
</ul>
