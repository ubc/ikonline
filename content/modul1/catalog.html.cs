<?php 
    require_once('../../sys/config.inc');
    page(__FILE__);
?>
<h2 class="text-primary">Katalog Univerzitní knihovny – cesta k literatuře</h2>

<p>S pomocí <?php get_popover('catalog', 'katalogu knihovny');?> </span></a> můžete vyhledávat dokumenty, které knihovna zakoupila 
nebo je má licencované. To všechno máte k dipozici. Kromě toho mají knihovny v katalogu také dokumenty, které jsou volně přístupné 
na internetu. Všechny tyto zdroje jsou vhodné pro Vaše seminární a závěrečné práce nebo referáty.</p>

<p>V katalogu Univerzitní knihovny v Chemnitz naleznete například tištěné a elektronické</p>
<ul>
    <li>knihy: <?php get_popover('monograph','monografie');?>, příručky všeho druhu, lexika, dizertace</li>
    <li>časopisy a noviny</li>
    <li><?php get_popover('thesis','vysokoškolské práce');?> TU Chemnitz</li>
</ul>
<p>ale i</p>
<ul>
    <li>výběr článků z knih a časopisů, jejichž plné texty jsou volně dostupné na internetu – pro podrobnější vyhledávání jsou 
    vhodnější databáze – více v <a href="../modul3/">modulu 3</a>.</li>
    <li>ostatní, např. DVD, CD atd.</li>
</ul>

<p><strong>Tip:</strong> Takzvaný „OPAC“ není vlastně ničím jiným než elektronickým katalogem knihovny. Dříve obsahoval i kartotéční 
karty. Proto rozlišujeme „online“: OPAC znamená „Online Public Access Catalogue“.</p>
