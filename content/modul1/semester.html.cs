<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Semestrální aparáty – vybraná seminární literatura</h2>

<p>Konvenční semestrální aparáty se skládají z dokumentů, které Váš vyučující připravil pro určitý seminář.<br>
<?php get_popover('semester', 'Semestrální aparáty');?> jsou v knihovně odděleny od běžného volného výběru. 
Většinou zůstávají sestaveny po dobu jednoho, někdy i více semestrů.</p>

<p>Literatura v semestrálních aparátech je tak důležitá, že je vyňata z běžné výpůjčky. Stojí-li kniha v semestrálním aparátu, 
vidíte to na našem příkladu v zobrazení exempláře v katalogu. Zde je také zobrazeno, v jakém semestrálním aparátu se kniha nachází:</p>

<img src="../../images/semester.png" class="img-responsive">

<p>Semestrální aparáty bývají také v eletronické podobě, kam ho umístí vyučující. V Sasku je tomu přizpůsobena platforma zvaná OPAL.</p>
