<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Nadregionální vyhledávání</h2>

<p>A kde můžete hledat, pokud hledanou knihu nenajdete v katalogu? Vaše vyhledávání není omezeno pouze na katalog dané knihovny. 
Kromě tohoto katalogu existují také sloučené katalogy a <?php get_popover('metasearchengine','metavyhledávače');?>, 
ve kterých můžete vyhledávat v několika knihovnách najednou.</p>

<p>Pokud najdete hledaný dokument, můžete si ho objednat ve Vaší knihovně v rámci meziknihovní výpůjční služby 
(více se dozvíte v <a href="../modul5/">modulu 5</a>).</p>
