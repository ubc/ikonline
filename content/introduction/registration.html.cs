<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>

<h2 class="text-primary">Jak se příhlásím na blended-lerarning cvičení informační kompetence?</h2>

<p>Abyste se mohli přihlásit, musíte být přihlášeni na jednom z těchto bakalářských oborů na TU Chemnitz:</p>
<ul>
    <li>Aplikovaná informatika</li>
    <li>Germanistika</li>
    <li>Politologie</li>
</ul>
<p>Přihlásit se můžete přes OPAL (Germanistika a Aplikovaná informatika) nebo přes svou katedru (Politologie).</p>

