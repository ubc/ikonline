<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>

<h2 class="text-primary">Jaké informace naleznete v našem kurzu?</h2>

<ul>
    <li>Jak přesně ohraničit téma práce, popsat ho vlastními slovy a najít vhodná hesla pro rešerši</li>
    <li>Seznámíte se s databázemi a internetovými portály důležitými pro Váš obor</li>
    <li>Jak vybrat tu správnou databázi pro Vaše téma</li>
    <li>Naučíte se vyhledávat odbornou literaturu v databázích a internetových vyhledávačích</li>
    <li>Naučíte se vyhodnotit nalezené výsledky a vybrat jen ty opravdu důležité</li>
    <li>Procvičíte si s námi export výsledků do programu spravujícího Vaši literaturu</li>
    <li>a my Vás naučíme, jak se dostanete k plným textům článků a knih a v případě, že v knihovně není článek nebo kniha k dispozici, ukážeme Vám, jak je můžete objednat</li>
</ul>

