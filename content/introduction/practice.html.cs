<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Jak cvičení probíhá?</h2>

<p>Cvičení Informační kompetence je sestaveno z několika prezenčních hodin.</p>

<p>Počet hodin se dle oboru různí. Mezi prezenčními hodinami se učíte vybranou část e-learningového modulu sami. Poté se s naučenou 
látkou vrátíte zpět do skupiny a látku si společně procvičíme. Můžete tak využít možnost klást otázky a zároveň nás poznat. 
Představíme Vám také oborové databáze.</p>

<p>Obdržíte speciální trénink v malých skupinkách přizpůsobený Vaší potřebě.</p>
