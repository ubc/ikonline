<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Jaké technické vybavení pro kurs potřebujete?</h2>

<ul>
    <li>Moduly jsou zpracovány ve formátu HTML-5 a jsou vhodné pro nové mobilní přístroje. Pro vhodný učební efekt je vhodná velikost tabletu a větší.</li>
    <li>Interaktivní funkce jako pojmy ve slovníku ( <span class="glyphicon glyphicon-book glossar"></span> ) nebo obrázky, jsou viditelné, když přes ně přejedete myší</li>
    <li>V obsahu naleznete slovník, tedy seznam odkazů pro jednotlivé moduly, a přehled k modulům</li>
    <li>Některé databáze jsou přístupné pouze z prostor univerzity. Vzdálený přístup je možný pouze díky službě 
    <a href="https://www.tu-chemnitz.de/urz/network/access/vpn.html">VPN (Virtual Private Network)</a>.</li>
</ul>
