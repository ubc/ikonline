<?php

$files = array (
    'de' => array(
        'index' => 'Willkommen',
        'content' => 'Kursinhalte',
        'modules' => 'Module',
        'technology' => 'Technik',
        'participants' => array(
            'participants' => 'Für Teilnehmer der Lehrveranstaltung',
            'registration' => 'Anmeldung',
            'practice' => 'Übung',
            'credit' => 'Credit Points',
            'performance' => 'Leistungen, Klausuren'
        )
    ),
    'cs' => array(
        'index' => 'Vítejte',
        'content' => 'Obsah kurzu',
        'modules' => 'Moduly',
        'technology' => 'Technické vybavení',
        'participants' => array(
            'participants' => 'Pro uživatele ze seminářů na TU Chemnitz',
            'registration' => 'Přihlásit',
            'practice' => 'Cvičení',
            'credit' => 'Credit Points',
            'performance' => 'Zkoušky, klauzury'
        )
    ),
    'en' => array(
        'index' => 'Welcome',
        'content' => 'Course content',
        'modules' => 'Units',
        'technology' => 'Technical requirements',
        'participants' => array(
            'participants' => 'For participants of TU Chemnitz courses',
            'registration' => 'Registration',
            'practice' => 'Seminar',
            'credit' => 'Credit points',
            'performance' => 'Requirements and exams'
        )
    )
);


?>
