<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>

<h2 class="text-primary">Portfolio rešerší</h2>

<p>Vyberete si téma a vyhledáte k němu literaturu, z níž vytvoříte bibliografii.</p>

<p>Poté popíšete vlastními slovy, jak jste ve vyhledávání literatury v našich databázích a na internetu postupovali. To vše nám zašlete emailem. 
My Vám  poté zašleme zpětnou vazbu s poznámkami, co můžete případně ještě vylepšit.</p>

<p>Podrobné informace naleznete v <a href="https://www.tu-chemnitz.de/ub/kurse-und-e-learning/ik/Materialien/rechercheportfolio_neu.pdf">návodu k portfoliu (DE)</a>.</p>

<h2 class="text-primary">Klauzura</h2>

<p>Obsahy sdělené na prezenčních hodinách a látka z IKOnline jsou základem pro klauzuru. Hlavním bodem jsou otázky, které máte zodpovědět 
vlastními slovy. Kromě toho Vás čekají i otázky s výběrem odpovědí.</p>

<p><strong>Máte-li otázky, obraťte se na odborného referenta: </strong>

<ul>
    <li>Aplikovaná informatika – Carolin Ahnert (<a href="mailto:carolin.ahnert@bibliothek.tu-chemnitz.de">carolin.ahnert@bibliothek.tu-chemnitz.de</a>)</li>
    <li>Germanistika – Katja Knop (<a href="mailto:katja.knop@bibliothek.tu-chemnitz.de">katja.knop@bibliothek.tu-chemnitz.de</a>)</li>
    <li>Politologie – Joachim Stemmler (<a href="mailto:joachim.stemmler@bibliothek.tu-chemnitz.de">joachim.stemmler@bibliothek.tu-chemnitz.de</a>)</li>
</ul>
