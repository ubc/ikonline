<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>

<h2 class="text-primary">Jaké moduly obsahují jednotlivé části e-learningu?</h2>
<ul>
    <li><a href="../modul1/index.cs">Modul 1: Univerzitní knihovna a katalog</a></li>
    <li><a href="../modul2/index.cs">Modul 2: Příprava rešerše</a></li>
    <li><a href="../modul3/index.cs">Modul 3: Techniky vyhledávání a funkce v databázích</a></li>
    <li><a href="../modul4/index.cs">Modul 4: Rešerše na internetu</a></li>
    <li><a href="../modul5/index.cs">Modul 5: Získání literatury</a></li>
    <li><a href="../modul6/index.cs">Modul 6: Citace a plagiáty</a></li>
</ul>
<p>Moduly můžete využít jednotlivě nebo všechny, jak chcete. Do modulů jsme integrovali interaktivní testy (DE). Můžete si tak vyzkoušet, co jste se naučili.</p>

