<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>

<h2 class="text-primary">Co musíte splnit, abyste získali kredity?</h2>

<p>Splníte-li předpoklady, obdržíte dva CP – a kromě toho máte dobrý základ pro rešerše během studia.</p>

<p>Předpoklady se liší dle oboru:</p>

<div class="col-xs-12 bg-primary">
    <h3>Studijní obory a zkouškové předpoklady</h3>
    <p><em>Klikněte prosím na obor pro zobrazení obsahu.</em>
    <div class="col-xs-4 text-center credit-subject" id="credit-it">
        <h4>Aplikovaná informatika</h4>
    </div>
    <div class="col-xs-4 text-center credit-subject" id="credit-german">
        <h4>Germanistika</h4>
    </div>
    <div class="col-xs-4 text-center credit-subject" id="credit-politics">
        <h4>Politologie</h4>
    </div>

    <div class="col-xs-12 credit-white">
        <div class="credit credit-it">
            <h5>povinně volitelný</h5>
            <p>typ zkoušky:</br>
            portfolio rešerší (předpoklad pro zkoušku), klauzura, účast na prezenčních hodinách</p>
        </div>

        <div class="credit credit-german">
            <h5>povinný</h5>
            <p>typ zkoušky:</br>
            portfolio rešerší, účast na prezenčních hodinách</p>
        </div>

        <div class="credit credit-politics">
            <h5>povinně volitelný</h5>
            <p>typ zkoušky:</br>
            portfolio rešerší (předpoklad pro zkoušku), klauzura, účast na prezenčních hodinách</p>
        </div>
    </div>
</div>
