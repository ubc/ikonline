<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Pro uživatele ze seminářů na TU Chemnitz</h2>

<p>K vybraným bakalářským studijním oborům na TU Chemnitz byly k prezenčnímu studiu koncipovány a vytvořeny moduly e-learningu, 
jež nabízí některé oblasti z nabídky informační kompetence k samostudiu a jsou integrovány do kompletní oblasti zprostředkování 
informační kompetence ve formě elementů blended-learningu.</p>

<p>Do jednotlivých modulů jsou integrovány testy a cvičení (DE), částečně interaktivní, které nabízí okamžitou zpětnou 
vazbu k pokroku a vlastnímu úspěchu.</p>

<p>Kromě toho mohou všichni zájemci, kteří se zajímají o téma informační kompetence a chtějí se něco naučit, využít online 
tutoriály (DE) bez jakéhokoli omezení.</p>

<p>Každý modul představuje samostatnou učební a vyučovací jednotku, nemusíte tedy postupovat v kurzu popořadě.</p>
