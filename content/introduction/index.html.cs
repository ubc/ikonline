<?php 
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Vítejte</h2>

<h3 class="text-danger">Informační kompetence – proč a k čemu je dobrá?</h3>

<p>Vědecká práce je v průběhu studia čím dál více důležitá a nároky stoupají – proto jsou dobré znalosti rešerší nepostradatelným základem úspěšné práce a projektů. </p>

<p>V tomto kurzu k informační kompetenci Vám ukážeme, jak rozlišovat důležité od méně důležitého a jak hledat cíleně informace, které zrovna potřebujete. </p>

<p>Kromě informačních zdrojů na internetu existuje mnoho databází, které Vám také rádi představíme.</p>

<p>Na TU Chemnitz jsou tyto online moduly integrovány do blended-learning semináře. Víc informací naleznete v menu pod bodem „pro uživatele ze seminářů na TU Chemnitz“.</p>
