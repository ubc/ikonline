<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Vyhledávání frází</h2>

<p>Hledáte pojem, který se skládá z několika slov?</p>

<p>Pro vyhledávání konkrétního znakového řetězce vložte pojem do uvozovek, např. „make or buy“, „Václav Havel“ nebo „česko-saské vztahy“.</p>

<p>Říká se tomu vyhledávání frází.</p>
