<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Volba vyhledávacího pole</h2>

<div class="col-xs-8 col-md-10">

<p>V <a href="../modul2/">modulu 2</a> jsme Vám ukázali, jak naleznete databázi. Nyní máme pár tipů, jak v těchto databázích vyhledávat.</p>

<p>Většina vyhledávacích technik je vhodná pro všechny databáze.</p>

<p>Některé databáze ale nabízí i funkce navíc.</p>

<p>Než proto začnete vyhledávat, informujte se v napovědě, zda v dané databázi takové funkce existují.</p>
</div>
<div class="col-xs-4 col-md-2">
    <img src="../../images/technik.png" class="img-responsive">
</div>
