<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Index</h2>

<p>Jak vyhledávat autory se zvláštním pravopisem jako například Gudula Rünger nebo Thomas Geßner v anglické databázi? 
Ruenger? Runger? Gessner? Geßner?</p>

<p>Pokud si nejste jistí, jak se něco píše, pomůže Vám index dané databáze. Index neboli rejstřík je abecední seznam 
všech hledaných výrazů, které můžete zadat do pole vyhledávání.</p>

<h3 class="text-danger">Tutoriál Vyhledávání zvláštních znaků (DE)</h2>

<iframe style="width:100%; min-height:550px;" src="../tutorials/modul3-index/index.html"></iframe>
