<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Využití Booleovských operátorů</h2>

<p>Vaše téma seminární práce není jen o vyhledávání? Pro propojení několika aspektů.</p>

<p>Využijte proto <?php get_popover('booleanoperator','Booleovské operátory');?>:</p>


<div class="col-md-3 col-xs-12 hovereffect">
    <p>AND</p>
</div>
<div class="bg-info col-md-9 col-xs-12 hover-div hovereffect">
    <p class="hover-content">Obdržíte výsledky s oběma pojmy.</p>
</div>

<div class="clearfix"></div>

<div class="col-md-3 col-xs-12 hovereffect">
    <p>OR</p>
</div>
<div class="bg-info col-md-9 col-xs-12 hover-div hovereffect">
    <p class="hover-content">Obdržíte výsledky, ve kterých je buď jeden nebo druhý a nebo oba pojmy.</p>
</div>

<div class="clearfix"></div>

<div class="col-md-3 col-xs-12 hovereffect">
    <p>NOT</p>
</div>
<div class="bg-info col-md-9 col-xs-12 hover-div hovereffect">
    <p class="hover-content">Obdržíte výsledky, ve kterých bude vyloučen jeden pojem.</p>
</div>

<div class="clearfix"></div>
<p>Jednotlivé operátory můžete i kombinovat.</p>

<p>Jak mohou takové kombinace vypadat, uvidíte v následující tabulce.</p>

<div class="col-xs-11">
    <table class="table table-bordered table-reponsive">
        <thead>
            <tr>
                <th class="info">Aspekty</th>
                <th class="info">Komunikace</th>
                <th class="info">Podnik</th>
            </tr>
        </thead>
        <tbody>
        <tr>
            <td class="info">Synonyma</td>
            <td>komunikační proces, informační proces</td>
            <td>firma, podnik, podnikání</td>
        </tr>
        <tr>
            <td class="info">Nadřazený pojem</td>
            <td>jazyková teorie</td>
            <td>podniková ekonomika</td>
        </tr>
        <tr>
            <td class="info">Podřazené pojmy</td>
            <td>vědecká komunikace práce s veřejností, nonverbální komunikace, komunikace uvnitř podniku</td>
            <td>technologické podniky, malé podniky, středně velké podniky</td>
        </tr>
        <tr>
            <td class="info">Příbuzné pojmy </td>
            <td>sémiotika, komunikátor, recipient, informace</td>
            <td>právní forma, podnikatel, personální plán, nákup a logistika</td>
        </tr>
        <tr>
            <td class="info">Zkratky</td>
            <td></td>
            <td>MSVP</td>
        </tr>
        <tr>
            <td class="info">Angličtina</td>
            <td>communication</td>
            <td>enterprise (firm, company)</td>
        </tr>
        </tbody>
    </table>
</div>

<div class="col-xs-1" id="or">
    <span class="glyphicon glyphicon-arrow-up"></span>
    <p class="sr-only">OR</p>
    OR&nbsp;
    <span class="glyphicon glyphicon-arrow-down"></span>
</div>

<div class="col-xs-12" id="and">
    <span class="glyphicon glyphicon-arrow-left"></span>
    <p class="sr-only">AND</p>
    AND
    <span class="glyphicon glyphicon-arrow-right"></span>

</div>
