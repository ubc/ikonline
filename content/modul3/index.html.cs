<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Vítejte v modulu Techniky vyhledávání a funkce v databázích!</h2>

<h3 class="text-danger">V tomto modulu zodpovíme následující otázky:</h3>

<ul>
    <li>Jaké techniky vyhledávání můžete používat ve vyhledávání v databázích?</li>
    <li>Jaké další komfortní funkce nabízí databáze?</li>
</ul>
