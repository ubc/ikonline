<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Pomocné funkce</h2>

<p>Pokud máte otázky k zacházení s databází, vyplatí se nahlédnout do nápovědy. Většinou ji najdete hned na úvodní stránce. 
Často jsou k dispozici také tutoriály, ve kterých je návod na vyhledávání v dané databázi.</p>

<p>A pokud Vám to nestačí:</p>

<p>Zeptejte se ve Vaší knihovně! Telefonicky, emailem nebo osobně.</p>
