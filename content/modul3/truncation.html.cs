<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Truncation</h2>

<p>Pokud jste našli vhodná hesla, může se stát, že narazíte na problém s pravopisem. Vyhledávače hledají přesné pořadí znaků. 
Proto při vyhledávání slova „Toskana“ nenaleznete výsledky obsahující „Toscana“.</p>

<p>Stejně tak se vyhledávače chovají v případě gramatických koncovek hledaných slov.</p>

<p>Nezoufejte! Pomůže vám tzv. <?php get_popover('truncation','truncation');?>, při které použijete otazník ?, 
hvězdičku * nebo znaky $ a #.</p>

<p>Chcete-li vědět, které znaky Vámi používaná databáze uznává, podívejte se do nápovědy. Najdete v ní např. pojem 
<?php get_popover('wildcard','wildcard');?></a>.</p>

<p>Příklady (přejeďte myší přes barevné pole):</p>

<div class="bg-info col-xs-12 hover-div hovereffect" id="trunk">
    <div class="hover-content">
        <dl class="dl-horizontal">
            <dt>Kind*</dt>
            <dd>Kinder, Kinderarmut, Kinderarbeit, Kinderbetreuung</dd>
            <dt>*hilfe</dt>
            <dd>Integrationshilfe, Aufbauhilfe, Starthilfe</dd>
            <dt>Tos?ana</dt>
            <dd>Toskana, Toscana</dd>
            <dt>wom$n<dt>
            <dd>woman, women</dd>
        </dl>
        <p>Otazník zde zastává přesně jeden znak, hvězdička libovolný počet.</p>
    </div>

</div>
