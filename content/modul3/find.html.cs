<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Nalezení vyhledávaného pojmu</h2>

<p>Zadáte-li do správných polí správné vyhledávací výrazy, bude Vaše vyhledávání úspěšné.</p>

<p>Rady, jaké pomůcky máte při vyhledávání k dispozici, jsme Vám dali již v <a href="../modul2/">modulu 2</a>.</p>

<p>Zde jsou ještě dva doplňující tipy:</p>
<ul>
    <li>Používejte v databázích tezaury popř. <?php get_popover('subjectheadinglist','předmětové hesláře');?>.</li>
    <li>Obsažené odborné pojmy v databázích, především klíčová slova, Vás mohou odkázat i dál.</li>
</ul>
