<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Užitečné funkce v databázích</h2>

<p>Kromě vyhledávacích technik, které jsme Vám představili, nabízí mnoho databází další feutures, které usnadňují vyhledávání:</p>

<ul>
    <li><?php get_popover('searchhistory','Search History');?></li>
    <li><?php get_popover('alert','Alerting');?></li>
    <li>Pomocné funkce</li>
</ul>
