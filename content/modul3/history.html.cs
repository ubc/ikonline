<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Search History</h2>

<p>Při komplexním vyhledávání můžete snadno ztratit přehled. Search History nabízí možnost zobrazení posledních 
vyhledávaných výrazů z aktuálního vyhledávání. Můžete si je taktéž uložit pro příští vyhledávání a propojení.</p>

<img src="../../images/history.png" class="img-responsive">

