<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Volba vyhledávacího pole</h2>

<p>Stejně jako v internetových vyhledávačích probíhá vyhledávání v databázích přes vyhledávací pole, do kterých se 
zadávají hledané pojmy. Většinou existuje jednoduché vyhledávání s jedním vyhledávacím polem, do kterého zadáte 
libovolný text, a rozšířené vyhledávání s <?php get_popover('searchtemplate','vyhledávací maskou');?>, která nabízí několik vyhledávacích polí.</p>

<h3 class="text-danger">Jednoduché vyhledávání</h3>
<img src="../../images/db_einfach.png" class="img-responsive">

<h3 class="text-danger">Rozšířené vyhledávání</h3>
<img src="../../images/db_erweitert.png" class="img-responsive">
