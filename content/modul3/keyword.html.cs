<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Předmětové heslo/ klíčové slovo</h2>

<p>Víte, jaký je rozdíl mezi předmětovým heslem a klíčovým slovem? V každodenním životě jsou tyto pojmy používány jako 
synonyma. Přesněji se ale o synonyma nejedná:</p>

<table class="table table-striped">
    <thead>
        <tr class="info">
            <th>Předmětové heslo</th>
            <th>Klíčové slovo</th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td>slovo vyjadřující obsah z názvu či abstraktu publikace</td>
        <td>pojem popisující téma publikace</td>
    </tr>
    <tr>
        <td>nemusí souhlasit s obsahem publikace</td>
        <td>nemusí být v názvu či abstraktu</td>
    </tr>
    <tr>
        <td>databáze: pomocí předmětového hesla vyhledáváte v textových polích; např. v poli název, abstrakt</td>
        <td>databáze: klíčové slovo (nebo také deskriptor, keyword nebo subject term) má vlastní vyhledávací pole</td>
    </tr>
    </tbody>
</table>
