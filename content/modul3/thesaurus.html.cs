<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Tezaurus</h2>

<p>Jak si můžete být jistí, že pracujete s pojmy, které používá k popisu i Vámi vybraná databáze? Pomůže Vám tezaurus.</p>

<p>V tutoriálu <a href="../modul2/analyse.html.cs">Tématická analýza (Modul 2)</a> jsme již jeden tezaurus jako vyhledávací 
pomůcku poznali. Ale co je vlastně tezaurus?</p>

<p><?php get_popover('thesaurus','Tezaurus');?> pojmů je systematicky uspořádaná sbírka pojmů. Všechna klíčová slova 
v tezauru se nazývají <?php get_popover('descriptor','deskriptory');?> se stejným oborem. Tezaurus najdete v závislosti 
na databázi v různých jazycích.</p>

<p><strong>Tip:</strong> Použijete-li pro Vaše vyhledávání deskriptory z tezauru v databázi, budou Vaše výsledky preciznější!</p>

<p>Příklady:</p>
<ul>
    <li><a href="http://zbw.eu/stw/versions/latest/" class="linkextern" target="blank">STW Thesaurus for Economicks</a>,
    <li><a href="http://web.a.ebscohost.com/ehost/thesaurus?vid=2&sid=337f87ea-8146-4e64-a1bc-179337e6a722%40sessionmgr4009" class="linkextern" target="blank">MLA-Thesaurus</a>
    (Thesauruslink oben links)</li>
</ul>
