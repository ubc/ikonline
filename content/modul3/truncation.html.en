<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Truncation</h2>

<p>If you have found suitable search terms you encounter the problem that there are different spellings. Since electronic search instruments 
search strictly for strings all hits with "Toscana" are ignored when searching for "Toskana". </p>

<p>The same applies to the different grammatical endings of the search terms.</p>

<p>No problem! <?php get_popover('truncation','Truncation');?> helps you here. You use the question mark ? or the asterisk *, also the characters $ and # are common.</p>

<p>If you want to know exactly which character is used for truncation, it is best to look for the terms truncation or <?php get_popover('wildcard','wildcard');?> 
in the help text of the corresponding database.</p>

<p>Examples (move your mouse over the colored field):</p>

<div class="bg-info col-xs-12 hover-div hovereffect" id="trunk">
    <div class="hover-content">
        <dl class="dl-horizontal">
            <dt>Child*</dt>
            <dd>Children, child poverty, child labour, child care</dd>
            <dt>*aid</dt>
            <dd>Integration aid, development aid, start-up aid </dd>
            <dt>Tos?ana</dt>
            <dd>Toskana, Toscana</dd>
            <dt>wom$n<dt>
            <dd>woman, women</dd>
        </dl>
        <p>The question mark and the dollar symbol replaces exactly one character, the asterisk stands for any number of characters.</p>
    </div>

</div>
