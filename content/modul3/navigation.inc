<?php

$files = array (
    'de' => array(
        'index' => 'Willkommen',
        'technology' => 'Suchtechniken',
        'searchfields' => array(
            'search' => 'Suchfelder/Suchbegriffe',
            'searchfields' => 'Suchfelder wählen',
            'find' => 'Suchbegriffe finden',
            'keyword' => 'Stichwort/ Schlagwort',
            'tutorial1' => 'Tutorial Schlagwörter',
            'thesaurus' => 'Thesaurus',
            'register' => 'Index',
        ),
        'truncation' => array(
            'searchtools' => 'Suchwerkzeuge',
            'truncation' => 'Trunkierung',
            'phrase' => 'Phrasen suchen',
            'boolean' => 'Boolesche Operatoren',
            'tutorial2' => 'Tutorial Boolesche Operatoren'
        ),
        'function' => array(
            'function' => 'Datenbankfunktionen',
            'history' => 'Search History',
            'alert' => 'Alerting',
            'help' => 'Hilfefunktionen'
        ),
        'test' => 'Abschlusstest'
    ),
    'cs' => array(
        'index' => 'Vítejte',
        'technology' => 'Techniky vyhledávání ',
        'searchfields' => array(
            'search' => 'Vyhledávací pole/ vyhledávané pojmy',
            'searchfields' => 'Volba vyhledávacího pole',
            'find' => 'Nalezení vyhledávaného pojmu',
            'keyword' => 'Předmětové heslo/ klíčové slovo',
            'tutorial1' => 'Tutoriál klíčová slova (DE)',
            'thesaurus' => 'Tezaurus',
            'register' => 'Index',
        ),
        'truncation' => array(
            'searchtools' => 'Nástroje pro vyhledávání',
            'truncation' => 'Truncation',
            'phrase' => 'Vyhledávání frází',
            'boolean' => 'Booleovské operátory',
            'tutorial2' => 'Tutoriál Booleovské operátory (DE)'
        ),
        'function' => array(
            'function' => 'Funkce v databázích',
            'history' => 'Search History',
            'alert' => 'Alerting',
            'help' => 'Pomocné funkce'
        ),
        'test' => 'Závěrečný test (DE)'
    ),
    'en' => array(
        'index' => 'Welcome',
        'technology' => 'Search techniques',
        'searchfields' => array(
            'search' => 'Seach fields/ Search terms',
            'searchfields' => 'Choosing search fields',
            'find' => 'Finding search terms',
            'keyword' => 'Keywords/ subject headings',
            'tutorial1' => 'Tutorial subject headings',
            'thesaurus' => 'Thesaurus',
            'register' => 'Index',
        ),
        'truncation' => array(
            'searchtools' => 'Search tools',
            'truncation' => 'Truncation',
            'phrase' => 'Phrase searching',
            'boolean' => 'Boolean operators',
            'tutorial2' => 'Tutorial Boolean operators'
        ),
        'function' => array(
            'function' => 'Database functions',
            'history' => 'Search history',
            'alert' => 'Alerting',
            'help' => 'Help'
        ),
        'test' => 'Test'
    ),


);

?>

