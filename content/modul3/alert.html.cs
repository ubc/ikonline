<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Alerting</h2>

<p>Pokud pracujete delší dobu na jednom tématu, pomůže Vám funkce Alerting, abyste zůstali aktuální. Uložte vyhledávání. 
Pokud databáze při své aktualizaci objeví nové výsledky, bude Vám automaticky zaslán seznam výsledků (Alert) emailem nebo 
jako RSS-Feed.</p>

<img src="../../images/alerting.png" class="img-responsive">

