<?php 
require_once('../../sys/config.inc');
page(__FILE__);
?>

<h2>Obecné odkazy*</h2>
<ul>
    <li><a href="http://www.gesetze-im-internet.de/urhg/index.html" class="linkextern" target="blank">Autorské právo</a></li>

    <li><a href="https://www.base-search.net/about/de/help.php" class="linkextern" target="blank">BASE Help</a></li>

    <li><a href="https://www.base-search.net/about/de/index.php" class="linkextern" target="blank">Bielefeld Academic Search Engine (BASE)</a></li>

    <li><a href="http://dbis.uni-regensburg.de/dbinfo/detail.php?bib_id=tuche&colors=&ocolors=&lett=fs&tid=0&titel_id=5660" 
        class="linkextern" target="blank">Business Source Complete (EBSCO Host)</a></li>

    <li><a href="https://mediatum.ub.tum.de/node?id=1231945" class="linkextern" target="blank">Citace – osnova TU München/ Zitierleitfaden TU München</a></li>

    <li><a href="http://dbis.uni-regensburg.de/dbinfo/fachliste.php?bib_id=tuche&lett=l" class="linkextern" target="blank">Databankový informační systém/ 
	Datenbank-Infosystem (DBIS)</a></li>

    <li><a href="http://dbis.uni-regensburg.de/dbinfo/detail.php?bib_id=tuche&colors=&ocolors=&lett=fs&tid=0&titel_id=7197" 
        class="linkextern" target="blank">Datenbank PSYNDEXplus Literature and Audiovisual Media (EBSCO Host)</a></li>

    <li><a href="http://dbis.uni-regensburg.de/dbinfo/detail.php?bib_id=tuche&colors=&ocolors=&lett=fs&tid=0&titel_id=277" 
        class="linkextern" target="blank">FIS Bildung</a></li>

    <li><a href="http://wirtschaftslexikon.gabler.de/" class="linkextern" target="blank">Gabler Wirtschaftslexikon</a></li>

    <li><a href="https://scholar.google.de/intl/de/scholar/about.html" class="linkextern" target="blank">Google Scholar</a></li>

    <li><a href="https://scholar.google.de/intl/de/scholar/help.html" class="linkextern" target="blank">Google Scholar Help</a></li>

    <li><a href="http://dbis.uni-regensburg.de/dbinfo/detail.php?bib_id=tuche&colors=&ocolors=&lett=fs&tid=0&titel_id=76" 
        class="linkextern" target="blank">MLA International Bibliography</a></li>

    <li><a href="https://fidmath.de/" class="linkextern" target="blank">Odborná infromační služba pro matematiku</a></li>

    <li><a href="http://www.linguistik.de/" class="linkextern" target="blank">Odborný portál pro germanistiku</a></li>

    <li><a href="https://gi.de/service/digitale-bibliothek/" class="linkextern" target="blank">Odborný portál pro informatiku</a></li>

    <li><a href="https://www.econbiz.de/" class="linkextern" target="blank">Odborný portál pro ekonomii</a></li>

    <li><a href="https://www.tu-chemnitz.de/ub/suchen-und-finden/fachrecherche/fachportale/index.html" class="linkextern" target="blank">Odborné portály UK Chemnitz</a></li>

    <li><a href="https://www.youtube.com/watch?v=CbKvgS2TiDM&list=PLcKvP7CbWnTSnIUJwyK5biURJmRPlOi2G" class="linkextern" 
        target="blank">Online tutoriál Lotse-Team: Plagiáty</a></li>

    <li><a href="https://www.youtube.com/watch?v=su90I6kLze8" class="linkextern" target="blank">Online tutoriál Lotse-Team: Pravidla citací</a></li>

    <li><a href="https://rvk.uni-regensburg.de/regensburger-verbundklassifikation-online" class="linkextern" target="blank">Řezenská sdružená klasifikace/ Regensburger Verbundklassifikation</a></li>

    <li><a href="http://swb.bsz-bw.de/DB=2.104/START_WELCOME" class="linkextern" target="blank">Společný soubor norem</a></li>

    <li><a href="http://zbw.eu/stw/versions/latest/" class="linkextern" target="blank">Standardní tezaurus ekonomický/ Standard Thesaurus Wirtschaft</a></li>

    <li><a href="https://www.zotero.org/styles/" class="linkextern" target="blank">Přehled o stylech citací (EN)</a></li>

    <li><a href="http://kvk.bibliothek.kit.edu/" class="linkextern" target="blank">Virtuální katalog Karlsruhe/ Karlsruher Virtueller Katalog</a></li>

    <li><a href="https://www.vifaost.de/" class="linkextern" target="blank">Virtuální odborná knihovny východní Evropy/ Virtuelle Fachbibliothek Osteuropa (Politik)</a></li>

    <li><a href="https://www.vifa-recht.de/" class="linkextern" target="blank">Virtuální odborná knihovna pro právo/ Virtuelle Fachbibliothek Recht</a></li>

    <li><a href="https://www.tu-chemnitz.de/urz/network/access/vpn.html#client" class="linkextern" target="blank">VPN Client</a></li>

    <li><a href="http://dbis.uni-regensburg.de/dbinfo/detail.php?bib_id=tuche&colors=&ocolors=&lett=fs&tid=0&titel_id=1331" 
        class="linkextern" target="blank">WTI-Frankfurt</a></li>
</ul>

<h2>Odkazy na obsahy UK Chemnitz</h2>

<ul>
    

    <li><a href="https://www.tu-chemnitz.de/ub/suchen-und-finden/emedien/ebooks/ebooks.html" class="linkextern" 
        target="blank">E-Books nabídka UK Chemnitz</a></li>

    <li><a href="https://www.tu-chemnitz.de/ub/suchen-und-finden/emedien/ejournals/ejournals.html" class="linkextern" target="blank">E-Journals nabídka UK Chemnitz</a></li>

    <li><a href="https://www.tu-chemnitz.de/forschung/grundsaetze.php" class="linkextern" target="blank">Jak psát dobrou vědeckou práci na Technické univerzitě v 
    Chemnitz/ Ordnung zur Sicherung guter wissenschaftlicher Praxis der Technischen Universität Chemnitz</a></li>

    <li><a href="https://katalog.bibliothek.tu-chemnitz.de/" class="linkextern" target="blank">Katalog UK Chemnitz</a></li>

    <li><a href="https://www.tu-chemnitz.de/ub/service/anmelden-ausleihe/dokulief/dokulief.html" class="linkextern" 
        target="blank">Komerční dodavatelé</a></li> 

    <li><a href="https://www.tu-chemnitz.de/ub/service/anmelden-ausleihe/fernleihe.html" class="linkextern" target="blank">Meziknihovní výpůjční služba</a></li>

    <li><a href="https://www.tu-chemnitz.de/ub/suchen-und-finden/fachrecherche/fachportale/formulare/anschaffung_allg.php" 
        class="linkextern" target="blank">Návrh na pořízení</a></li>

    <li><a href="https://www.tu-chemnitz.de/ub/service/anmelden-ausleihe/ausleihe.html" class="linkextern" 
        target="blank">Pokyny k výpůjčce a bjednávce v UK Chemnitz</a></li>

    <li><a href="https://www.tu-chemnitz.de/ub/index.html.cs" data-lang="cs" class="linkextern" target="blank">Web UK Chemnitz (CZ)</a></li>
</ul>

<p>* Není-li uvedeno jinak, obsahují uvedené odkazy obsahy v němčině.</p>




