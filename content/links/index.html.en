<?php 
require_once('../../sys/config.inc');
page(__FILE__);
?>

<h2>General links</h2>
<ul>

    <li><a href="https://www.base-search.net/about/de/help.php" class="linkextern" target="blank">BASE Help</a></li>

    <li><a href="https://www.base-search.net/about/de/index.php" class="linkextern" target="blank">Bielefeld Academic Search Engine (BASE)</a></li>

    <li><a href="http://dbis.uni-regensburg.de/dbinfo/detail.php?bib_id=tuche&colors=&ocolors=&lett=fs&tid=0&titel_id=5660" 
        class="linkextern" target="blank">Business Source Complete (EBSCO Host)</a></li>

    <li><a href="https://mediatum.ub.tum.de/node?id=1231945" class="linkextern" target="blank">Citation guide of the TU München</a></li>

    <li><a href="http://dbis.uni-regensburg.de/dbinfo/fachliste.php?bib_id=tuche&lett=l" class="linkextern" target="blank">Database Information System (DBIS)</a></li>

    <li><a href="http://dbis.uni-regensburg.de/dbinfo/detail.php?bib_id=tuche&colors=&ocolors=&lett=fs&tid=0&titel_id=7197" 
        class="linkextern" target="blank">Datenbank PSYNDEXplus Literature and Audiovisual Media (EBSCO Host)</a></li> 

    <li><a href="http://wirtschaftslexikon.gabler.de/" class="linkextern" target="blank">Gabler Business Encyclopedia</a></li>

    <li><a href="https://scholar.google.de/intl/de/scholar/about.html" class="linkextern" target="blank">Google Scholar</a></li>

    <li><a href="https://scholar.google.de/intl/de/scholar/help.html" class="linkextern" target="blank">Google Scholar Help</a></li>

    <li><a href="http://kvk.bibliothek.kit.edu/" class="linkextern" target="blank">Karlsruhe Virtual Catalog (KVK)</a></li>

    <li><a href="https://fidmath.de/" class="linkextern" target="blank">Mathematics Information Service</a></li>

    <li><a href="http://dbis.uni-regensburg.de/dbinfo/detail.php?bib_id=tuche&colors=&ocolors=&lett=fs&tid=0&titel_id=76" 
        class="linkextern" target="blank">MLA International Bibliography</a></li>

    <li><a href="http://swb.bsz-bw.de/DB=2.104/START_WELCOME" class="linkextern" target="blank">Online Gemeinsame Normdatei</a></li>

    <li><a href="https://www.youtube.com/watch?v=CbKvgS2TiDM&list=PLcKvP7CbWnTSnIUJwyK5biURJmRPlOi2G" class="linkextern" 
        target="blank">Online tutorial Lotse-Team: Plagiarisme</a></li>

    <li><a href="https://www.youtube.com/watch?v=su90I6kLze8" class="linkextern" target="blank">Online tutorial Lotse-Team: Citation rules</a></li>

    <li><a href="https://www.zotero.org/styles/" class="linkextern" target="blank">Overview of citation styles</a></li>

    <li><a href="https://rvk.uni-regensburg.de/regensburger-verbundklassifikation-online" class="linkextern" target="blank">Regensburger Verbundklassifikation</a></li>

    <li><a href="https://gi.de/service/digitale-bibliothek/" class="linkextern" target="blank">Specialised Information Service Computer Science</a></li>

    <li><a href="https://www.econbiz.de/" class="linkextern" target="blank">Specialised Information Service Economics</a></li>

    <li><a href="http://dbis.uni-regensburg.de/dbinfo/detail.php?bib_id=tuche&colors=&ocolors=&lett=fs&tid=0&titel_id=277" 
        class="linkextern" target="blank">Specialised Information Service Education Science</a></li>

    <li><a href="http://www.linguistik.de/" class="linkextern" target="blank">Specialised Information Service Linguistics</a></li>

    <li><a href="http://zbw.eu/stw/versions/latest/" class="linkextern" target="blank">Standard thesaurus economy</a></li>

    <li><a href="https://www.tu-chemnitz.de/ub/suchen-und-finden/fachrecherche/fachportale/index.html" class="linkextern" target="blank">Subject portals of the Chemnitz University Library</a></li>

    <li><a href="http://www.gesetze-im-internet.de/urhg/index.html" class="linkextern" target="blank">Copyright Law</a></li>

    <li><a href="https://www.vifaost.de/" class="linkextern" target="blank">Virtual Library Eastern Europe (politics)</a></li>

    <li><a href="https://www.vifa-recht.de/" class="linkextern" target="blank">Virtual Library Law</a></li>

    <li><a href="https://www.tu-chemnitz.de/urz/network/access/vpn.html#client" class="linkextern" target="blank">VPN Client</a></li>

    <li><a href="http://dbis.uni-regensburg.de/dbinfo/detail.php?bib_id=tuche&colors=&ocolors=&lett=fs&tid=0&titel_id=1331" 
        class="linkextern" target="blank">WTI-Frankfurt</a></li>


</ul>

<h2>Links of the Chemnitz University Library</h2>

<ul>
    <li><a href="https://www.tu-chemnitz.de/ub/suchen-und-finden/fachrecherche/fachportale/formulare/anschaffung_allg.php" 
        class="linkextern" target="blank">Aquisition form</a></li>

    <li><a href="https://katalog.bibliothek.tu-chemnitz.de/" class="linkextern" target="blank">Catalog of the UB Chemnitz</a></li>

    <li><a href="https://www.tu-chemnitz.de/ub/service/anmelden-ausleihe/dokulief/dokulief.html" class="linkextern" 
        target="blank">Commercial document delievery services</a></li>

    <li><a href="https://www.tu-chemnitz.de/ub/suchen-und-finden/emedien/ebooks/ebooks.html" class="linkextern" 
        target="blank">E-books of the UB Chemnitz</a></li>

    <li><a href="https://www.tu-chemnitz.de/ub/suchen-und-finden/emedien/ejournals/ejournals.html" class="linkextern" target="blank">E-journals of the UB Chemnitz</a></li>

    <li><a href="https://www.tu-chemnitz.de/ub/service/anmelden-ausleihe/fernleihe.html" class="linkextern" target="blank">Interlibrary loan</a></li>

    <li><a href="https://www.tu-chemnitz.de/ub/service/anmelden-ausleihe/ausleihe.html" class="linkextern" 
        target="blank">Notes on borrowing and ordering at the Chemnitz UB</a></li>

    <li><a href="https://www.tu-chemnitz.de/forschung/grundsaetze.php" class="linkextern" target="blank">Order to ensure good scientific 
        practice at the Chemnitz University of Technology</a></li>

    <li><a href="https://www.tu-chemnitz.de/ub/" class="linkextern" target="blank">Website of the UB Chemnitz</a></li>
</ul>





