<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Co použít? -  Prohlédnutí a modifikace</h2>

<p>Jaké obsahy nakonec použijete pro svoji vědeckou práci? Pokud si kladete tuto otázku, dostanete se 
opět ke kroku č. 5 z našeho modelu pěti kroků z <a href="../modul2/view.html.cs">modulu 2</a>.</p>

<p>A taková otázke je v případě rešerše na internetu velmi důležitá! </p>

<div class="col-xs-12 bg-primary">
    <p><em>Podívejte se na následující kritéria kvality, která platí pro zdroje z internetu (klikněte na sloupec).</em></p>

    <div class="col-xs-2 col-xs-offset-1 text-center credit-subject" id="credit-purpose "> 
        <h4>Účel</h4>
    </div>
    <div class="col-xs-2 text-center credit-subject" id="credit-source">
        <h4>Zdroj</h4>
    </div>
    <div class="col-xs-2 text-center credit-subject" id="credit-content">
        <h4>Obsah</h4>
    </div>

    <div class="col-xs-2 text-center credit-subject" id="credit-reference">
        <h4>Reference</h4>                                                                                        
    </div>
    <div class="col-xs-2 text-center credit-subject" id="credit-quote">
        <h4>Citace</h4>                                                                                        
    </div>

    <div class="col-xs-10 col-xs-offset-1 credit-white" id="quote">
        <div class="credit credit-purpose">
            <p>Účel zveřejnění (cílová skupina, objektivita) – Komu je dokument určen? Vědecké komunitě, interesovaným laikům?</p>
        </div>

        <div class="credit credit-source">
            <p>Zdroj (autor, korporace, top domain level URL (.gov, .org)) – Je autorem Vašeho zdroje příslušník “Univerzity of Cambridge” 
            nebo Petr Novák z Nemanic?</p>
        </div>

        <div class="credit credit-content">
            <p>Obsah (přesnost, verifikace, hloubka a šířka informací, aktualita) – Kdy byl proveden poslední update? Jak detailní 
            jsou uvedené údaje?</p>
        </div>

        <div class="credit credit-reference">
            <p>Reference (reference/ odkazy na jiné zdroje, odkazy na stránku) – Jsou k dispozici další literární zdroje? Jsou výroky 
            opatřeny použitou literaturou?</p>
        </div>

        <div class="credit credit-quote">
            <p>Citace – Je tento text citován jiným vědcem?</p>
        </div>

    </div>
</div>

