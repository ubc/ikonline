<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Vítejte v modulu Rešerše na internetu!</h2>

<h3 class="text-danger">V tomto modulu zodpovíme následující otázky:</h3>

<ul>
    <li>Jaké vyhledávače a webové stránky jsou vhodné pro vědecké práce?</li>
    <li>Jak můžete na internetu efektivně hledat informace a především vědecké texty?</li>
    <li>Jaké zdroje z internetu můžete použít pro svou vědeckou práci?</li>
</ul>
