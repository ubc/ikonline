<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Důležité odborné  stránky</h2>

<div class="col-xs-12  bg-primary">
    <h3>Odborné portály</h3>
    <p><em>Pro zobrazení obsahu klikněte prosím na obor.</em></p>
    <div class="col-xs-2 text-center credit-subject" id="credit-it">
        <h4>Informatika</h4>
    </div>
    <div class="col-xs-2 text-center credit-subject" id="credit-german">
        <h4>Germanistika</h4>
    </div>
    <div class="col-xs-2 text-center credit-subject" id="credit-politics">
        <h4>Politologie</h4>
    </div>
    <div class="col-xs-2 text-center credit-subject" id="credit-law">
        <h4>Právo</h4>
    </div>
    <div class="col-xs-2 text-center credit-subject" id="credit-math">
        <h4>Matematika</h4>
    </div>
    <div class="col-xs-2 text-center credit-subject" id="credit-economics">
        <h4>Ekonomie</h4>
    </div>
    <div class="col-xs-12 credit-white">
        <div class="credit credit-it">
            <h4>Odborný portál pro informatiku</h4>
            <p>Odkaz: <a href="http://www.io-port.net/index.php?id=358" class="linkextern" target="blank">http://www.io-port.net/index.php?id=358</a></p>
            <p>Odborný portál pro informatiku provozuje FIZ Karlsruhe – Leibnitzův Institut pro informační infrastrukturu.</p>
        </div>

        <div class="credit credit-math">
            <h4>Odborný portál pro matematiku</h4>
            <p>Odkaz: <a href="https://fidmath.de/" class="linkextern" target="blank">https://fidmath.de/</a></p>
            <p>Centrální vyhledávání ve zdrojích typických pro matematiku.</p>
        </div>

        <div class="credit credit-politics">
            <h4>Odborný portál pro politologii</h4>
            <p>Virtuální odborná knihovna východní Evropy: <a href="https://www.vifaost.de/" class="linkextern" target="blank">https://www.vifaost.de/</a></p>
            <p>Jedná se o nadoborový regionální portál k výzkumu východní Evropy.</p>
            <p>Díky detailním vyhledávacím funkcím máte přístup k vědeckým informacím z oboru dějepisu, jazyků, literatury, politologie a kultury zemí a 
            regionů východní, středovýchodní a jihovýchodní Evropy.</p>
        </div>

        <div class="credit credit-law">
            <h4>Odborný portál pro právo</h4>
            <p>Virtuální odborná knihovna pro právo: <a href="http://www.vifa-recht.de/" class="linkextern" target="blank">http://www.vifa-recht.de/</a></p>
            <p>Virtuální odborná knihovna pro právo je servisní portál odborné informační služby pro mezinárodní a interdisciplinární výzkum v oboru práva.</p>
        </div>

        <div class="credit credit-german">
            <h4>Odborný portál pro germanistiku</h4>
            <p>Lin|gu|is|tik - Portál pro jazykovědu: <a href="http://www.linguistik.de" class="linkextern" target="blank">http://www.linguistik.de</a></p>
            <p>Portál Lin|gu|is|tik nabízí odborné informace ke všem oborům jazykovědy.</p>
        </div>

        <div class="credit credit-economics">
            <h4>Odborný portál pro ekonomii</h4>
            <p>ECONBIZ – Find Economic Literature: <a href="http://www.econbiz.de/" class="linkextern" target="blank">http://www.econbiz.de/</a></p>
            <p>Portál ECONBIZ je nabídka <a href="http://www.zbw.eu/" class="linkextern" target="blank">ZBW – Leibnitzovo Informační centrum pro ekonomii/ 
            Německá centrální knihovna pro ekonomii</a>.</p>
        </div>
    </div>
</div>

<p>Existuje mnoho dalších odborných portálů. Jaké se hodí právě k Vašemu oboru, můžete zjistit v <a href="http://dbis.uni-regensburg.de/dbinfo/fachliste.php?bib_id=tuche&lett=l"
    class="linkextern" target="blank">DBIS</a>.</p>

