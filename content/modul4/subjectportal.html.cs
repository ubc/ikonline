<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Kde ještě můžu hledat? – Odborné stránky</h2>

<p>Kromě toho všeho existují ještě speciální, <?php get_popover('subjectportal','odborné internetové stránky');?>.
Tyto odborné portály obsahují vybranou nabídku obsahů, které jsou pravidelně verifikovány.</p>

<h3 class="text-danger">Jaké výhody mají tyto stránky?</h3>

<ul>
    <li>Obsahují vědecky relevantní odkazy.</li>
    <li>Obsahy jsou sestaveny, verifikovány a komentovány vědeckým personálem.</li>
    <li>Provozovatel (např. úřady, vědecké instituce, podniky) zpravidla nemají zájem na zpeněžení nabídky.</li>
    <li>Nabídky omezí Váš výběr tématicky a kvalitativně.</li>
</ul>

<p>Dostanete se k relevantním výsledkům.</p>
