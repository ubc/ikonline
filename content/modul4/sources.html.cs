<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Informační zdroj internet</h2>

<blockquote>
    <p>Effective internet searching is part science, part art, part skill and part luck</p>
    <footer>Bradley 2013, 18</footer>
</blockquote>

<p>Od svého vzniku původně z <?php get_popover('arpanet','ARPANETU');?> se internet etabloval jako dynamická pracovní, 
informační a komunikační platforma pro (téměř) každý obor včetně vědy a studia.</p>

<p>Určitě se ale ptáte, jak a kde na internetu naleznete vědecké texty. Nejde jenom o náhodu, díky níž na texty narazíte.</p>
