<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Jak hledat? Techniky vyhledávání</h2>

<p>Většinu technik vyhledávání, které jsme Vám představili v <a href="../modul3/">modulu 3</a> k použití 
při vyhledávání v databázích, můžete použít i ve vyhledávačích a odborných portálech, např.:</p>

<ul>
    <li>výběr mezi jednoduchým a rozšířeným vyhledáváním</li>
    <li>Booleovské operátory</li>
    <li>vyhledávání frází</li>
    <li>Truncation</li>
    <li>Search History</li>
    <li>Alerting</li>
</ul>

