<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Kde (hlavně) nehledat? – Obecné vyhledávače</h2>

<p>Jsou Google, Yahoo, DuckDuckGo a.j. vhodné pro eficientní vědeckou rešerši?</p>

<p>Odpověď je jasná: <strong>Ne</strong>.</p> 

<p>Nejspíš už jste se nejednou zlobili, že výsledky vyhledávání na Googlu dosahují příliš vysokých čísel.</p>

<p>Kromě toho si nemůžete být vždy jistí, jak relevantní, spolehlivé a seriózní tyto výsledky jsou. Pro každodenní rešerši k určitým
faktům nebo běžným záležitostem se internetové <?php get_popover('searchengine','vyhledávače');?> určitě hodí, 
pro rozsáhlejší, informační rešerše není jejich využití příliš eficientní.</p>

<p>Jedním důvodem pro nejistotu při vyhledávání pomocí obecných vyhledávačů je, že nemohou zahrnovat všechny zdroje. 
Tyto vyhledávače tedy neprohledávají obsahy speciálních databází, které jsou chráněny heslem nebo vyhledávacím formulářem. 
A neumí zobrazit všechny datové formáty (např. flash).</p>

<p>Název <?php get_popover('invisibleweb','Deep Web');?> popisující skryté obsahy na internetu jste již určitě slyšeli. 
Obecné vyhledávače prohlížejí pouze tzv. Visible nebo také <?php get_popover('surfaceweb','Surface Web');?> (viditelý neboli povrchový web).</p>
