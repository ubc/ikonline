<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Surface, Deep a Dark Web</h2>

<p>Přejeďte myší přes jednotlivá pole.</p>
<img src="../../images/surface.png" class="img-responsive">

<div class="col-xs-2 hover-div surface text-center hovereffect">
    <p class="hover-content">Obsahy, které vyhodnocují vyhledávače.</p>
</div>

<div class="col-xs-6 col-xs-offset-1 hover-div surface hovereffect">
    <div class="hover-content">
        <p class="col-xs-6">Obsahy, které z technických důvodů vyhledávače nevyhodnotí, např. zaheslované obsahy, 
        obsahy schované za formuláře, obsahy v nestandardních formátech.</p>
        <p class="col-xs-6">Obsahy, které jsou pro vyhledávače záměrně neviditelné.</p>
    </div>
</div>

<div class="col-xs-2 col-xs-offset-1 hover-div surface text-center hovereffect">
    <p class="hover-content">Obsahy, které si musí <br>uživatel propojit manuálně.</p>
</div>
