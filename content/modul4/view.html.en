<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">What do I use? - Reviewing and modifying</h2>

<p>Which content can you use for your scientific paper? If you ask yourself this question, you are back 
to step 5 of the 5-step model from <a href="../modul2/view.html">unit 2</a>.</p>

<p>And this question is really important for internet research!</p>

<div class="col-xs-12 bg-primary">
    <p><em>Check for the following quality criteria when you have found a document on the Internet (click on column)</em></p>

    <div class="col-xs-2 col-xs-offset-1 text-center credit-subject" id="credit-purpose "> 
        <h4>Purpose</h4>
    </div>
    <div class="col-xs-2 text-center credit-subject" id="credit-source">
        <h4>Source</h4>
    </div>
    <div class="col-xs-2 text-center credit-subject" id="credit-content">
        <h4>Content</h4>
    </div>

    <div class="col-xs-2 text-center credit-subject" id="credit-reference">
        <h4>References</h4>                                                                                        
    </div>
    <div class="col-xs-2 text-center credit-subject" id="credit-quote">
        <h4>Citations</h4>                                                                                        
    </div>

    <div class="col-xs-10 col-xs-offset-1 credit-white" id="quote">
        <div class="credit credit-purpose">
            <p>Purpose of publication (target group, objectivity) - Who is the document addressed to? To the 
            scientific community, to interested laymen?</p>
        </div>

        <div class="credit credit-source">
            <p>Source (author, corporation, top domain level of the URL (.gov, .org)) - Was the document written 
            by a member of the University of Cambridge or by Max Müller from Musterhausen?</p>
        </div>

        <div class="credit credit-content">
            <p>Content (accuracy, verifiability, depth and breadth of information, timeliness) - When was the last
            update carried out? How detailed are the explanations?</p>
        </div>

        <div class="credit credit-reference">
            <p>References (references/links to other works, links to the page) - Are there any further references?
            Are statements proved by references?</p>
        </div>

        <div class="credit credit-quote">
            <p>Citation - Is this text quoted by other scientists?</p>
        </div>

    </div>
</div>

