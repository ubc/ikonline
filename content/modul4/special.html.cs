<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Kde mám tedy hledat? – Speciální vyhledávače</h2>

<p>A co teď? Jak najdete na internetu vědecké zdroje? Užitečnější než obecné vyhledávače jsou 
<?php get_popover('specialsearchengine','speciální vyhledávače');?>.
Tyto vyhledávače se většinou specializují na vybrané obsahy nebo typy dokumentů, ke kterým ale mají ucelené podklady.</p>

<p>Abyste takový vyhledávač našli, můžete buď napřed využít běžný vyhledávač nebo hledat v Databankovém Informačním systému DBIS 
(viz <a href="../modul2/index.html.cs">Modul 2</a>) Zadáte-li „vyhledávač“, zobrazí se Vám potřebné výsledky.</p>
