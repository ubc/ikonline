<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Příklady – speciální vyhledávače</h2>

<p>Představujeme Vám dva speciální vyhledávače Google Scholar a BASE:</p>

<table class="table table-responsive table-striped table-bordered">
    <thead>
        <tr class="info">
            <th><span class="sr-only">Aspekt</span></th>
            <th><a href="http://scholar.google.de" class="linkextern" target="blank">Google Scholar</a></th>
            <th><a href="http://base-content.net" class="linkextern" target="blank">BASE (Bielefeld Academic Search Engine)</a></th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td class="info">Vlastník</td>
        <td><strong>Komerční vlastník: </strong>Google</td>
        <td><strong>Veřejný vlastník:</strong> Univerzitní knihovna Bielefeld</td>
    </tr>
    <tr>
        <td class="info">Popis</td>
        <td>
            <ul>
                <li>vhodný především pro vyhledávání vědecké literatury, např. názvy děl, články z časopisů, technical papers</li>
                <li>kooperuje s vědeckými nakladatelstvími a má tak přístup k obsahům zpoplatněných zdrojů na Deep Webu</li>
                <li>obsahuje 100-106 Mil. dokumentů, vyhodnocuje seminární, magisterské, diplomové, bakalářské, masterské a doktorské 
                práce, navíc i knihy, shrnutí a články z akademických nakladatelství, profesních spolků, preprintů, univerzit a dalších 
                vzdělávacích zařízení. (viz <a href="https://scholar.google.de/int/de/scholar/about.html" class="linkextern" target="blank">
                    https://scholar.google.de/int/de/scholar/about.html</a>)</li>
            </ul>
        </td>
        <td>
            <ul>
                <li>vyhledávač ve smyslu Open Access – volně dostupných vědeckých dokumentů</li>
                <li>přístup na dokumenty z Deep Webu</li>
                <li<indexuje kolem 80 miliónů dokumentů, vysokoškolských prací, preprintů, článků z časopisů a dalších přes 4000 dokumentových serverů (stav 2016)</li>
                <li><a href="https://www.base-search.net/about/de/index.html" class="linkextern" target="blank">
                    https://www.base-search.net/about/de/index.html</a></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td class="info">Výhody</td>
        <td>
            <ul>
                <li>v rámci kampusu obdržíte odkaz na literaturu z fondu UK Chemnitz</li>
            </ul>
        </td>
        <td>
            <ul>
                <li>omezení vyhledávání dle autorů, klíčových slov, DDC klasifikace, roku vydání, zdrojů, jazyků a typu dokumentu</li>
                <li>vyhledávání dle DDC klasifikace a typu dokumentu</li>
                <li>redakční výběr zdrojů (seznam zdrojů je k náhledu)</li>
                <li>70% dokumentů je přístupných v plném textu</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td class="info">Nevýhody</td>
        <td>
            <ul>
                <li>příliš bibliografických odkazů, málo plných textů (45-50%)</li>
                <li>žádný seznam zahrnutých zdrojů</li>
                <li>úplnost není zaručená</li>
                <li>omezené možnosti přesného vyhledávání</li>
                <li>špatně indexované dokumenty</li>
                <li>výsledky vyhledávání se liší a nejsou použitelné</li>
            </ul>
        </td>
        <td>
            <ul>
                <li>vyhledávání je omezeno na metadata dokumentů, plné texty nejsou prohledávány</li>
            </ul>
        </td>
    </tr>
    </tbody>
</table>
