<?php

$files = array (
    'de' => array(
        'index' => 'Willkommen',
        'sources' => array(
            'sources' => 'Informationsquellen im Internet',
            'searchengines' => 'Allgemeine Suchmaschinen',
            'surface' => 'Surface, Deep & Darknet',
            'special' => 'Spezialsuchmaschinen',
            'special2' => 'Beispiele Spezialsuchmaschinen'
        ),
        'subjectportal' => array(
            'subjectportal' => 'Fachportale',
            'importantportal' => 'Wichtige Fachportale',
            'portal_ubc' => 'Fachportale der UB Chemnitz'
        ),
        'technology' => array(
            'technology' => 'Suchtechniken',
            'technology2' => 'Beispiele'
        ),
        'view' => 'Sichten & Modifizieren',
        'test' => 'Abschlusstest'
    ),
    'cs' => array(
        'index' => 'Vítejte',
        'sources' => array(
            'sources' => 'Informační zdroje na internetu',
            'searchengines' => 'Obecné vyhledávače',
            'surface' => 'Surface, Deep & Darknet',
            'special' => 'Speciální vyhledávače',
            'special2' => 'Příklady speciálních vyhledávačů'
        ),
        'subjectportal' => array(
            'subjectportal' => 'Odborné stránky',
            'importantportal' => 'Důležité odborné stránky',
            'portal_ubc' => 'Odborné stránky UK Chemnitz'
        ),
        'technology' => array(
            'technology' => 'Techniky vyhledávání',
            'technology2' => 'Příklady'
        ),
        'view' => 'Prohlédnutí a modifikace',
        'test' => 'Závěrečný test (DE)'
    ),
    'en' => array(
        'index' => 'Welcome',
        'sources' => array(
            'sources' => 'Sources of information on the Internet',
            'searchengines' => 'General search engines',
            'surface' => 'Surface, Deep & Darknet',
            'special' => 'Special search engines',
            'special2' => 'Examples for special search engines'
        ),
        'subjectportal' => array(
            'subjectportal' => 'Subject portals',
            'importantportal' => 'Important subject portals',
            'portal_ubc' => 'Subject portals of the University Library'
        ),
        'technology' => array(
            'technology' => 'Search techniques',
            'technology2' => 'Examples'
        ),
        'view' => 'Review & Modify',
        'test' => 'Test'
    )
);

?>

