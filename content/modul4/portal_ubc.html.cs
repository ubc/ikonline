<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Je libo ještě víc? – Odborné stránky Univerzitní knihovny Chemnitz</h2>

<p>Univerzitní knihovna Chemnitz má sdružené různé portály.
<a href="https://www.tu-chemnitz.de/ub/suchen-und-finden/fachrecherche/fachportale/index.html" class="linkextern" target="blank">
    Najděte jednoduše svůj vlastní obor</a>.</p>

<p>Na těchto stránkách naleznete:</p>
<ul>
    <li>informace o službách a možnostech rešerší, které Univerzitní knihovna nabízí</li>
    <li>nejdůležitější odborné odkazy k nabídkám mimo Univerzitní knihovnu</li>
    <li>kontaktní údaje a kontaktní osobu v Univerzitní knihovně.</li>
</ul>
