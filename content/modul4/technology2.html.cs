<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
<h2 class="text-primary">Příklady pro vyhledávací techniky</h2>

<table class="table table-striped">
    <thead>
        <tr class="info">
            <th><span class="sr-only">Vyhledávací technika</span></th>
            <th>Google Scholar</th>
            <th>BASE</th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td>spojení AND</td>
        <td>mezerník</td>
        <td>mezerník</td>
    </tr>
    <tr>
        <td>spojení OR</td>
        <td>OR</td>
        <td>Do závorky: (pojem A pojem B)</td>
    </tr>
    <tr>
        <td>spojení NOT</td>
        <td>znaménko mínus: -</td>
        <td>znaménko mínus: -</td>
    </tr>
    <tr>
        <td>Vyhledávání frází</td>
        <td>""</td>
        <td>""</td>
    </tr>
    <tr>
        <td>Truncation</td>
        <td>nelze</td>
        <td>*</td>
    </tr>
    <tr>
        <td>Search History</td>
        <td>není v nabídce</td>
        <td>je v nabídce</td>
    </tr>
    <tr>
        <td>Alerting</td>
        <td>je v nabídce</td>
        <td>je v nabídce</td>
    </tr>
    </tbody>
</table>

<p>Narazíte i na další operátory. Zadáte-li „filetyp“, můžete například při hledání v Google Scholar hledat určitý typ dokumentu nebo „intitle“ určitý název.
Všechny vyhledávače mají nápovědy a vysvětlivky, ve kterých najdete odpovědi na Vaše otázky:</p>
<ul>
    <li><a href="https://scholar.google.de/intl/de/scholar/help.html" class="linkextern" target="blank">Google Scholar</a></li>
    <li><a href="https://www.base-search.net/about/de/help.php" class="linkextern" target="blank">BASE</a></li>
</ul>
