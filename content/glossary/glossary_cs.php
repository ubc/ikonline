<?php

//File contains all terms for glossary 

//Explanation of array keys
//main ->  refer to the main term (in German s.)
//link -> refer to further terms ( in German s.a.)
//link could contains several terms which must be seperated by ' , '

//21.06.2018, Katrin Otto 

$see = 'viz ';

$seeAlso = 'viz také ';

$glossary = array(
    '5-steps-model' => array (
        'title' => 'Model pěti kroků',
        'text' => 'Postup využití slovních hesel a klíčových slov při tématické rešerši.'
    ),
    'distanceoperator' => array(
        'title' => 'Odstupový operátor',
        'main' => 'sousední operátor'
    ),
    'abstract' => array(
        'title' => 'Abstrakt, krátký referát',
        'text' => 'Krátký údaj o obsahu textu, který byl zveřejněn v časopise nebo sbírce, zpravidla napsán autorem samotným.'
    ),
    'abstractdatabase' => array(
        'title' => 'Databáze abstraktů',
        'text' => 'Obsahuje kromě bibliografických údajů krátká shrnutí/obsahy doložených zveřejněných textů.'
    ),
    'accessionnumber' => array(
        'title' => 'Přírůstkové číslo, akcesní číslo, accession number',
        'text' => 'Číslo, které je přiděleno každému dokumentu v databázi, většinou chronologického rázu dle přijetí dokumentu do databáze.'
    ),
    'advancedsearch' => array(
        'title' => 'Advanced Search',
        'main' => 'expertsearch'
    ),
    'alert' => array(
        'title' => 'Alerting',
        'text' => 'Zaslání obsahů časopisů „Table of Contents (TAC) Alerts“ nebo seznamu odkazů "Key-Word Alert" k již uloženému vyhledávání; je zasílán emailem v pravidelném časovém odstupu.'
    ),
    'generalbibliography' => array(
        'title' => 'Obecná bibliografie, Universal Bibliography',
        'text' => 'Soubor publikací bez ohledu na odborný obsah dle jazykové oblasti (národní bibliografie) nebo regionálních aspektů (regionální bibliografie).',
        'link' => 'bibliography'
    ),
    'aquisition' => array(
        'title' => 'Návrh na pořízení',
        'text' => 'Pokud ve fondu knihovny postrádáte nějakou knihu, která není v katalogu, můžete pomocí formuláře podat návrh na její pořízení. Rozhodnutí, zda bude zakoupena, je v rukou knihovny.'
    ),
    'arpanet' => array(
        'title' => 'Arpanet',
        'text' => 'Arpanet (=Advanced Research Project Agency Network) je předchůdcem dnešního internetu.'
    ),
    'asap' => array(
        'title' => 'ASAP Article (As soon as publishable)',
        'main' => 'earlyview'
    ),
    'outputformat' => array(
        'title' => 'Formát vydání',
        'text' => 'Formát, ve kterém jsou k dispozici záznamy v databázi (např. plný formát, citační záznam).'
    ),
    'basicsearch' => array(
        'title' => 'Basic Search',
        'main' => 'simplesearch'
    ),
    'bibliography' => array(
        'title' => 'Bibliografie, Bibliography',
        'text' => 'Seznam samostatně a nebo nesamostatně vydané literatury nezávislý na fondu jedné nebo více knihoven.',
        'link' => 'subjectbibliography , generalbibliography'
    ),
    'bibliographicdata' => array(
        'title' => 'Bibliografické údaje',
        'text' => 'Údaje k názvu díla, autorovi, nakladatelství, ročníku časopisu atd., kterými je publikace jednoznačně definována a musí být uvedena při 
        objednávce a v seznamu literatury',
        'link' => 'metadata'
    ),
    'bibliographicdatabase' => array(
        'title' => 'Bibliografická databáze, Bibliographic Database',
        'text' => 'Dokládá publikace s bibliografickými údaji, částečně je rozšířena o klíčová slova',
        'link' => 'abstractdatabase , referencedatabase'
    ),
    'libraryconsortium' => array(
        'title' => 'Asociace knihoven, Library Consortium',
        'text' => 'Spojení zpravidla vědeckých knihoven za účelem společné dokumentace knihovnických fondů.'
    ),
    'bvb_term' => array(
        'title' => 'Bavorský knihovnický svaz / Bibliotheksverbund Bayern (BVB)',
        'text' => 'Spolek přednostně vědeckých Bavorských knihoven za účelem dokumentace knihovnických fondů.'
    ),
    'picturedatabase' => array(
        'title' => 'Obrázková databáze, Image Database',
        'text' => 'Obsahuje informace v obrazovém formátu.'
    ),
    'booleanoperator' => array(
        'title' => 'Booleovské operátory',
        'text' => 'Slouží k logickému propojení pojmů v rámci vyhledávání:
        <ul>
        <li>AND – tvoří průnik; hledáte-li pojmy A a B, naleznete výsledky, ve kterých je A a B,</li>
        <li>OR – tvoří množinu; hledáte-li pojmy A nebo B, neleznete výsledky, ve kterých je A nebo B a nebo A a B</li>
        <li>NOT – vylučuje; hledáte-li pojem A a ne B, naleznete výsledky, ve kterých je A a ne B.</li>
        </ul>'
    ),
    'bvb' => array(
        'title' => 'BVB',
        'main' => 'bvb_term'
    ),
    'campuslicence' => array(
        'title' => 'Licence pro kampus',
        'text' => 'Licence pro přístup k elektronickým médiím omezená na oblast univerzitního kampusu. Přístup je možný pouze z počítačů, které mají 
        univerzitní IP adresy.',
        'link' => 'vpn'
    ),
    'darkweb' => array(
        'title' => 'Dark Web',
        'text' => 'Oblast v internetu, která je dostupná pouze díky právům k nahlédnutí do určitých dokumentů. Pro přístup zpravidla potřebujete heslo. Dokumenty v 
        Dark Webu jsou většinou propojovány manuálně.'
    ),
    'database' => array(
        'title' => 'Databáze',
        'text' => 'Elektronická forma katalogu, bibliografie, sbírky dat, nebo textů (slovník, pracovní vydání). Formálně se dělí databáze na online (většina) 
        a databáze na CD nebo DVD.'
    ),
    'databasehost' => array(
        'title' => 'Poskytovatel databáze, Host',
        'text' => 'Umožňuje nahlednutí do různých databází ze společného rozcestníku, většinou za poplatek.'
    ),
    'dbis_term' => array(
        'title' => 'Databankový informační systém (DBIS)',
        'text' => 'Kooperativní servis k využití kompletních databází; část je dostupná na internetu zdarma. V současné době čítá 5000 záznamů, z toho 1000 jich je 
        volně dostupných na internetu.'
    ),
    'dbis' => array(
        'title' => 'DBIS',
        'main' => 'dbis_term'
    ), 
    'deepweb' => array(
        'title' => 'Deep Web',
        'link' => 'invisibleweb'
    ),
    'descriptor' => array(
        'title' => 'Deskriptory',
        'text' => 'Všechny pojmy zanesené do tezaurusu patřící do stejného oboru; používají se k indexaci publikace.',
        'link' => 'subjectheading'
    ),
    'dnb' => array(
        'title' => 'Německá národní knihovna',
        'text' => 'Německá národní knihovna shromažďuje všechny monografie, časopisy, mapy, atlasy, disertace a habilitace v tištěné a elektronické podobě, které vyšly v Německu. 
        Německé publikace, které vyšly v zahraničí, překlady z němčiny do jiných jazyků a cizojazyčná germanica. Součástí je Německý hudební archiv v Berlíně.'
    ),
    'doi_term' => array(
        'title' => 'Digital Object Identifier (DOI)',
        'text' => 'Sled znaků pro jednoznačnou identifikaci digitálních objektů (srovnatelné s ISBN pro knihy; tím mohou být přesně označeny i články v časopisech, výňatky ze sborníků a.j.).'
    ),
    'doi' => array(
        'title' => 'DOI',
        'main' => 'doi_term'
    ),
    'deliveryservice' => array(
        'title' => 'Služby pro doručení dokumentů',
        'text' => "Poskutnutí publikace přímou cestou z knihovny k uživateli; Objednávka / doručení jsou většinou podávány v elektronické podobě: Články jsou poskytovány jako kopie nebo 
        zaslány emailem, knihy jsou většinou zasílány v originále; jedná se o placenou službu!<br>
        Známé komerční služby jsou např. Subito nebo Ingenta. UB Chemnitz kromě toho nabízí svým zaměstnancům a studentům 
        <a target='blank' class='linkextern' href='https://www.tu-chemnitz.de/ub/service/anmelden-ausleihe/dokulief/dokuliefformular.php'>službu inhouse</a>.
        Zasílá tak publikace v rámci univerzity."
    ),
    'ebook' => array(
        'title' => 'E-book – Electronic Book, elektronická kniha',
        'text' => 'Kniha v “elektronické podobě“, jejíž kapitoly jsou tvořeny jednotlivými dokumenty s možností stažení. Některá nakladatelství omezují objem dokumentů, které mohou 
        být staženy. E-booky najdete v katalogu vaší knihovny.'
    ),
    'ejournal' => array(
        'title' => 'Internetový časopis, online magazín, Electronic Journal',
        'text' => 'Časopis v “elektronické podobě”, jehož články jsou k dispozici ke stažení. Internetové časopisy jsou k dispozici ve vaší knihovně, případně v Databázi pro elektronické 
        časopisy/Elektronische Zeitschriftendatenbank (EZB). Můžete si prohlédnout předplacené časopisy a stáhnout jednotlivé články.'
    ),
    'earlyview' => array(
        'title' => 'Early View, ASAP Article, Online First',
        'text' => 'Články jsou před vytištěním zveřejněny online, možnost citace díky DOI.'
    ),
    'simplesearch' => array(
        'title' => 'Jednoduché, rychlé vyhledávání',
        'text' => 'Často je k dispozici pouze jedno vyhledávací pole; Zpravidla se vyhledává ve všech vyhledávacích polích (omezení pomocí field tags); použití operátorů je také možné.'
    ),
    'ezb_term' => array(
        'title' => 'Databáze pro elektronické časopisy/Elektronische Zeitschriftendatenbank (EZB)',
        'text' => 'Služba založená na kooperaci více než 300 knihoven s cílem zpřístupnit uživatelům vědecké čásopisy vycházející v elektronické podobě; obsahuje všechny časopisy, které nabízí 
        články ve formě plného textu. Knihovna TU Chemnitz má databázi integrovanou ve svém katalogu.'
    ),
    'expandsearch' => array(
        'title' => 'Expanded Search',
        'main' => 'expertsearch'
    ),
    'expertsearch' => array(
        'title' => 'Rozšířené vyhledávání, vyhledávání pro experty',
        'text' => 'Vyhledávací maska v databázích umožňující komplexní zadání.'
    ),
    'ezb' => array(
        'title' => 'EZB',
        'main' => 'ezb_term'
    ),
    'subjectbibliography' => array(
        'title' => 'Odborná bibliografie, Subject Bibliography',
        'text' => 'Seznam samostatně nebo nesamostatně vydané literatury k určité oblasti nezávisle na tom, zda se nachází v jedné nebo vice knihovnách.'
    ),
    'subjectportal' => array(
        'title' => 'Odborné internetové stránky',
        'text' => 'Selekce obsahově cenných internetových stránek a systematická struktura vybraných odkazů.',
        'link' => 'portal'
    ),
    'subjectthesaurus' => array(
        'title' => 'Odborný tezaurus',
        'main' => 'thesaurus'
    ),
    'subjectdatabase' => array(
        'title' => 'Databáze faktů, Fact Database',
        'text' => 'Obsahuje fyzikálně-chemické údaje, obchodní zprávy, statistiky a dokládá field Tags.'
    ),
    'interlibraryloan' => array(
        'title' => 'Meziknihovní výpůjční služba, interlibrary loan',
        'text' => 'Služba, díky které mohou být dokumenty (knihy, články z časopisů a.j.), které nemá knihovna v nabídce, obstarány v jiné knihovně.'
    ),
    'fieldtag' => array(
        'title' => 'Field Tags',
        'text' => 'Mohou ve vyhledávání určit, ve kterých polích má být vyhledáváno: např. název autora, název časopisu, klíčové slovo, předmětové heslo.'
    ),
    'openaccessholding' => array(
        'title' => 'Volný výběr',
        'text' => 'Zde si mohou uživatelé sami vzít knihu z regálu a vypůjčit si ji.'
    ),
    'freetextsearch' => array(
        'title' => 'Volné vyhledávání',
        'text' => 'Vyhledávání pojmu nebo jména je provedeno v několika polích najednou. Zadává se s dodáním „AllFields“. V jakých polích pak vyhledávání probíhá závisí na databázi samotné.'
    ),
    'gbv' => array(
        'title' => 'GBV',
        'main' => 'gbv_term'
    ),
    'gnd' => array(
        'title' => 'Společný soubor norem/Gemeinsame Normdatei (GND)',
        'text' => 'Společnýsystém klíčových slov (soubor norem), který je používán k vyhledávání obzvláště v knihovnách. OGND nebol „Společný soubor norem online“ nabízí přístup ke 
        klíčovým slovům a jménům autorů z databáze GND; jednoznačně popisuje obsah dokumentu nebo textu.'
    ),
    'gbv_term' => array(
        'title' => 'Společný svaz knihoven/Gemeinsamer Bibliotheksverbund (GBV)',
        'text' => 'Sloučení vědeckých knihoven spolkových zemí Bremen, Hamburg, Mecklenburg-Vorpommern, Niedersachsen, Sachsen-Anhalt, Schleswig-Holstein, Thüringen a Statní knihovny v 
        Berlíně – Nadace pruského kulturního dědictví za účelem sloučení fondů.'
    ),
    'googlescholar' => array(
        'title' => 'Google Scholar',
        'text' => 'Komerční vyhledávač vědeckých publikací.'
    ),
    'greyliterature' => array(
        'title' => 'Šedá literatura, Grey Literature',
        'text' => 'Literature, která vyšla a není listovaná v knihkupectvích, není téměř v žádných bibliografiích a katalzích a proto není snadno dohledatelná, např. 
        kongresové zprávy, průběžné zprávy, obchodní zprávy.'
    ),
    'thesis' => array(
        'title' => 'Vysokoškolská práce',
        'text' => 'Publikace sepsána studentem vysoké školy jako výsledek jeho studia a bádáním např. studentské závěrečné práce, disertace, habilitace.'
    ),
    'idiom' => array(
        'title' => 'Idiom',
        'text' => 'Ustálené spojení slov či frazém, jehož význam se nedá určit rozložením na jednotlivé významové jednotky (např. vzít roha ve smyslu utéci).'
    ),
    'ipac' => array(
        'title' => 'Katalog v obrazech, IPAC (Image Public Access Catalog)',
        'text' => 'Nabídka konvenčních katalogů v digitální podobě; Katalogy jsou naskenovány jako obrázky (images); rešerše je možná jen v jedné dimenzi, dle vlastností naskenovaných 
        katalogů: Autor nebo název (u více než tří autorů).'
    ),
    'index' => array(
        'title' => 'Rejstřík, registr',
        'text' => 'Abecední seznam všech hledaných pojmů, např. rejstřík autorů.'
    ),
    'information' => array(
        'title' => 'Informace',
        'text' => 'Odvozeno od latinského informare – vzdělávat, stvořit; je potenciální nebo vlastní použitelná a používaná znalost. Sémiotika vykládá pojem informace jako cíleně 
        orientovaná data, která rozšiřují vědomosti.'
    ),
    'informationliteracy' => array(
        'title' => 'Informační vzdělávání, informační gramotnost, information literacy',
        'text' => 'Představuje v dnešní moderní a velmi dynamické společnosti klíčovou kompetenci, jež má pomoci s častým problémem. Patři do oblasti tzv. soft skills a zahrnuje řadu schopností, 
        jež jedinci umožní kompetentní, eficientní a zodpovědné zacházení s informacemi.Tyto schopnosti se vztahují na všechny aspekty rozeznání potřeby dané informace, její vyhledání, 
        organizaci, cílenou selekci díky analýze a evaluaci pomocí shromáždění infofrmací a jejich cílenou prezentaci.'
    ),
    'interlibrary_loan' => array(
        'title' => 'Interlibrary Loan',
        'main' => 'interlibraryloan'
    ),
    'isbn_term' => array(
        'title' => 'International Standard Book Number (ISBN)',
        'text' => 'Mezinárodní identiikační číslo pro knihy; sestává z 10 nebo 13 číslic, příklad ISBN s 10 číslicemi: 3-609-1476-3.'
    ),
    'issn_term' => array(
        'title' => 'International Standard Serial Number (ISSN)',
        'text' => 'Mezinárodní identifikační číslo periodické publikace, např. časopisů. Jedná se o osmimístné číslo ve dvou čtyřznakových skupinách. Např.: 0113-4763.'
    ),
    'internet' => array(
        'title' => 'Internet, Interconnected Network',
        'text' => 'Je celosvětová síť na sobě nezávislých sítí.Slouží ke komunikaci a výměně informací. Každý počítač v síti může teoreticky komunikovat s jakýmkoli jiným počítačem.'
    ),  
    'invisibleweb' => array(
        'title' => 'Invisible Web, Deep Web, Hidden Web',
        'text' => 'Je část internetu, která není dosažitelná rešerší v běžném vyhledávači. Deep Web je z větší části složen z  tématicky specifických databází 
        (odborných databází) a webových stránek, které jsou dohledatelné pouze specifickým vyhledáváním v databázích.'
    ),
    'isbn' => array(
        'title' => 'ISBN',
        'main' => 'isbn_term'
    ),
    'issn' => array(
        'title' => 'ISSN',
        'main' => 'issn_term'
    ),
    'kvk_term' => array(
        'title' => 'Virtuální katalog Karlsruhe/ Karlsruher Virtueller Katalog (KVK)',
        'text' => ' Umožňuje vyhledávání v několika německojazyčných a mezinárodích online katalozích popř. knihovnických sdruženích najednou; m.j. umožňuje i rešerši v databázi 
        časopisů/ Zeitschriftendatebank (ZDB) a knihkupeckých katalozích.'
    ),
    'catalog' => array(
        'title' => 'Katalog',
        'text' =>'Katalog je seznam všech médií obsažených ve fondu knihovny. V moderním katalogu naleznete také elektronická média a časopisy a čím dál více 
        se v něm objevují odkazy na články z volných a licencovaných databází plných textů.'
    ),
    'classifikation' => array(
        'title' => 'Klasifikace, Classification',
        'main' => 'system'
    ),
    'contextoperator' => array(
        'title' => 'Kontextový operátor',
        'main' => 'proximityoperator'
    ),
    'shortpresentation' => array(
        'title' => 'Krátký referát',
        'main' => 'abstract'
    ),
    'kvk' => array(
        'title' => 'KVK',
        'main' => 'kvk_term'
    ),
    'lexicaldatabase' => array(
        'title' => 'Lexikální databáze',
        'text' =>'Elektronická forma příručky.'
    ),
    'link' => array(
        'title' => 'Odkaz',
        'main' => 'url'
    ),
    'literaturedepartment' => array(
        'title' => 'Oddělení literatury',
        'text' => 'Popisuje umístění a současně dostupnost média. Rozdělujeme volný výběr (možnost výpůjčky, volně dostupný), depozitář (většinou možnost výpůjčky, pro uživatele nedostupný) 
        a čtecí sál (volně dostupný, bez možnosti výpůjčky).'
    ),
    'literaturedatabase' => array(
        'title' => 'Databáze literatury',
        'main' => 'referencedatabase'
    ),
    'stackcollection' => array(
        'title' => 'Depozitář',
        'text' => 'Oblast knihovny, která je dostupná pouze pro zaměstnance a slouží pro uchování starších vydání knih a časopisů a nestandardních formátů knih. 
        Literatura z depozitáře musí být v katalogu objednána pomocí tlačítka \'depozitář\''
    ),
    'mask' => array(
        'title' => 'Maskování',
        'text' => 'Díky zadaní maskovacího znaku *.?.$ (wildcards) do slova mohou být do vyhledávání zahrnuty různé pravopisné formy (např. Tos*ana = Toskana a Toscana).'
    ),
    'metadata' => array(
        'title' => 'Metadata',
        'text' => 'Jedná se o údaje o údajích. Metadata ke knize jsou např. jméno autora, ISBN, vydání nebo nakladatelství.',
        'link' => 'bibliographicdata'
    ),
    'metasearchengine' => array(
        'title' => 'Metavyhledávače',
        'text' => 'Zvláštní forma vyhledávače; požadavek paralelně komunikuje do databází dalších běžných vyhledávačů a odstraňuje zdvojené výsledky.'
    ),
    'monarchqucosa' => array(
        'title' => 'MONARCH-QUCOSA (Multimedia Online Archiv Chemnitz)',
        'text' => 'Dokumentová a publikační služba TU Chemnitz; slouží k archivaci vysokoškolských publikací. Hosting zajišťuje QUCOSA Dresden.'
    ),
    'monograph' => array(
        'title' => 'Monografie',
        'text' => 'Publikace, která se rozsáhle věnuje jednomu tématu, většinou psána jedním autorem.'
    ),
    'proximityoperator' => array(
        'title' => 'Sousední operátor, odstupový operator, Proximity operator, Adjacency Operator',
        'text' => 'Definuje odstup mezi jednotlivými vyhledávanými pojmy: zda mají být vedle sebe nebo v rámci definovaného odstupu, věty nebo pole.'
    ),
    'reference' => array(
        'title' => 'Record',
        'main' => 'record'
    ),
    'norm' => array(
        'title' => 'Norma, právní norma',
        'text' => 'Právní ustanovení o například velikosti žárovky, které je platné pro všechny možné zájemce. UK Chemnitz má ve fondu m.j. kompletní seznam norem, ASTM standardy 
        (American Society for Testing and Materials) a předpisy VDI (Verein deutscher Ingenieure.)'
    ),
    'onlinefirst' => array(
        'title' => 'Online First',
        'main' => 'earlyview'
    ),
    'opac' => array(
        'title' => 'OPAC (Online Public Access Catalogue)',
        'text' => 'Katalog fondu knihovny založený na bázi webu 2.0.'
    ),
    'openaccess' => array(
        'title' => 'Open Access',
        'text' => 'Volný bezplatný přístup – Je snahou o volný přístup (bezplatný a bez licencí) ke zpravidla vědecké literatuře a materiálům.'
    ),
    'operator' => array(
        'title' => 'Operator',
        'main' => 'booleanoperator , proximityoperator'
    ),
    'patent' => array(
        'title' => 'Patent',
        'text' => 'Časově omezený monopol udělený vynálezci státem za účelem užívání vlastního vynálezu; v Patentním informačním centru/Patentinformationszentrum (PIZ) lze patenty vyhledat.'
    ),
    'peerreview' => array(
        'title' => 'Peer Reviewing, Posudek',
        'text' => 'Postup, při kterém oponent posuzuje např. vědecké články.'
    ),
    'phrase' => array(
        'title' => 'Fráze',
        'text' => 'Několik slov v předem stanovené posloupnosti, které spolu utváří jeden pojem (např. „Sasko-polská unie“)'
    ),
    'phrasesearch' => array(
        'title' => 'Vyhledávání frází',
        'text' => 'Vyhledávání pojmu, který se skládá z několika slov. Pojem se zadává do uvozovek.'
    ),
    'plagiarism' => array(
        'title' => 'Plagiát',
        'text' => 'Plagiát je protiprávní převzetí a zpracování cizích textů v jakékoli formě bez udání zdroje.'
    ),
    'portal' => array(
        'title' => 'Portál',
        'text' => 'Centrální přístup, díky kterému jsou k dipozici inidividuální informace a služby. Portály určené pro speciální obory jsou nazývány Odborné portály.'
    ),
    'primarysource' => array(
        'title' => 'Primární zdroje, primary source ',
        'text' => 'Originální literatura, ve které jsou uvedeny aktuální poznatky a nejnovější výsledky.'
    ),
    'qucosa' => array(
        'title' => 'QUCOSA',
        'text' => "QUCOSA je hostingovým partnerem publikačního serveru MONARCH v UK Chemnitz, viz <a href='#monarchqucosa'>MONARCH-QUCOSA</a>"
    ),
    'quicksearch' => array(
        'title' => 'Quicksearch',
        'main' =>'simplesearch'
    ),
    'ranking' => array(
        'title' => 'Ranking, pořadí vyhledávání',
        'text' => 'Zobrazení výsledků vyhledávání třízené dle relevance (důležitost dle zadání); výpočet relevance je v každé databázi jiný'
    ),
    'record' => array(
        'title' => 'Záznam, Record',
        'text' => 'Dokument v databázi'
    ),
    'redilinks' => array(
        'title' => 'ReDI»Links, Odkaz na plné texty',
        'text' => 'Automatizovaný service tool k přezkoušení dostupnosti dokumentů z databáze, například. v rámci knihovny.'
    ),
    'research' => array(
        'title' => 'Rešerše',
        'text' => 'Cílené vyhledávání informací v databázi nebo na internetu.'
    ),
    'refereedjournal' => array(
        'title' => 'Refereed Journal, Časopis s posudkem',
        'text' => 'Časopis, který byl posouzen jinými vědci.',
        'link' => 'peerreview'
    ),
    'referencedatabase' => array(
        'title' => 'Referenční databáze, Databáze literatury, Reference Database',
        'text' => 'Obsahuje údaje o pulikacích s uvedením názvu autora, místa vydání a data, popř. název časopisu nebo knihy a počet stran, částečně jsou uvedena klíčová slova a abstrakty.',
        'link' => 'bibliographicdatabase , abstractdatabase'
    ),
    'rvk_term' => array(
        'title' => 'Řezenská sdružená klasifikace (RSK)/ Regensburger Verbundklassifikation (RVK)',
        'text' => 'Systematika uspořádání knihovních fondů ve volném výběru; kombinace písmen a znaků; vyvinutá UK Řezno/ Regensburg. V katalogu lze toto dělení využít pro 
        tématické vyhledávání literatury, např. ST 250 (programovací jazyky).'
    ),
    'register' => array(
        'title' => 'Registr',
        'main' => 'index'
    ),
    'rss' => array(
        'title' => 'RSS',
        'text' => 'Rich Site Summary nebo nově: Real Simple Syndication; možnost sledování novinek a upozonění, jakýsi „News Ticker“ na webu. Program, který sleduje pomocí RSS-Reader 
        zpravodajské agentury, online noviny a blogy a hlásí nové příspěvky ve formě nadpisu s krátkým popisem. RSS Reader je součástí vyhledávače.'
    ),
    'rvk' => array(
        'title' => 'RVK',
        'main' => 'rvk_term'
    ),
    'subjectheading' => array(
        'title' => 'Klíčové slovo',
        'text' => 'Jedná se o pojem k pokud možno krátkému a úplnému popisu věcného obsahu publikace; nemusí být součástí názvu publikace.'
    ),
    'subjectheadinglist' => array(
        'title' => 'Předmětový heslář',
        'text' => 'Abecední seznam pojmů poukazujících na obsah; používá se k indexaci publikace při ukládání do databáze a pro vyhledávání; určuje jednotnou formu psaní'
    ),
    'quick_search' => array(
        'title' => 'Rychlé vyhledávání',
        'main' => 'simplesearch'
    ),
    'search_history' => array(
        'title' => 'Search History',
        'main' => 'searchhistory'
    ),
    'secondarysource' => array(
        'title' => 'Sekundární zdroje, Sekundární literatura',
        'text' => 'Seznam dokládající zdroje literatury v originále, například bibliografie, časopisy a příručky.'
    ),
    'independentlypublishedliterature' => array(
        'title' => 'Samostatně vydaná literatura',
        'text' => 'Literatura, která vychází jako samostatná jednotka a je tak vedena i v knihovnickém katalogu, např. monografie, disertace.'
    ),
    'semester' => array(
        'title' => 'Semestrální aparáty',
        'text' => 'Sbírka knih, které přípravil vyučující pro účastníky zvoleného semináře. Výpůjčka knih ze semestrálního aparátu není během semestru možná. V elektronickém 
        semestrálním aparátu mohou být (vyučujícím) vloženy dokumenty do chráněné online oblasti.'
    ),
    'specialsearchengine' => array(
        'title' => 'Speciální vyhledávače',
        'text' => 'Speciální vyhledávače se omezují na vybrané obsahy a typy dokumentů na internetu, ke kterým mají ucelené podklady'
    ),
    'keyword' => array(
        'title' => 'Předmětová hesla',
        'text' => 'Slovo vyjadřující obsah z názvu či abstraktu publikace.'
    ),
    'searchservice' => array(
        'title' => 'Vyhledávací služba',
        'text' => 'Kromě vyhledávačů existují i katalogy a další nástroje zpracovávané speciální redakcí. S jejich pomocí mohou být nalezeny obsahy na internetu, které hledáte. 
        Většinou pracují na principu robota nebo katalogu; při použití operátorů mohou být zadány komplexní pojmy.'
    ),
    'searchhistory' => array(
        'title' => 'Historie vyhledávání, Search History',
        'text' => 'Protokol vyhledávaných výrazů a výsledků v rámci vyhledávání v databázi.'
    ),
    'searchengine' => array(
        'title' => 'Vyhledávač',
        'text' => 'Služba, která v rámci Word Wide Webu vyhledává dokumenty a webové stránky k zadanému heslu (předmětové heslo). Základ tvoří automaticky vytvořený index.'
    ),
    'searchtemplate' => array(
        'title' => 'Vyhledávací maska',
        'text' => 'Formulář pro zadání několika hledených výrazů.'
    ),
    'searchoperator' => array(
        'title' => 'Vyhledávací operátor',
        'main' => 'booleanoperator , proximityoperator'
    ),
    'swb_term' => array(
        'title' => 'Knihovní síť jihozápadního Německa/ Südwestdeutscher Bibliotheksverbund (SWB)',
        'text' => 'Sdružení vědeckých knihoven ve spolkových zemích Bádensko Württenbersko, Sársko a Sasko za účelem propojení svých fondů).'
    ),
    'surfaceweb' => array(
        'title' => 'Surface Web',
        'text' => 'Běžně dostupná část internetu.'
    ),
    'swb' => array(
        'title' => 'SWB',
        'main' => 'swb_term'
    ),
    'system' => array(
        'title' => 'Systematika, klasifikace',
        'text' => 'pořádkový systém rozdělující oblast na třídy a podtřídy. Systematika poukazuje na soudržnost a členění všech  vědních oborů, přičemž ale vychází z 
        jednotlivých oborů a dělí je na menší a menší pojmy. Tyto různé skupiny a dělení obsahují určitou notaci.'
    ),
    'tertiarysource' => array(
        'title' => 'Terciální zdoj',
        'text' => 'Literární zdroj, ve kterém jsou shrnuty vědecké zprávy (např. učebnice, slovníky)'
    ),
    'topicanalyses' => array(
        'title' => 'Tématická analýza',
        'main' => '5-steps-model'
    ),
    'thesaurus' => array(
        'title' => 'Tezaurus',
        'text' => 'V informační vědě normovaný slovník sestávající ze systematicky seřazené sbírky stejného tématu.'
    ),
    'truncation' => array(
        'title' => 'Truncation',
        'text' => 'Pomocí znaků jako jsou *, ?, $ lze při vyhledávání krátit slovní kořeny. Krácení je možné zprava i zleva.',
        'link' => 'mask'
    ),
    'undependentpublishedliterature' => array(
        'title' => 'Nesamostatně vydaná literatura',
        'text' => 'Části (články, kapitoly) ze samostatně vydaných knih, které nejsou obzvlášť uvedeny v katalogu.'
    ),
    'url' => array(
        'title' => 'URL, odkaz',
        'text' => 'Zkratka pro „Uniform Resource Locator“ sloužící k přímémo odkazu na stránku.'
    ),
    'fulltextdatabase' => array(
        'title' => 'Databáze plných textů',
        'text' => 'Obsahuje všechny texty jedné publikace, např. články z časopisů, patenty nebo obsah celé knihy.'
    ),
    'vpn' => array(
        'title' => 'VPN',
        'text' => "S pomocí VPN se můžete připojit do chráněného webového rozhraní TU Chemnitz, viz. 
        <a target='blank' href='https://www.tu-chemnitz.de/urz/network/access/vpn.html'>zde</a>."
    ),
    'webcatalog' => array(
        'title' => 'Internetový katalog',
        'text' => 'Seznam internetových stránek o který se stará příslušná redakce. Každá nově vytvořená stránka je redakcí ohodnocena a zařazena na seznam.'
    ),
    'wildcard' => array(
        'title' => 'Wildcard',
        'text' => 'Zastoupení jednoho či více znaků ve vyhledávaném pojmu; běné jsou ?, *, $ a #.',
        'link' => 'truncation'
    ),
    'zdb' => array(
        'title' =>  'ZDB',
        'main' => 'zdb_term'
    ),
    'journal' => array(
        'title' => 'Časopis, žurnál',
        'text' => '<ul>
        <li>Tištěný časopis: minimálně dvakrát za rok vycházející periodikum v tištěné podobě</li>
        <li>Internetový časopis: elektronická forma časopisu</li>
        </ul>'
    ),
    'zdb_term' => array(
        'title' => 'Databáze pro časopisy/Zeitschriftendatenbank (ZDB)',
        'text' => 'Seznam děl a detialy k periodikům, tedy časopisům, novinám, výročním publikácím a.j.: doklad o tištěných časopisech a elektronických časopisech.'
    ),
    'accessnumber' => array(
        'title' => 'Přírůstkové číslo',
        'main' => 'accessionnumber'
    )
);

?>
