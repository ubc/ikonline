<?php

//File contains all terms for glossary in German

//Explanation of array keys
//main ->  refer to the main term (in German s.)
//link -> refer to further terms ( in German s.a.)
//link could contains several terms which must be seperated by ' , '

//21.06.2018, Katrin Otto 

$see = 's. ';

$seeAlso = 's.a. ';

$glossary = array(
    '5-steps-model' => array (
        'title' => '5-Schritte-Modell',
        'text' => 'Vorgehensweise bei der thematischen Literatursuche mit Stich- und Schlagwörtern'
    ),
    'distanceoperator' => array(
        'title' => 'Abstandsoperator',
        'main' => 'proximityoperator'
    ),
    'abstract' => array(
        'title' => 'Abstract, Kurzreferat',
        'text' => 'Die kurze Inhaltsangabe eines Aufsatzes, der in einer Zeitschrift oder in einem Sammelband veröffentlicht wird, in der Regel vom Autor verfasst.'
    ),
    'abstractdatabase' => array(
        'title' => 'Abstractdatenbank',
        'text' => 'Enthält neben bibliographischen Angaben kurze Zusammenfassung/Inhaltserschließung der nachgewiesenen Veröffentlichungen'
    ),
    'accessionnumber' => array(
        'title' => 'Accessionsnummer, Zugriffsnummer, Accession Number',
        'text' => 'Nummer, die jedem einzelnen Dokument in einer Datenbank zugewiesen wird, meist chronologisch nach Aufnahme der Dokumente in die Datenbank vergeben'
    ),
    'advancedsearch' => array(
        'title' => 'Advanced Search',
        'main' => 'expertsearch'
    ),
    'alert' => array(
        'title' => 'Alerting-Dienst',
        'text' => 'Zusenden von Inhaltsverzeichnissen von Zeitschriften „Table of Contents (TOC) Alerts“ oder Trefferlisten „Key-Word Alert“ zu einer einmal 
        gespeicherten Suchanfrage; erfolgt per E-Mail und in festgelegten Zeitabständen.'
    ),
    'generalbibliography' => array(
        'title' => 'Allgemeinbibliographie, Universal Bibliography',
        'text' => 'Zusammenstellung von Veröffentlichungen unabhängig vom fachlichen Inhalt nach Sprachraum (Nationalbibliographie) oder regionalen Aspekten (Regionalbibliographie)',
        'link' => 'bibliography'
    ),
    'aquisition' => array(
        'title' => 'Anschaffungsvorschlag',
        'text' => 'Wenn Sie ein Buch im Bestand der Universitätsbibliothek vermissen und es im Katalog nicht finden, können Sie über ein Formular der Universitätsbibliothek einen 
        Anschaffungsvorschlag machen. Die Entscheidung, ob ein Medium erworben wird, obliegt der Bibliothek'
    ),
    'arpanet' => array(
        'title' => 'Arpanet',
        'text' => 'Das Arpanet (=Advanced Research Project Agency Network) ist der Ursprung des heutigen Internet.'
    ),
    'asap' => array(
        'title' => 'ASAP Article (As soon as publishable)',
        'main' => 'earlyview'
    ),
    'outputformat' => array(
        'title' => 'Ausgabeformat',
        'text' => 'Format, in dem die Treffer einer Datenbankrecherche ausgegeben werden (z.B. Vollformat, Zitierformat)'
    ),
    'basicsearch' => array(
        'title' => 'Basic Search',
        'main' => 'simplesearch'
    ),
    'bibliography' => array(
        'title' => 'Bibliographie, Bibliography',
        'text' => 'Verzeichnis von selbstständig und/oder unselbstständig erscheinender Literatur unabhängig vom Bestand einer oder mehrerer Bibliotheken',
        'link' => 'subjectbibliography , generalbibliography'
    ),
    'bibliographicdata' => array(
        'title' => 'Bibliographische Daten',
        'text' => 'Angaben wie Titel, Autoren, Verlag, Zeitschriftenheft usw., die eine Publikation eindeutig beschreiben und bei Bestellungen und in 
        Literaturlisten angegeben werden müssen.',
        'link' => 'metadata'
    ),
    'bibliographicdatabase' => array(
        'title' => 'Bibliographische Datenbank, Bibliographic Database',
        'text' => 'Weist Veröffentlichungen mit bibliographischen Angaben nach, teilweise erweitert durch Schlagworte',
        'link' => 'abstractdatabase , referencedatabase'
    ),
    'libraryconsortium' => array(
        'title' => 'Bibliotheksverbund, Library Consortium',
        'text' => 'Zusammenschluss von i.d.R. wissenschaftlichen Bibliotheken zum gemeinsamen Nachweis ihrer Bestände'
    ),
    'bvb_term' => array(
        'title' => 'Bibliotheksverbund Bayern (BVB)',
        'text' => 'Zusammenschluss vorrangig wissenschaftlicher Bibliotheken Bayerns zum gemeinsamen Nachweis ihrer Bibliotheksbestände'
    ),
    'picturedatabase' => array(
        'title' => 'Bilddatenbank, Image Database',
        'text' => 'Enthält Informationen in Bildformaten'
    ),
    'booleanoperator' => array(
        'title' => 'Boolesche Operatoren, Verknüpfungsoperatoren, Boolean Operators',
        'text' => 'Dienen zum logischen Verknüpfen von Begriffen in einer Suchanfrage: und/and, oder/or und nicht/not
        <ul>
        <li>UND - bildet eine Schnittmenge<br>
        (Sucht man nach den Begriffen A und B, bekommt man Treffer, in denen A und B vorkommen)</li>
        <li>ODER - bildet eine Vereinigungsmenge<br>
        (Sucht man nach den Begriffen A oder B, bekommt man Treffer, in denen A oder B bzw. A und B vorkommen)</li>
        <li>NICHT - schließt aus<br>
        (Sucht man nach den Begriffen A und nicht B, bekommt man Treffer, in denen nur A enthalten ist, aber nicht B)</li>
        </ul>'
    ),
    'bvb' => array(
        'title' => 'BVB',
        'main' => 'bvb_term'
    ),
    'campuslicence' => array(
        'title' => 'Campuslizenz',
        'text' => 'Eine Lizenz, die den Zugriff auf elektronische Medien innerhalb eines Hochschulcampus beschränkt. Es kann nur von Rechnern zugegriffen werden, 
        die zum IP-Bereich der Hochschule gehören.',
        'link' => 'vpn'
    ),
    'darkweb' => array(
        'title' => 'Dark Web',
        'text' => 'Bereich des Internets, der nur zugänglich ist, wenn die Berechtigung zum Zugang für bestimmte Inhalte vorliegt. In der Regel erfolgt der Zugang über 
        Eingabe von Passwörtern. Die Inhalte im Dark Web werden meist manuell verknüpft.'
    ),
    'database' => array(
        'title' => 'Datenbank, Database',
        'text' => 'Elektronische Form eines Kataloges, einer Bibliographie, einer Datensammlung oder eines Textes (Lexikon, Werkausgabe). Es wird formal 
        unterschieden zwischen Online-Datenbanken (überwiegender Anteil) und CD-ROM- bzw. DVD-Datenbanken.'
    ),
    'databasehost' => array(
        'title' => 'Datenbankanbieter, Host',
        'text' => 'Bietet die Nutzung verschiedener Datenbanken unter einer gemeinsamen Suchoberfläche an, meist gegen Gebühr'
    ),
    'dbis_term' => array(
        'title' => 'Datenbank-Infosystem DBIS',
        'text' => 'Kooperativer Service zur Nutzung wissenschaftlicher Datenbanken; ein Teil davon steht kostenlos im Internet zur Verfügung. 
        Das Datenbank-Infosystem umfaßt z.Zt. über 5000 Einträge. Davon sind über 1000 frei über das Internet verfügbar.'
    ),
    'dbis' => array(
        'title' => 'DBIS',
        'main' => 'dbis_term'
    ), 
    'deepweb' => array(
        'title' => 'Deep Web',
        'link' => 'invisibleweb'
    ),
    'descriptor' => array(
        'title' => 'Deskriptor, Descriptor',
        'text' => 'Alle im Rahmen eines Thesaurus zugelassenen Begriffe, die zu einem Sachgebiet gehören; werden zum Indexieren von Literaturnachweisen benutzt',
        'link' => 'subjectheading'
    ),
    'dnb' => array(
        'title' => 'Die Deutsche Nationalbibliothek',
        'text' => 'Die Deutsche Nationalbibliothek (DNB) sammelt vollständig in Deutschland erschienene Monographien, Zeitschriften, Karten und Atlanten, 
        Dissertationen und Habilitationsschriften in gedruckter oder elektronische Form, deutschsprachige Publikationen des Auslandes, Übersetzungen aus dem 
        Deutschen in andere Sprachen und fremdsprachige Germanica. Angeschlossen ist das Deutsche Musikarchiv Berlin.'
    ),
    'doi_term' => array(
        'title' => 'Digital Object Identifier (DOI)',
        'text' => 'Zeichenkette zur eindeutigen Identifikation digitaler Objekte (vergleichbar der ISBN für Bücher; damit können auch Zeitschriftenaufsätze, 
        Ausschnitte aus Gesamtwerken u.ä. genau identifiziert werden)'
    ),
    'doi' => array(
        'title' => 'DOI',
        'main' => 'doi_term'
    ),
    'deliveryservice' => array(
        'title' => 'Dokumentlieferdienst',
        'text' => "Bereitstellung von Publikationen auf direktem Weg von der Lieferbibliothek zum Nutzer; Bestellung/ Lieferung häufig auf elektronischem Weg möglich: 
        Aufsätze als Kopie oder per E-mail, Bücher meist im Original bereitgestellt; kostenpflichtig! <br>
        Bekannte kommerzielle Lieferdienste sind z.B. Subito oder Ingenta. Die UB Chemnitz bietet zudem den Universitätsangehörigen einen 
        <a target='blank' class='linkextern' href='https://www.tu-chemnitz.de/ub/service/anmelden-ausleihe/dokulief/dokuliefformular.php'>Inhouse-Dienst</a>
        für Zusendung der Publikationen an."
    ),
    'ebook' => array(
        'title' => 'E-Book – Electronic Book, elektronisches Buch',
        'text' => 'Ein Buch in „elektronischer Form“, dessen Kapitel aus Dateien bestehen, die man im Regelfall herunterladen kann. Manche Verlage beschränken 
        den Umfang des Downloads von Informationen. E-Books werden im Katalog der UB Chemnitz nachgewiesen.'
    ),
    'ejournal' => array(
        'title' => 'E-Journal – Electronic Journal, elektronische Zeitschrift',
        'text' => 'Eine Zeitschrift in „elektronischer Form“, in der die Aufsätze als Dateien heruntergeladen werden können. E-Journals sind im Katalog der UB 
        Chemnitz nachgewiesen und in der Elektronischen Zeitschriftenbibliothek (EZB). Es ist möglich, auf die bezahlten Jahrgänge der E-Journals zuzugreifen 
        und sich einzelne Aufsätze herunterzuladen.'
    ),
    'earlyview' => array(
        'title' => 'Early View, ASAP Article, Online First',
        'text' => 'Artikel werden vor Drucklegung Online veröffentlicht, Zitierfähigkeit über DOI gegeben'
    ),
    'simplesearch' => array(
        'title' => 'Einfache Suche, Schnellsuche, Basic Search',
        'text' => 'Oft wird nur ein Suchfeld oder Suchschlitz angeboten; gesucht wird in der Regel in allen Suchfeldern (Einschränkung über Field Tags); der 
        Einsatz von Operatoren ist möglich'
    ),
    'ezb_term' => array(
        'title' => 'Elektronische Zeitschriftenbibliothek (EZB)',
        'text' => 'Kooperativer Service von über 300 Bibliotheken mit dem Ziel, ihren Nutzern einen einfachen und komfortablen Zugang zu elektronisch erscheinenden 
        wissenschaftlichen Zeitschriften zu bieten; aufgenommen werden alle Zeitschriften, die Artikel im Volltext anbieten. An der UB Chemnitz ist die EZB in den Katalog integriert.'
    ),
    'extendedsearch' => array(
        'title' => 'Erweiterte Suche',
        'main' => 'expertsearch'
    ),
    'expandsearch' => array(
        'title' => 'Expanded Search',
        'main' => 'expertsearch'
    ),
    'expertsearch' => array(
        'title' => 'Expertensuche, Erweiterte Suche, Advanced Search, Expanded Search',
        'text' => 'Suchmaske von Datenbanken, die komplexe Suchanfragen ermöglicht'
    ),
    'ezb' => array(
        'title' => 'EZB',
        'main' => 'ezb_term'
    ),
    'subjectbibliography' => array(
        'title' => 'Fachbibliographie, Subject Bibliography',
        'text' => 'Verzeichnis von selbstständig und/oder unselbstständig erscheinender Literatur zu einem bestimmten Fachgebiet unabhängig vom Bestand einer oder mehrerer Bibliotheken'
    ),
    'subjectportal' => array(
        'title' => 'Fachportal',
        'text' => 'Selektierung inhaltlich wertvoller Internetseiten und systematische Strukturierung der ausgewählten Links',
        'link' => 'portal'
    ),
    'subjectthesaurus' => array(
        'title' => 'Fachthesaurus',
        'main' => 'thesaurus'
    ),
    'subjectdatabase' => array(
        'title' => 'Faktendatenbank, Fact Database',
        'text' => 'Es werden physikalisch-chemische Daten, Geschäftsbilanzen, Statistiken u.ä. nachgewiesen'
    ),
    'interlibraryloan' => array(
        'title' => 'Fernleihe, Interlibrary loan',
        'text' => 'Ist ein Service, mit dem Medien (Bücher, Zeitschriftenaufsätze u.a.), die am Ort nicht vorhanden sind, von auswärtigen Bibliotheken beschafft 
        und in der eigenen Bibliothek zur Verfügung gestellt werden können'
    ),
    'fieldtag' => array(
        'title' => 'Field Tags',
        'text' => 'Damit kann bei der Suche festgelegt werden, in welchem Feld gesucht werden soll: z.B. Autorenname, Zeitschriftentitle, Schlagwort, Stichwort.'
    ),
    'openaccessholding' => array(
        'title' => 'Freihandbereich',
        'text' => 'Hier können Bibliotheksbesucher Bücher selbst aus den Regalen entnehmen und ausleihen.'
    ),
    'freetextsearch' => array(
        'title' => 'Freitextsuche',
        'text' => 'Die Suche nach einem Begriff oder Namen wird in mehreren Feldern gleichzeitig durchgeführt. Man trägt die Suchbegriffe in Suchfelder mit den 
        Bezeichnungen „Freitext“, „Beliebig“, oder „All Fields“ ein. In welchen Feldern genau gesucht wird, variiert von Datenbank zu Datenbank.'
    ),
    'gbv' => array(
        'title' => 'GBV',
        'main' => 'gbv_term'
    ),
    'gnd' => array(
        'title' => 'Gemeinsame Normdatei - GND',
        'text' => 'Kontrolliertes Schlagwortsystem (Normdatei), das vor allem in Bibliotheken zur Sacherschließung eingesetzt wird. Die OGND oder „Gemeinsame 
        Normdatei online“ bietet Zugriff auf Schlagwörter und Personennamen aus der GND; damit kann der Inhalt eines Dokuments oder Textes eindeutig beschrieben werden.'
    ),
    'gbv_term' => array(
        'title' => 'Gemeinsamer Bibliotheksverbund (GBV)',
        'text' => 'Zusammenschluss wissenschaftlicher Bibliotheken der Länder Bremen, Hamburg, Mecklenburg-Vorpommern, Niedersachsen, Sachsen-Anhalt, Schleswig-Holstein, 
        Thüringen sowie der Staatsbibliothek Berlin - Stiftung Preußischer Kulturbesitz zum gemeinsamen Nachweis ihrer Bibliotheksbestände'
    ),
    'googlescholar' => array(
        'title' => 'Google Scholar',
        'text' => 'eine kommerzielle Suchmaschine für wissenschaftliche Publikationen'
    ),
    'greyliterature' => array(
        'title' => 'Graue Literatur, Grey Literature',
        'text' => 'Literatur, die außerhalb des Buchhandels erschienen ist, wenig in Bibliographien und Katalogen nachgewiesen wird und deshalb schwer auffindbar ist, z.B. 
        Kongressberichte, Fortschrittsberichte, Geschäftsberichte'
    ),
    'thesis' => array(
        'title' => 'Hochschulschrift',
        'text' => 'Publikation, die von einem an einer Hochschule Tätigen oder Studierenden auf Grund seiner Lern- , Lehr- oder Forschungstätigkeit 
        erstellt wurde, z.B. studentische Abschlussarbeiten, Dissertationen, Habilitationen'
    ),
    'idiom' => array(
        'title' => 'Idiom',
        'text' => 'Feste Wortverbindung oder syntaktische Fügung, deren Bedeutung sich nicht aus der Bedeutung der einzelnen Bestandteile ableiten lässt (z.B. »Eulen nach 
        Athen tragen« im Sinne von »etwas Überflüssiges tun«).'
    ),
    'ipac' => array(
        'title' => 'Imagekatalog, IPAC (Image Public Access Catalog)',
        'text' => 'Digitale Bereitstellung konventioneller Kataloge; Kataloge werden als Images (Bilder) eingescannt; Recherche ist nur eindimensional nach den Ordnungsmerkmalen 
        der eingescannten Kataloge möglich: Verfasser oder Titel (bei mehr als drei Verfassern)'
    ),
    'index' => array(
        'title' => 'Index, Register',
        'text' => 'Alphabetische Liste aller Suchbegriffe, z.B. Index aller Autorennamen'
    ),
    'information' => array(
        'title' => 'Information',
        'text' => 'Abgeleitet von lateinisch: informare - bilden, durch Unterweisung Gestalt geben; ist potenziell oder tatsächlich vorhandenes nutzbares oder genutztes Wissen. 
        Die Semiotik versteht unter Informationen zweckorientierte Daten, die das Wissen erweitern'
    ),
    'informationliteracy' => array(
        'title' => 'Informationskompetenz, information literacy',
        'text' => 'Stellt in der modernen, stark dynamischen Informationsgesellschaft eine Schlüsselqualifikation zur Bewältigung von Problemen dar. Sie gehört zum Bereich 
        der soft skills und umfasst im Allgemeinen eine Reihe von Fähigkeiten, die dem Einzelnen den kompetenten, effizienten und verantwortungsbewussten Umgang mit 
        Informationen ermöglicht. Diese Fähigkeiten beziehen sich auf alle Aspekte des problembezogenen Erkennens eines Bedarfs an Informationen, ihrer Lokalisation, 
        ihrer Organisation, ihrer zielgerichteten Selektion durch Analyse und Evaluation und ihrer zweckoptimierten Gestaltung und Präsentation.'
    ),
    'interlibrary_loan' => array(
        'title' => 'Interlibrary Loan',
        'main' => 'interlibraryloan'
    ),
    'isbn_term' => array(
        'title' => 'International Standard Book Number (ISBN)',
        'text' => 'International eingeführte Identifikationsnummer für jedes Buch; 10- oder 13-stellige Nummer in vier Teilen zur eindeutigen Identifizierung von Büchern.<br>
        Beispiel einer 10-stelligen ISBN: 3-609-1476-3'
    ),
    'issn_term' => array(
        'title' => 'International Standard Serial Number (ISSN)',
        'text' => 'International eingeführte Identifikationsnummer für fortlaufende Sammelwerke, z.B. Zeitschriften. 8-stellige Nummer in zwei Blöcken zu je vier Ziffern 
        zur Identifizierung von fortlaufenden Sammelwerken wie Zeitungen, Zeitschriften, etc.<br>
        Beispiel: 0113-4763'
    ),
    'internet' => array(
        'title' => 'Internet, Interconnected Network',
        'text' => '    Ist ein weltweites Netzwerk voneinander unabhängiger Netzwerke. Es dient der Kommunikation und dem Austausch von Informationen. Jeder Rechner eines 
        Netzwerkes kann prinzipiell mit jedem anderen Rechner kommunizieren.'
    ),  
    'invisibleweb' => array(
        'title' => 'Invisible Web, Deep Web, Hidden Web',
        'text' => 'Bezeichnet den Teil des Internets, der bei einer Recherche nicht über normale Suchmaschinen auffindbar ist. Das Deep Web besteht zu großen 
        Teilen aus themenspezifischen Datenbanken (Fachdatenbanken) und Webseiten, die erst durch Anfragen dynamisch aus Datenbanken generiert werden'
    ),
    'isbn' => array(
        'title' => 'ISBN',
        'main' => 'isbn_term'
    ),
    'issn' => array(
        'title' => 'ISSN',
        'main' => 'issn_term'
    ),
    'kvk_term' => array(
        'title' => 'Karlsruher Virtueller Katalog (KVK)',
        'text' => 'Ermöglicht in einer Suche die Abfrage mehrerer deutschsprachiger sowie internationaler Online-Kataloge bzw. Bibliotheksverbünde; außerdem 
        ist die Recherche in der Zeitschriftendatenbank (ZDB) und in Buchhandelskatalogen möglich'
    ),
    'catalog' => array(
        'title' => 'Katalog, Catalogue',
        'text' =>'Der Katalog ist das Bestandsverzeichnis aller Medien einer Bibliothek. Auch die elektronischen Bücher und Zeitschriften 
        sind in einem modernen Katalog enthalten sowie in zunehmendem Maße elektronisch verfügbare Artikel aus lizenzierten und freien Volltext-Datenbanken.'
    ),
    'classifikation' => array(
        'title' => 'Klassifikation, Classification',
        'main' => 'system'
    ),
    'contextoperator' => array(
        'title' => 'Kontextoperator',
        'main' => 'proximityoperator'
    ),
    'shortpresentation' => array(
        'title' => 'Kurzreferat',
        'main' => 'abstract'
    ),
    'kvk' => array(
        'title' => 'KVK',
        'main' => 'kvk_term'
    ),
    'lexicaldatabase' => array(
        'title' => 'Lexikalische Datenbank',
        'text' =>'Elektronische Form eines Nachschlagewerkes'
    ),
    'link' => array(
        'title' => 'Link',
        'main' => 'url'
    ),
    'literaturedepartment' => array(
        'title' => 'Literaturabteilung',
        'text' => 'Bezeichnet den Standort und gleichzeitig die Verfügbarkeit eines Buches. Hierzu zählen die Bereiche Freihand (ausleihbar, frei zugänglich), Magazin 
        (meist ausleihbar, für den Nutzer nicht zugänglich) und Lesesaal (frei zugänglich, nicht ausleihbar).'
    ),
    'literaturedatabase' => array(
        'title' => 'Literaturdatenbank',
        'main' => 'referencedatabase'
    ),
    'stackcollection' => array(
        'title' => 'Magazin',
        'text' => 'Nur für Mitarbeiter zugänglicher Bereich der Bibliothek, in dem Bücher, Zeitschriften und Zeitungen aufbewahrt werden, die älter sind oder ungewöhnliche Formate haben. 
        Magazin-Literatur muss über den Katalogbutton \'Magazin\' bestellt werden und wird für den Nutzer reserviert.'
    ),
    'mask' => array(
        'title' => 'Maskierung',
        'text' => 'Durch Eingabe vorgegebener Maskierungszeichen *.?,$ (Wildcards) innerhalb eines Wortes können u.a. Wörter unterschiedlicher Schreibweise mit einer 
        einzigen Abfrage ermittelt werden (z.B. Do*umentation = Dokumentation und documentation)'
    ),
    'metadata' => array(
        'title' => 'Metadaten',
        'text' => 'Es handelt sich um Daten über Daten. Metadaten zu einem Buch sind z.B. Autorenname, ISBN, Auflage oder Verlag.',
        'link' => 'bibliographicdata'
    ),
    'metasearchengine' => array(
        'title' => 'Metasuchmaschine',
        'text' => 'Besondere Form von Suchmaschine; leitet die Suchanfragen parallel an die Datenbestände mehrerer herkömmlicher Suchmaschinen weiter und 
        entfernt doppelte Einträge aus den Trefferlisten'
    ),
    'monarchqucosa' => array(
        'title' => 'MONARCH-QUCOSA (Multimedia Online Archiv Chemnitz)',
        'text' => 'Dokumenten- und Publikationsservice der TU Chemnitz; dient der Archivierung hochschuleigener Publikationen (Hochschulschriften). Hosting durch QUCOSA Dresden'
    ),
    'monograph' => array(
        'title' => 'Monographie, Monography',
        'text' => 'Veröffentlichung, in der ein einzelnes begrenztes Thema umfassend behandelt wird, meist eines Verfassers'
    ),
    'proximityoperator' => array(
        'title' => 'Nachbarschaftsoperator , Abstandsoperator, Proximity Operator, Adjacency Operator',
        'text' => 'Definiert den Abstand zwischen den einzelnen Suchbegriffen: ob sie direkt nebeneinander oder innerhalb eines definierten Abstandes, Satzes oder Feldes vorkommen sollen'
    ),
    'reference' => array(
        'title' => 'Nachweis',
        'main' => 'record'
    ),
    'norm' => array(
        'title' => 'Normschrift - Rechtsnorm',
        'text' => 'Gesetzliche Regelung von, beispielsweise, den Maßen einer Glühbirne, die grundsätzlich an jeden gerichtet ist. Die UB Chemnitz verfügt über einen 
        umfangreichen Bestand, so zum Beispiel das komplette aktuelle DIN-Normenwerk, die ASTM-Standards (American Society for Testing and Materials) und die 
        VDI-Richtlinien (Verein deutscher Ingenieure).'
    ),
    'onlinefirst' => array(
        'title' => 'Online First',
        'main' => 'earlyview'
    ),
    'opac' => array(
        'title' => 'OPAC (Online Public Access Catalogue)',
        'text' => 'Rechnergestütztes Bestandsverzeichnis (Katalog) einer Bibliothek'
    ),
    'openaccess' => array(
        'title' => 'Open Access, freier kostenloser Zugang',
        'text' => 'Bezeichnet das Ziel, i.d.R. wissenschaftliche Literatur und Materialien im Internet frei (kostenlos und ohne Lizenzbeschränkungen) zugänglich zu machen'
    ),
    'operator' => array(
        'title' => 'Operator',
        'main' => 'booleanoperator , proximityoperator'
    ),
    'patent' => array(
        'title' => 'Patent',
        'text' => 'Das, einem Erfinder vom Staat erteilte, zeitlich begrenzte Monopol für die wirtschaftliche Nutzung einer Erfindung; recherchierbar sind Patente 
        über das Patentinformationszentrum (PIZ).'
    ),
    'peerreview' => array(
        'title' => 'Peer Reviewing, Begutachtung',
        'text' => 'Es bezeichnet das Verfahren, durch Gutachter z.B. Artikel wissenschaftlich bewerten zu lassen'
    ),
    'phrase' => array(
        'title' => 'Phrase',
        'text' => 'Mehrere einzelne Wörter in vorgegebener Wortfolge bilden zusammen einen Begriff (z.B. „Sächsisch-polnische Union“).'
    ),
    'phrasesearch' => array(
        'title' => 'Phrasensuche',
        'text' => 'Die Suche nach einem Begriff, der aus mehreren Wörtern besteht. Die Zeichenkette wird mit Anführungszeichen oder Hochkommata als zusammengehörige Wortfolge markiert.'
    ),
    'plagiarism' => array(
        'title' => 'Plagiat',
        'text' => 'Ein Plagiat ist die widerrechtliche Übernahme und Verbreitung von fremden Texten jeglicher Art und Form ohne Kenntlichmachung der Quelle.'
    ),
    'placeholder' => array(
        'title' => 'Platzhalter',
        'main' => 'wildcard'
    ),
    'portal' => array(
        'title' => 'Portal',
        'text' => 'Ein zentraler Zugang, über den man auf individuell zugeschnittene Informationen und Dienste zugreifen kann. Bei Portalen mit spezieller 
        fachlicher Ausrichtung spricht man auch von Fachportalen'
    ),
    'primarysource' => array(
        'title' => 'Primärquelle, Primary Source',
        'text' => 'Originalliteratur, in der die aktuellen Erkenntnisse und neuesten Ergebnisse dokumentiert sind'
    ),
    'qucosa' => array(
        'title' => 'QUCOSA',
        'text' => "QUCOSA ist Hostingpartner des Publikationsservers MONARCH der UB Chemnitz, s. <a href='#monarchqucosa'>MONARCH-QUCOSA</a>"
    ),
    'quicksearch' => array(
        'title' => 'Quicksearch',
        'main' =>'simplesearch'
    ),
    'ranking' => array(
        'title' => 'Ranking, Suchrang',
        'text' => 'Anzeigen der Suchergebnisse sortiert nach Relevanz (Wichtigkeit entsprechend der Anfrage); die Berechnung der Relevanz erfolgt in den Datenbanken unterschiedlich'
    ),
    'record' => array(
        'title' => 'Record, Treffer, Nachweis',
        'text' => 'Dokument in einer Datenbank'
    ),
    'redilinks' => array(
        'title' => 'ReDI»Links, Link zum Volltext',
        'text' => 'Automatisiertes Service-Tool zur Bestandsprüfung der in Datenbanken gefundenen Dokumente, z.B. innerhalb einer Bibliothek'
    ),
    'research' => array(
        'title' => 'Recherche',
        'text' => 'Gezielte Suche nach Informationen in Datenbanken oder im Internet.'
    ),
    'refereedjournal' => array(
        'title' => 'Refereed Journal, begutachtete Zeitschrift',
        'text' => 'Zeitschrift, die von Gutachtern wissenschaftlich bewertet wurde',
        'link' => 'peerreview'
    ),
    'referencedatabase' => array(
        'title' => 'Referenzdatenbank, Literaturdatenbank, Reference Database',
        'text' => 'Enthält Nachweise von Veröffentlichungen mit Angaben zu Titel, Autor, Erscheinungsort und -datum, ggf. Zeitschriften- oder Buchtitel und Seitenzahl , 
        teilweise erweitert durch Schlagwörter und Abstracts',
        'link' => 'bibliographicdatabase , abstractdatabase'
    ),
    'rvk_term' => array(
        'title' => 'Regensburger Verbundklassifikation (RVK)',
        'text' => 'Aufstellungssystematik für Bibliotheksbestände in Freihand; Kombination aus Buchstaben und Zahlen; entwickelt und gepflegt von der UB Regensburg. 
        Im Katalog lassen sich die RVK-Systemstellen, z.B ST 250 (einzelne Programmiersprachen) für die Suche thematisch verwandter Literatur nutzen.'
    ),
    'register' => array(
        'title' => 'Register',
        'main' => 'index'
    ),
    'rss' => array(
        'title' => 'RSS',
        'text' => 'Rich Site Summary oder neuer: Real Simple Syndication; eine Art Neuheiten-Verfolgung und - benachrichtigung, also ein "News Ticker" im Web. 
        Ein Client-Programm, der RSS-Reader überwacht Nachrichtenagenturen, Online-Zeitungen und Blogs auf neue Inhalte und meldet dem Abonnenten diese in 
        Form von Schlagzeilen samt einer Zusammenfassung. RSS Reader sind Bestandteil des Browsers.'
    ),
    'rvk' => array(
        'title' => 'RVK',
        'main' => 'rvk_term'
    ),
    'subjectheading' => array(
        'title' => 'Schlagwort, Subject Term, Keyword',
        'text' => 'Ist ein Begriff zur möglichst kurzen und vollständigen Beschreibung für den sachlichen Inhalt einer Veröffentlichung; muß nicht im Titel einer 
        Veröffentlichung vorkommen'
    ),
    'subjectheadinglist' => array(
        'title' => 'Schlagwortliste',
        'text' => 'Alphabetische Liste der inhaltserschließenden Begriffe; wird zum Indexieren von Veröffentlichungen für die Aufnahme in eine Datenbank als auch 
        bei der Datenbankrecherche benötigt; legt die Schreibweise eindeutig fest'
    ),
    'swd_term' => array(
        'title' => 'Schlagwortnormdatei der DNB (SWD)',
        'main' => 'gnd'
    ),
    'quick_search' => array(
        'title' => 'Schnellsuche',
        'main' => 'simplesearch'
    ),
    'search_history' => array(
        'title' => 'Search History',
        'main' => 'searchhistory'
    ),
    'secondarysource' => array(
        'title' => 'Sekundärquelle, Sekundärliteratur',
        'text' => 'Verzeichniss, das systematisch Originalschrifttum nachweist, z. B. Bibliographien, Referatezeitschriften und Handbücher'
    ),
    'independentlypublishedliterature' => array(
        'title' => 'Selbstständig erscheinende Literatur',
        'text' => 'Literatur, die als eigene Einheit produziert und in Bibliothekskatalogen nachgewiesen wird, z.B. Monographien, Dissertationen'
    ),
    'semester' => array(
        'title' => 'Semesterapparat',
        'text' => 'Eine von einem Dozenten für die Teilnehmer eines Seminars zusammengestellte Sammlung von Büchern. Während des laufenden Semesters ist es nicht 
        möglich, Bücher aus dem Semesterapparat zu entleihen. Im elektronischen Semesterapparat können Dokumente (vom Dozenten) online in einem geschützten Bereich abgelegt werden.'
    ),
    'specialsearchengine' => array(
        'title' => 'Spezialsuchmaschinen',
        'text' => 'Spezialsuchmaschinen beschränken sich auf ausgewählte Inhalte und Dokumententypen im Internet, die sie vollständig erfassen.'
    ),
    'keyword' => array(
        'title' => 'Stichwort',
        'text' => '    Sinntragendes Wort aus dem Titel einer Veröffentlichung oder dem Abstract'
    ),
    'searchservice' => array(
        'title' => 'Suchdienst',
        'text' => 'Neben Suchmaschinen gibt es auch noch redaktionell bearbeitete Kataloge und Werkzeuge anderer Art, mit deren Hilfe Internet-Inhalte 
        aufgespürt werden können. Sie arbeiten meist nach dem Roboter- oder Katalogprinzip; mit Operatoren können komplexe Suchanfragen formuliert werden'
    ),
    'searchhistory' => array(
        'title' => 'Suchgeschichte, Search History',
        'text' => 'Protokoll der Suchfragen und Suchergebnisse einer Datenbankrecherche.'
    ),
    'searchengine' => array(
        'title' => 'Suchmaschine',
        'text' => 'Dienst, mit dem im World Wide Web Dokumente und Webseiten zu einem eingegebenen Begriff gefunden werden (Stichwortsuche). Grundlage bildet ein Index der 
        maschinell erstellt wurde.'
    ),
    'searchtemplate' => array(
        'title' => 'Suchmaske',
        'text' => 'Formular zur Eingabe von Suchbegriffen'
    ),
    'searchoperator' => array(
        'title' => 'Suchoperator',
        'main' => 'booleanoperator , proximityoperator'
    ),
    'swb_term' => array(
        'title' => 'Südwestdeutscher Bibliotheksverbund (SWB)',
        'text' => 'Zusammenschluss der wissenschaftlichen Bibliotheken Baden-Württembergs, des Saarlandes und Sachsens zum gemeinsamen Nachweis ihrer Bibliotheksbestände'
    ),
    'surfaceweb' => array(
        'title' => 'Surface Web oder Visible Web',
        'text' => 'Der Teil des Internets, der allgemein zugänglich ist.'
    ),
    'swb' => array(
        'title' => 'SWB',
        'main' => 'swb_term'
    ),
    'swd' => array(
        'title' => 'SWD',
        'main' => 'swd_term'
    ),
    'system' => array(
        'title' => 'Systematik , Klassifikation',
        'text' => 'Ordnungssystem, das einen Gegenstandsbereich in Klassen und Unterklassen einteilt. Die Systematik spiegelt den Zusammenhang und die Gliederung 
        aller Wissensgebiete wieder, wobei sie von den einzelnen Wissenschaften ausgeht und diese dann in immer kleinere und speziellere Begriffe untergliedert. 
        Diese verschiedenen Gruppen und Unterteilungen erhalten jeweils eine bestimmte Notation.'
    ),
    'tertiarysource' => array(
        'title' => 'Tertiärquelle',
        'text' => 'Literaturquelle, in der Wissenschaftsbereiche in zusammenfassender Form dargestellt werden (z.B. Lehrbücher, Lexika, Wörterbücher)'
    ),
    'topicanalyses' => array(
        'title' => 'Themenanalyse',
        'main' => '5-steps-model'
    ),
    'thesaurus' => array(
        'title' => 'Thesaurus',
        'text' => 'In der Informationswissenschaft ein normiertes Vokabular. Es besteht aus einer systematisch geordneten Sammlung von Begriffen, die in thematischer Beziehung 
        zueinander stehen.'
    ),
    'truncation' => array(
        'title' => 'Trunkierung, Truncation',
        'text' => 'Über den Wortstamm können mit Hilfe von Trunkierungszeichen wie *, ?, $ bei einer Datenbankrecherche alle Zusammensetzungen eines Wortes ermittelt werden. 
        Es besteht die Möglichkeit, sowohl links als auch rechts zu trunkieren',
        'link' => 'mask'
    ),
    'undependentpublishedliterature' => array(
        'title' => 'Unselbstständig erscheinende Literatur',
        'text' => 'Teile (Artikel, Kapitel) aus selbstständig erscheinenden Medien, werden in Katalogen im Allgemeinen nicht nachgewiesen'
    ),
    'url' => array(
        'title' => 'URL, Link',
        'text' => 'Die Abkürzung steht für "Uniform Resource Locator" und dient dazu Webseiten direkt anzusteuern.'
    ),
    'fulltextdatabase' => array(
        'title' => 'Volltextdatenbank',
        'text' => 'Enthält vollständige Texte einer Publikation, z.B. Zeitschriftenaufsätze, Patente oder den Inhalt ganzer Bücher'
    ),
    'vpn' => array(
        'title' => 'VPN',
        'text' => "Mittels VPN lassen sich gesicherte Netzwerkverbindungen in die TU Chemnitz aufbauen, s. <a target='blank' href='https://www.tu-chemnitz.de/urz/network/access/vpn.html'>hier</a>"
    ),
    'webcatalog' => array(
        'title' => 'Webkatalog',
        'text' => 'Redaktionell betreutes Verzeichnis über Internetseiten. Hierbei wird jede neu aufgenommene Website von einem Redakteur begutachtet'
    ),
    'wildcard' => array(
        'title' => 'Wildcard, Platzhalter, Maskierung',
        'text' => 'Ersetzt innerhalb eines Suchbegriffes ein oder mehrere Zeichen; verwendet werden abhängig von der Datenbank *, $, ?, #',
        'link' => 'truncation'
    ),
    'zdb' => array(
        'title' =>  'ZDB',
        'main' => 'zdb_term'
    ),
    'journal' => array(
        'title' => 'Zeitschrift, Journal',
        'text' => '<ul>
        <li>Print: periodisch mindestens zwei Mal im Jahr erscheinendes Sammelwerk in gedruckter Form</li>
        <li>E-Journal: elektronische Form einer Zeitschrift</li>
        </ul>'
    ),
    'zdb_term' => array(
        'title' => 'Zeitschriftendatenbank (ZDB)',
        'text' => 'Verzeichnet Titel- und Bestandsnachweise fortlaufender Sammelwerke, also Zeitschriften, Zeitungen, Jahrbücher u.ä.; Nachweis von Printzeitschriften und elektronischen Zeitschriften'
    ),
    'accessnumber' => array(
        'title' => 'Zugriffsnummer',
        'main' => 'accessionnumber'
    )
);

?>
