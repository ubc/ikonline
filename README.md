# IKOnline - Vermittlung von Informationskompetenz

## Inhalt
Das E-Learning Angebot dient der Vermittlung von Informationskompetenz und wurde an der Universitätsbibliothek Chemnitz entwickelt und inhaltlich durch die 
Arbeitsgruppe Informationsvermittlung erarbeitet. 

Das Angebot gliedert sich in eine Einführung und 6 Module, die jeweils mit einem Test abgeschlossen werden können.

Inhalte sind:<br>
1. Universitätsbibliothek & Katalog
2. Vorbereitung der Recherche
3. Suchtechniken und Datenbankfunktionen
4. Recherchieren im Internet
5. Literaturbeschaffung
6. Zitieren und Plagiat

Zusätzlich gibt es eine abschließende Evaluation, eine Linkssammlung, sowie ein Glossar.

## Technische Grundlagen

IKOnline basiert auf dem Framework [Bootstrap 3.3.6](https://getbootstrap.com/docs/3.3/) und nutzt PHP (5.3 oder höher), CSS und die Javascript Bibliothek
[jQuery](https://jquery.com/).

Die Abschlusstests und die Evaluation wurden mit den kostenpflichtigen Software iSpring und die Tutorials mit der kostenpflichtigen Software Adobe Captivate 
erstellt und sind nur mit der jeweiligen Software bearbeitbar.

Eine Demoversion finden Sie [hier](https://www.tu-chemnitz.de/ub/projekte-und-sammlungen/projekte/lrbip/ik/).

## Aufbau des E-Learning Angebots

Der HTML Rahmen und die Navigation werden zentral durch ```./sys/template.inc``` erstellt. Notwendige Konfigurationen zu Sprache, Hauptnavigation etc. werden in der
```./sys/config.inc``` vorgenommen. Die einzelnen Seiten befinden sind im Ordner ```./content/``` im jeweiligen Modulordner.


Bitte benennen Sie die ```./sys/config_default.inc``` um in ```./sys/config.inc``` und gegeben Sie den Weblink zum Angebot an. Danach sollte sich das Angebot
über die eingegebene Adresse starten lassen.

### Modulnavigation
Die Modulnavigation wird über ein multidimensionales Array in den jeweiligen ```navigation.inc``` definiert, die sich in den einzelnen Modulen unter ```./content/``` befinden. 
Das erste Array-Element definiert die Sprache und enthält ein Array mit dem Muster: **filename => Menutext**.

Enthalten Menüpunkte zusätzlich Unterpunkte wird ein weiteres Array erstellt. Der übergeordnete Menüpunkt ist immer das erste Array-Element und das 
Array muss immer den Dateinamen des ersten Unterpunkt als Key erhalten. Diese müssen nicht identisch sein, wenn es zum übergeordneten Menüpunkt keine Seite gibt.

BSP: Die ersten beiden Menüpunkte mit Untermenü haben keine Seite für den übergeordneten Menüpunkt
```
$files = array (
    'de' => array(
        'index' => 'Willkommen',
        'technology' => 'Suchtechniken',
        'searchfields' => array(
            'search' => 'Suchfelder/Suchbegriffe',
            'searchfields' => 'Suchfelder wählen',
            'find' => 'Suchbegriffe finden',
            'keyword' => 'Stichwort/ Schlagwort',
            'tutorial1' => 'Tutorial Schlagwörter',
            'thesaurus' => 'Thesaurus',
            'register' => 'Index',
        ),
        'truncation' => array(
            'searchtools' => 'Suchwerkzeuge',
            'truncation' => 'Trunkierung',
            'phrase' => 'Phrasen suchen',
            'boolean' => 'Boolesche Operatoren',
            'tutorial2' => 'Tutorial Boolesche Operatoren'
        ),
        'function' => array(
            'function' => 'Datenbankfunktionen',
            'history' => 'Search History',
            'alert' => 'Alerting',
            'help' => 'Hilfefunktionen'
        ),
        'test' => 'Abschlusstest'
    )
);
```

### Einzelne Seiten

Die einzelnen Seiten befinden sich unter ```./content/``` im jeweilige Modul.

Für jeden Menüpunkt wird eine eigene HTML-Seite angelegt und der Rahmen wird eingebunden. Nicht deutschsprachige Seiten erhalten zudem eine zusätzliche 
Sprachendung, z.B. ```index.html.cs```. Die Endung wird in ```./sys/config.inc``` festgelegt.

BSP Modul1
```
<?php
require_once('../../sys/config.inc');
page(__FILE__);
?>
```

Bei Seiten, die keine eigene Modulnavigation haben, wird eine weiter Variable übergeben, dann erscheint die Modulübersicht 
als linkes Menü.

BSP Startseite
```
<?php 
require_once('sys/config.inc');
page(__FILE__, 'start');
?>
```

## Autoren

Inhalt: [Carolin Ahnert](mailto:carolin.ahnert@bibliothek.tu-chemnitz.de)<br>
Quellcode: [Katrin Otto] (mailto:katrin.otto@bibliothek.tu-chemnitz.de)

## Lizenzen
Die Inhalte stehen unter der Lizenz  [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.de) Universitätsbiliothek Chemnitz<br>
Der Quellcode steht unter der [MIT Licence](LICENSE) Copyright (c) 2018 Library of the Chemnitz University of Technology

