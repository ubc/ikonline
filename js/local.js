// author: Katrin Otto (katrin.otto@bibliothek.tu-chemnitz.de)

$( document ).ready(function() 
{
    // show all submenu items in mobile view
    if ($(window).width() < 768)
    {
        $('li .hidden').removeClass('hidden');
    }

    // Scrollback function
    $(window).scroll(function () 
    {
        if ($(this).scrollTop() > 50) 
        {
            $('#back-to-top').fadeIn();
        } 
        else 
        {
            $('#back-to-top').fadeOut();
        }
    });

    // scroll body to 0px on click
    $('#back-to-top').click(function () 
    {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate(
            {
                scrollTop: 0
            }, 
        800);
        return false;
    });

    $('#back-to-top').tooltip('show');

    // popover function
    $('[data-toggle="popover"]').popover();


    // click actions
    $('.credit-subject').click(function()
    {
        var id = $(this).attr('id');
        
        $('.credit-subject').removeClass('credit-active');

        $(this).addClass('credit-active');

        $('.credit').hide();

        $('.'+id).toggle();
    });

    // same hight of content by hover boxes
    $('.hover-content').each(function()
    {
        var height = $(this).outerHeight();
        var divHeight = height+40;

        $(this).parent().css('height', divHeight );
    });
});
