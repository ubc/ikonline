<?php   
# File contains all configuration for frame and the main menu
# Please change the name into config.inc and make your configuration

# author: Katrin Otto (katrin.otto@bibliothek.tu-chemnitz.de) 
# 21.06.2018


# active TUC specific settings with TRUE
$tuc = false;

# local file path
$base_dir = dirname(__FILE__);

# name of the library for webtitle
$library_name = array (
    'de' => '',
    'cs' => ''
);

# basic weblink to ikonline
$base_URL = '';

# current weblink
$url =(isset($_SERVER['HTTPS'])?'https':'http').'://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
# add index.html if url doesn't contains an file name
$url = !preg_match('/\.html/',$url ) ? $url.'index.html' : $url;

# current file name
$file_current =  basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
# add index.html if url doesn't contains an file name
$file_current = (strpos($file_current, '.html') !== false) ? $file_current : 'index.html';

# name of current file
$file_name = explode('.',pathinfo($file_current, PATHINFO_FILENAME))[0];

# list of all available languages
$lang = array (
    'Deutsch' => 'de',
    'English' => 'en',
    'Česky' => 'cs'
);

# startpage for ikonline
$startpage = '/index.html';

# main navigation
# key is name of folder
$array_navigation = array(
    'de' => array(
        'titel' => 'IKOnline',
        'links' => array(
            //Der Key ist der Ordner und Value ist die Benennung
            'introduction' => 'Einführung',
            'modul1' => 'Modul 1',
            'modul2' => 'Modul 2',
            'modul3' => 'Modul 3',
            'modul4' => 'Modul 4',
            'modul5' => 'Modul 5',
            'modul6' => 'Modul 6',
            'evaluation' => 'Evaluation'),
        'rechts' => array(
            'Werkzeuge' => array(
                'links' => 'Linkssammlung',
                'glossary' => 'Glossar'),
            'Sprachen' => $lang)),
    'cs' => array(
        'titel' => 'IKOnline',
        'links' => array(
            'introduction' => 'Úvod',
            'modul1' => 'Modul 1',
            'modul2' => 'Modul 2',
            'modul3' => 'Modul 3',
            'modul4' => 'Modul 4',
            'modul5' => 'Modul 5',
            'modul6' => 'Modul 6',
            'evaluation' => 'Evaluace'),
        'rechts' => array(
            'Nástroje' => array(
                'links' => 'Seznam odkazů',
                'glossary' => 'Pojmy'),
            'Jazyk' => $lang)),
    'en' => array(
        'titel' => 'IL-Online',
        'links' => array(
            'introduction' => 'Introduction',
            'modul1' => 'Unit 1',
            'modul2' => 'Unit 2',
            'modul3' => 'Unit 3',
            'modul4' => 'Unit 4',
            'modul5' => 'Unit 5',
            'modul6' => 'Unit 6',
            'evaluation' => 'Evaluation'),
        'rechts' => array(
            'Tools' => array(
                'links' => 'Link collection',
                'glossary' => 'Glossary'),
            'Languages' => $lang))

        );

# left menu for start page and evaluation
$start_menu = array(
    'de' => array(
        'Inhalt' => array(
            'introduction' => 'Einführung',
            'modul1' => '1. Universitätsbibliothek & Katalog', 
            'modul2' => '2. Vorbereitung der Recherche', 
            'modul3' => '3. Suchtechniken und Datenbankfunktionen', 
            'modul4' => '4. Recherchieren im Internet', 
            'modul5' => '5. Literaturbeschaffung', 
            'modul6' => '6. Zitieren und Plagiat', 
            'evaluation' => 'Evaluierung'
        )
    ),
    'cs' => array(
        'Obsah' => array(
            'introduction' => 'Úvod',
            'modul1' => '1. Univerzitní knihovna a katalog',
            'modul2' => '2. Příprava rešerše',
            'modul3' => '3. Techniky vyhledávání a funkce v databázích',
            'modul4' => '4. Rešerše na internetu',
            'modul5' => '5. Získání literatury',
            'modul6' => '6. Citace a plagiáty',
            'evaluation' => 'Evaluace'
        )
    ),
    'en' => array(
        'Inhalt' => array(
            'introduction' => 'Introduction',
            'modul1' => '1. University Library & catalog',
            'modul2' => '2. Preparation of research',
            'modul3' => '3. Search techniques and features of databases',
            'modul4' => '4. Online reserach',
            'modul5' => '5. Getting hold of literature',
            'modul6' => '6. Citing and plagiarism',
            'evaluation' => 'Evaluation'
        )
    )


);

# text for next and previous button
$zurueck = array(
    'de' => 'Zurück',
    'cs'  => 'Zpět',
    'en' => 'Back'
);

$vor = array(
    'de' => 'Weiter',
    'cs' => 'Dál',
    'en' => 'Forward'
);

# TUC spezifc variable
# key is the name of the breadcrumb key and the value contains the link
# last value must be empty
$breadcrumb = array(
    'de' => array (
        'Einrichtung' => '../../../',
        'Bibliothek' => '../../',
        'Bereich' => '../',
        'IKOnline' => ''
    )
);

# including page
require_once($base_dir.'/template.inc');


?>
