<?php
# author: Katrin Otto (katrin.otto@bibliothek.tu-chemnitz.de)
# 21.06.2018

# to stop the recursion after first include
$CMS_page_shown = 0;

# get the current language by checking the htnl file
# language beeing not german have an special ending like index.html.cs
# endings are defined in the config.inc
if (preg_match('/\.html./' , $url ))
{
    $current_lang = explode ('html.' ,  $url)[1];
}
else
{
    $current_lang = 'de';
}

# inlcude glossary array
require_once($base_dir . '/../content/glossary/glossary_'.$current_lang.'.php');


# returns the hole page
function page($file, $index ='')
{
    # Variable for recursion and current lang from top of the script
    global $CMS_page_shown, $current_lang;
    # Variables from config.inc
    global $library_name, $base_dir, $base_URL, $array_navigation;

    # TUC
    global $tuc, $breadcrumb;

    # allready shown - stop of recursion
    if ($CMS_page_shown == 1) return;
    $CMS_page_shown = 1;

    $html = array();

    $html[] = '<html>'. PHP_EOL;
    $html[] = '<head>'. PHP_EOL;
    $html[] = '<meta charset="UTF-8">'. PHP_EOL;
    $html[] = '<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1">'. PHP_EOL;
    $html[] = '<title>Informationskomptentenz Online - ' . $library_name[$current_lang] . '</title>'. PHP_EOL;
    $html[] = '<link rel="stylesheet" href="' . $base_URL . '/bootstrap_336/css/bootstrap.css">'. PHP_EOL;
    $html[] = '<link rel="stylesheet" href="' . $base_URL . '/css/standard.css"/>'. PHP_EOL;
    $html[] = '<script type="text/javascript" src="' . $base_URL . '/js/jquery.js"></script>'. PHP_EOL;
    $html[] = '<script type="text/javascript" src="' . $base_URL . '/bootstrap_336/js/bootstrap.min.js"></script>'. PHP_EOL;
    $html[] = '<script type="text/javascript" src="' . $base_URL . '/js/local.js"></script>'. PHP_EOL;

    # TUC specfic Layout
    if($tuc)
    {   
        $html[] = '<script type="text/javascript" src="/tucal4/js/tucal.js?201803011412/de"></script>'. PHP_EOL;
        $html[] = '<link rel="stylesheet" type="text/css" href="/tucal4/css/tucal4.css?201802080836">'. PHP_EOL;
        $html[] = '<link rel="stylesheet" href="'.$base_URL.'/css/tuc.css"/>'. PHP_EOL;
    }

    $html[] = '</head>'. PHP_EOL;

    # TUC specific class for body
    $body_class = $tuc ? 'class-tuc' : 'main';

    $html[] = '<body class="' . $body_class . '">'. PHP_EOL;

    # TUC specific header
    if($tuc)
    {
        $html[] = '<header>'. PHP_EOL;
        $html[] = '<div id="tucal-head" class="hidden-print">'. PHP_EOL;
        $html[] = '<div class="container">'. PHP_EOL;
        $html[] = '<div id="tucal-headlogoborder" class="col-sm-3">'. PHP_EOL;
        $html[] = '<a href="/index.html.en" accesskey="h" class="border"><span class="sr-only">Link zur Startseite</span>
            <img src="/tucal4/img/logo.png" id="tucal-headlogo" alt="Logo: TU Chemnitz"></a>'. PHP_EOL;
        $html[] = '</div>'. PHP_EOL;
        $html[] = '<div id="tucal-headnavigation" class="col-sm-9 collapse navbar-collapse">'. PHP_EOL;
        $html[] = '<div id="tucal-orgtitle" class="row">'. PHP_EOL;
        $html[] = '<div class="col-xs-12 no-spacing">'. PHP_EOL;
        $html[] = '<div>' . $library_name[$current_lang] . '</div>'. PHP_EOL;
        $html[] = '</div></div></div></div></div></header>'. PHP_EOL;
        $html[] = '<nav id="tucal-breadcrumbs" class="hidden-print"><h2 class="sr-only">Breadcrumb Navigation</h2>'. PHP_EOL;
        $html[] = '<div class="container">'. PHP_EOL;
        $html[] = '<div id="tucal-breadcrumbrow" class="row">'. PHP_EOL;
        $html[] = '<ol class="tucal-breadcrumb">'. PHP_EOL;
        foreach ($breadcrumb[$current_lang] as $b => $link)
        {
            if(!empty($link))
            {
                $html[] = '<li><a href="' . $link . '">' . $b . '</a></li>'. PHP_EOL;
            }
            else
            {
                 $html[] = '<li class="active">' . $b . '</li>'. PHP_EOL;
            }
        }
        $html[] = '</ol>'. PHP_EOL;
        $html[] = '</div></div></nav>'. PHP_EOL;
    }

    $html[] = '<div id="cclicence">'. PHP_EOL;
    $html[] = '<p><a href="https://creativecommons.org/licenses/by/4.0/deed.de" target="blank"><img id="imagecc" src="' . $base_URL . '/images/ccby.png"></a>'. PHP_EOL;
    $html[] = '<a href="https://www.tu-chemnitz.de/ub/" target="blank">UB Chemnitz</a><br>'. PHP_EOL;
    $html[] = '<a href="https://gitlab.hrz.tu-chemnitz.de/ubc/ikonline/" target="blank">Gitlab TU Chemnitz</a></p>'. PHP_EOL;
    $html[] = '</div>'. PHP_EOL;

    $html[] = '<div class="content">'. PHP_EOL;
    $html[] = '<div class="container ikonline">'. PHP_EOL;
    $html[] = '<div class="row">'. PHP_EOL;

    # include the main navigation in current language
    # main navigation is defined in config.inc
    $html[] = main_navigation($current_lang);

    # if there is an index element, the menu for start page will be included
    if ($index)
    {
        $html[] = menu_startpage($current_lang);
    }
    # else the modul navigation will include, the array is in naviation.inc
    else
    {
        require('navigation.inc');
        $html[] = getMenu($files, $current_lang);
    }

    $html[] = '<div class="col-sm-9 col-xs-12 modul-content">' . PHP_EOL;
    # include forward and back button, when normal modul page
    if (!$index)
    {
        $html[] = getNextPrevious ($files, $current_lang);
    }

    //echo ist nessessary before include
    echo implode('',$html);

    # include content file
    read($file);

    $html = array();

    $html[] = '<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" 
        data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>'. PHP_EOL;
    $html[] = '</div></div></div></div>'. PHP_EOL;
    $html[] = '</body>'. PHP_EOL;
    $html[] = '</html>'. PHP_EOL;

    echo implode('',$html);

    exit;
}

# main navigation on top
function main_navigation($current_lang)
{
    global $base_dir, $base_URL, $array_navigation, $url, $startpage, $tuc ;
    # get the naviagtion in current language
    $navigation = $array_navigation[$current_lang];

    $file_ending = $current_lang === 'de' ? '' : '.' . $current_lang;

    # array for output
    $html = array();

    # start navigation
    
    # TUC specific class for navigation
    $nav_class = $tuc ? 'navbar-inverse' : 'navbar-default';

    $html[] = '<nav class="navbar ' . $nav_class . '">'. PHP_EOL;
    $html[] = '<div class="container-fluid">'. PHP_EOL;
    $html[] = '<div class="navbar-header">'. PHP_EOL;

    # for mobile view
    $html[] = '<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main_navigation" aria-expanded="false">'. PHP_EOL;
    $html[] = '<span class="sr-only">Navigation ein-/ausblenden</span>'. PHP_EOL;
    $html[] = '<span class="icon-bar"></span>'. PHP_EOL;
    $html[] = '<span class="icon-bar"></span>'. PHP_EOL;
    $html[] = '<span class="icon-bar"></span>'. PHP_EOL;
    $html[] = '</button>'. PHP_EOL;

    $html[] = '<a class="navbar-brand" href="' . $base_URL . $startpage . $file_ending . '">' . $navigation['titel'] . '</a>'. PHP_EOL;
    $html[] = '</div>'. PHP_EOL;
    $html[] = '<div class="collapse navbar-collapse" id="main_navigation">'. PHP_EOL;
    $html[] = '<ul class="nav navbar-nav">'. PHP_EOL;

    # navigation on left side
    foreach ($navigation['links'] as $folder => $left)
    {
        # check in which part we are and set active for styling the active part
        $active = preg_match( '/' . $folder . '/' , $_SERVER[REQUEST_URI]) ? 'class="active"' : '';
        # navigation items     
        $html[] = '<li ' . $active . ' ><a href="' . $base_URL . '/content/' . $folder . '/index.html'. $file_ending . '">' . $left . '</a></li>'. PHP_EOL;
    }

    $html[] = '</ul>'. PHP_EOL;

    # navigation on right side
    $html[] = '<ul class="nav navbar-nav navbar-right">'. PHP_EOL;

    foreach ($navigation['rechts'] as $menu => $right)
    {
        # check if it is the language item
        if ($menu === 'Sprachen' || $menu === 'Languages' || $menu === 'Jazyk')
        {   
            foreach ($right as $l => $end)
            {
                if ($end !== $current_lang)
                {
                    # if the current language is german, than delete the ending becaurse
                    # german file has no specific file ending
                    $end = $end === 'de' ? '' : $end;
                    # get the url without the file ending
                    $url_basic = explode('.html', $url)[0];
                    # if current language is not german, the file ending need a dot e.g. .cs
                    $endurl = !empty($end) ? '.' . $end : $end;
                    # array of language items
                    $modul_language[] = '<li><a href="' . $url_basic . '.html' . $endurl . '">
                        <img class="lang-icon" src="'. $base_URL . '/images/sprache' . $end . '.png"> ' . $l .'</a></li>'. PHP_EOL;
                }
            }
        }
        # if there more then 2 languages there will be need a dropdown menu
        if(($menu === 'Sprachen'||  $menu === 'Languages' || $menu === 'Jazyk') && count($right) > 2)
        {
            $html[] = '<li class="dropdown">'. PHP_EOL;
            $html[] = '<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
                aria-expanded="false">' . $menu . '<span class="caret"></span></a>'. PHP_EOL;
            $html[] = '<ul id="language" class="dropdown-menu">'. PHP_EOL;
            $html[] = implode(' ', $modul_language);
        }
        elseif ($menu === 'Sprachen' && count($right) <=2)
        {
            $html[] = implode(' ' , $modul_language);
        }
        # normal menu item get an dropdowm menu up to 2 items
        elseif (is_array($right) && count($right) > 1)
        {
            $html[] = '<li class="dropdown">'. PHP_EOL;
            $html[] = '<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">' . $menu . '<span class="caret"></span></a>'. PHP_EOL;
            $html[] = '<ul class="dropdown-menu">'. PHP_EOL;
            foreach ($right as $submenu => $r)
            {
                $html[] = '<li><a href="' . $base_URL . '/content/' . $submenu . '/index.html'. $file_ending . '">' . $r . '</a></li>'. PHP_EOL;
            }
            $html[] = '</ul>'. PHP_EOL;
            $html[] = '</li>'. PHP_EOL;
        }
        else
        {
            $html[] = '<li><a href="' . $base_URL . '/'. $menu . '/index.html'. $file_ending . '">' . $right . '</a></li>'. PHP_EOL;
        }
    }

    $html[] = '</ul>'. PHP_EOL;
    $html[] = '</div>'. PHP_EOL;
    $html[] = '</div>'. PHP_EOL;
    $html[] = '</nav>'. PHP_EOL;

    return implode('',$html);
}

# left navigation for start and evaluation
function menu_startpage($current_lang)
{

    global $start_menu, $base_URL;

    $file_ending = $current_lang === 'de' ? '' : 'index.html.' . $current_lang;

    $html = array();

    # navigation start
    $html[] = '<div class="col-sm-3 col-xs-12" id="modul-nav">'. PHP_EOL;
    $html[] = '<nav class="navbar navbar-default" id="modulMenu">'. PHP_EOL;
    $html[] = '<div class="navbar-header">'. PHP_EOL;

    # mobile view
    $html[] = '<button id="modulMenu-button" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidemenu" aria-expanded="false">'. PHP_EOL;
    $html[] = '<span class="sr-only">Navigation ein-/ausblenden</span>'. PHP_EOL;
    $html[] = '<span class="icon-bar"></span>'. PHP_EOL;
    $html[] = '<span class="icon-bar"></span>'. PHP_EOL;
    $html[] = '<span class="icon-bar"></span>'. PHP_EOL;
    $html[] = '</button>'. PHP_EOL;

    $html[] = '<a class="navbar-brand sr-only" href="#">Modul Navigation</a>'. PHP_EOL;
    $html[] = '</div>'. PHP_EOL;
    $html[] = '<div id="sidemenu" class="navbar-collapse collapse">'. PHP_EOL;
    $html[] = '<ul class="nav nav-pills nav-stacked">'. PHP_EOL;

    #get the heading in current language
    $html[] = '<li class="modul-menu"><h3>' . key($start_menu[$current_lang]) . '</h3>'. PHP_EOL;

    $html[] = '<ul id="overview">'. PHP_EOL;

    foreach ($start_menu[$current_lang] as $menu)
    {
        foreach( $menu as $folder => $item)
        {
            # menu items
            $html[] = '<li><a href="' . $base_URL . '/content/' . $folder . '/' .  $file_ending . '">' . $item . '</a></li>'. PHP_EOL;
        }
    }
    $html[] = '</ul></li></ul>'. PHP_EOL;
    $html[] = '</div>'. PHP_EOL;
    $html[] = '</nav>'. PHP_EOL;
    $html[] = '</div>'. PHP_EOL;

    return implode(' ', $html);
}

# get buttons forward and back
function getNextPrevious ($files, $current_lang)
{
    global $file_current, $file_name, $vor, $zurueck;

    # list of alle pages
    $page = array();

    # previous page
    $previous = '';

    # next page
    $next = '';

    $flag = false;

    # write all possible file names from navigation.inc in a sereate file
    foreach($files[$current_lang] as $file => $name)
    {   
        $i = 0;

        # if the value is not an array (no subitems) write the key of array in the page array
        if (!is_array($name))
        {
            $page[] = $file;
        }
        # if the value is an array
        else
        {   
            # loop the array
            foreach($name as $f => $n)
            {
                # if the key of the first array element is not equal to the key of $file, 
                # there is no page for the mainitem 
                # this key will not be added to list
                if(($i==0 && $file === $f) || $i > 0)
                {
                    $page[] = $f;
                }
                $i++;
            }
        }
    }

    # loop the array of all pages
    foreach ($page as $p)
    {
        # if the values is equal to the current filename ($file_name), $flag will be set true
        # and $previous will not be overwriten
        if($p === $file_name)
        {
            $flag = true;
        }
        # if $flag is true the current page in loop is the next page
        # stop if loop
        elseif($flag == true)
        {
            $next = $p;
            break;
        }
        # if the current page in loop not equal to current file and $flag
        # is not true, overwrite $previous with current page
        else
        {
            $previous = $p;
        }
    }

    # get the correct file ending
    $file_end = $current_lang === 'de' ? '.html' : '.html.' . $current_lang;

    $html = array();

    $html[] = '<nav aria-label="modul-content">'. PHP_EOL;
    $html[] = '<ul class="pager">'. PHP_EOL;

    # get the button if not first or last page
    $html[] = !empty($previous) ? '<li class="col-xs-6 text-left"><a href="' . $previous . $file_end . '">' . $zurueck[$current_lang] . '</a></li>' : ''. PHP_EOL;
    $html[] = empty($previous) ? '<li class="col-xs-12 text-right">' : '<li class="col-xs-6 text-right">'. PHP_EOL;
    $html[]= !empty($next) ? '<a href="' . $next . $file_end . '">' . $vor[$current_lang] . '</a></li>' : ''. PHP_EOL;
    $html[] = '</ul></nav>'. PHP_EOL;

    return implode ('', $html);
}


# modul menu
function getMenu($files, $current_lang)
{
    global $file_current, $file_name;

    $html = array();

    #menu start
    $html[] = '<div class="col-sm-3 col-xs-12" id="modul-nav">'. PHP_EOL;
    $html[] = '<nav class="navbar navbar-default" id="modulMenu">'. PHP_EOL;
    $html[] = '<div class="navbar-header">'. PHP_EOL;

    # mobile view
    $html[] = '<button id="modulMenu-button" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidemenu" aria-expanded="false">'. PHP_EOL;
    $html[] = '<span class="sr-only">Navigation ein-/ausblenden</span>'. PHP_EOL;
    $html[] = '<span class="icon-bar"></span>'. PHP_EOL;
    $html[] = '<span class="icon-bar"></span>'. PHP_EOL;
    $html[] = '<span class="icon-bar"></span>'. PHP_EOL;
    $html[] = '</button>'. PHP_EOL;

    $html[] = '<a class="navbar-brand sr-only" href="#">Modul Navigation</a>'. PHP_EOL;
    $html[] = '</div>'. PHP_EOL;
    $html[] = '<div id="sidemenu" class="navbar-collapse collapse">'. PHP_EOL;
    $html[] = '<ul class="nav nav-pills nav-stacked ">'. PHP_EOL;

    # loop the array with content structure
    foreach ($files[$current_lang] as $file => $name)
    {
        $file_ending = $current_lang === 'de' ? '' : '.'.$current_lang;

        # $name is not an array (not submenu)
        if (!is_array($name))
        {
            # if current file ist equal to looped file set active
            $active = (strpos($file_current, $file) !== false) ? 'active' : '';
            $html[] = '<li class="modul-menu ' . $active . '"><a href="' . $file . '.html'.$file_ending.'">' . $name . '</a></li>'. PHP_EOL;


        }
        # if $name is an array (submenu)
        else
        {
            # flag to get the first element of the array
            $first = TRUE;

            foreach ($name as $f => $n)
            {
                if ($first === TRUE)
                {
                    # if it is the first element in array, it is the main item of the submenu
                    # if the current file also in looped submenu, set active
                    $active = (array_key_exists($file_name, $name)) ? 'active' : '';

                    # we use for the first item (main item of submenu) allways the key $file of the main array, because
                    # some main menu headings has no page, so the key of main array is filename of the first page and will be used 
                    # for the link e.g. modul 3
                    $html[] = '<li class="modul-menu ' . $active . '"><a href="' . $file . '.html' . $file_ending . '">' . $n . ' <span class="caret"></span></a>'. PHP_EOL;

                    $html[] = '<ul>'. PHP_EOL;
                    $first = FALSE;
                }

                else 
                {
                    # if the current submenu is equal to current file set class active, if not class normal (css)
                    $active = $file_name === $f ? 'active' : 'normal';

                    # if the current file ist not in array of current submenu is, set class hidden
                    # the dropdown elements will not be shown
                    $hidden = (array_key_exists($file_name, $name)) ? '': 'hidden';

                    $html[] = '<li class="' . $active . ' ' . $hidden . '"><a href="'.$f.'.html' . $file_ending . '">' . $n . '</a></li>'. PHP_EOL;
                }
            }
            $html[] = '</ul></li>'. PHP_EOL;
        }
    }

    $html[] = '</ul>'. PHP_EOL;
    $html[] = '</div>'. PHP_EOL;
    $html[] = '</div>'. PHP_EOL;

    return implode('',$html);
}

# function to include the file content
function read ($file) {
    include($file);
}

//Popover for glossary terms
function get_popover($id, $text)
{
    global $glossary;

    $html = array();

    $html[] = '<a class="popoverlink" tabindex="0" data-toggle="popover" data-html="true" ';
    $html[] = 'title="' . $glossary[$id]['title'] . '" data-trigger="hover" ';
    $html[] = 'data-content="' . $glossary[$id]['text'] . '">';
    $html[] = $text;
    $html[] = ' <span class="glyphicon glyphicon-book glossar"></span></a>';

    echo implode('', $html);	
}

# Glossary + alphabetic navigation
function get_glossary()
{
    global $glossary, $current_lang, $see, $seeAlso;

    # set local to order glossary array correct
    if ($current_lang === 'cs')
    {
        setlocale (LC_ALL, 'cs_CZ.utf8');
    }
    elseif($current_lang === 'de')
    {
        setlocale (LC_ALL, 'de_DE.utf8');
    }

    # order array by title
    $title = array();
    foreach ($glossary as $k => $row)
    {
        $title[$k] = $row['title'];
    }
    
    array_multisort($title, SORT_ASC, SORT_LOCALE_STRING, $glossary);

    $html = array('<dl id="glossar">');

    # varibale for last first letter
    $letter = '';

    # array for letters of navigation on top of glossary
    $head_navigation = array();
    foreach($glossary as $key => $term)
    {
        # get the first letter if terms title
        $letter_now = $term['title'][0];

        # compare the current letter with the last one
        if ($letter !== $letter_now)
        {
            $html[] ='<hr id="'.$letter_now.'">';
            # save current letter for navigation
            $head_navigation[] = $letter_now; 
        }
        #  save current letter
        $letter = $letter_now;

        # glossary output
        $html[] = '<dt id="'.$key.'">'.$term['title'];

        # refer to main term and other terms
        # only the first words until ',' will been shown, becaurse the other words are the repetition of sub terms 
        # keys to other terms are seperated by ' , '

        # check if more than one term is given
        if($term['link'])
        {
            $html[] = ', ' . $seeAlso;
            # write terms into an array
            $text = array();
            foreach(explode(' , ', $term['link']) as $v)
            {
                # links to terms
                $text[] = '<a href="#' . $v . '">'. preg_split('/,/', $glossary[$v]['title'])[0].'</a>';
            }
            $html[] = implode(', ', $text);
        }
        elseif($term['main'])
        {
            $html[] = ', ' . $see;
            $text = array();
            foreach(explode(' , ', $term['main']) as $v)
            {
                $text[] = '<a href="#' . $v . '">'. preg_split('/,/', $glossary[$v]['title'])[0].'</a>';
            }
            $html[] = implode(', ', $text); 
        }
        $html[] = '</dt>';
        $html[] = $term['text'] ? '<dd>' . $term['text'] . '</dd>' : '';

    }
    $html[] =  '</dl>';

    # navigation on top via letters of alphabet

    # range auf alphabet
    $alphabet = range('A','Z');

    $navigation = array('<div id="aZ" class="col-md-12 col-xs-12 col-md-offset-2">','<p>');

    foreach($alphabet as $a)
    {
        # if the letter of alphabet is in the array of used letters, set a link
        if(in_array($a,$head_navigation))
        {
            $navigation[] = '<a href="#' . $a . '">' . $a . '</a> | ';
        }
        else
        {
            $navigation[] = $a . ' | ';
        }
    }
    $navigation[] = '</p></div>';

    echo implode('', $navigation);
    echo implode('', $html);
}


?>
